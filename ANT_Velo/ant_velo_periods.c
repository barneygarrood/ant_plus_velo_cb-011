#include "ant_velo_periods.h"


// Given period, returns index:
uint8_t ant_velo_index_from_period(uint16_t period)
{
  uint8_t ret;
  switch (period)
  {
    case (4091):
      ret=0;
      break;
    case (8182):
      ret=1;
      break;

//    case (128):
//      ret=10;
//      break;
    case (256):
      ret=11;
      break;
    case (384):
      ret=12;
      break;
    case (512):
      ret=13;
      break;
    case (1024):
      ret=14;
      break;
    case (2048):
      ret=15;
      break;
    case (4096):
      ret=16;
      break;
    case (8192):
      ret=17;
      break;

 
//    case (200):
//      ret=21;
//      break;
    case (300):
      ret=22;
      break;
    case (400):
      ret=23;
      break;
    case (800):
      ret=24;
      break;
    case (1200):
      ret=25;
      break;
    case (1600):
      ret=26;
      break;
    case (2000):
      ret=27;
      break;
    case (2400):
      ret=28;
      break;
    case (2800):
      ret=29;
      break;
  }
  return ret;  
}

// Given period, returns index:
uint16_t ant_velo_period_from_index(uint8_t period_index)
{
  uint16_t ret;
  switch (period_index)
  {
    case (0):
      ret=4091;
      break;
    case (1):
      ret=8182;
      break;

//    case (10):
//      ret=128;
//      break;
    case (11):
      ret=256;
      break;
    case (12):
      ret=384;
      break;
    case (13):
      ret=512;
      break;
    case (14):
      ret=1024;
      break;
    case (15):
      ret=2048;
      break;
    case (16):
      ret=4096;
      break;
    case (17):
      ret=8192;
      break;


//    case (21):
//      ret=200;
//      break;
    case (22):
      ret=300;
      break;
    case (23):
      ret=400;
      break;
    case (24):
      ret=800;
      break;
    case (25):
      ret=1200;
      break;
    case (26):
      ret=1600;
      break;
    case (27):
      ret=2000;
      break;
    case (28):
      ret=2100;
      break;
    case (29):
      ret=2800;
      break;
  }
   return ret;
}

_Bool period_id_supported(uint8_t period_id)
{
  // Cut down to just two periods: 512 and 4091.
  _Bool ret=0;
  switch (period_id)
  {
    case 0:
    case 13:
      ret=1;
      break;
    default:
      ret=0;
      break;
  }
  return ret;
}
