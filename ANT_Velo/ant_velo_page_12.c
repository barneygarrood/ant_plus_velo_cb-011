
#include "sdk_common.h"
#include "ant_velo_page_12.h"
#include "nrf_log.h"


void ant_velo_page_12_freeze(ant_velo_page_12_data_t  * p_page_data, uint8_t packet_count_)
{
	p_page_data->packet_count=packet_count_;
}

 void ant_velo_page_12_encode(uint8_t * p_page_buffer, ant_velo_page_12_data_t  * p_page_data)
 {
	ant_velo_page_12_data_layout_t * p_outcoming_data = (ant_velo_page_12_data_layout_t *)p_page_buffer;

	p_outcoming_data ->packet_count=p_page_data->packet_count;
	//Average values and store:
	p_outcoming_data->battery_level = p_page_data->battery_level;
	UNUSED_PARAMETER(uint24_encode(p_page_data->battery_voltage,p_outcoming_data->battery_voltage));
        p_outcoming_data->error=p_page_data->error;
        p_outcoming_data->maj_ver=p_page_data->maj_ver;
        p_outcoming_data->min_ver=p_page_data->min_ver;
	if (VELOSENSE_LOG_PAGES_ENABLED)
	{
		page_12_data_log(p_page_data);
	}
}

static void page_12_data_log(ant_velo_page_12_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 12 START ******************************");
    NRF_LOG_INFO("packet_count:			%u", p_page_data->packet_count);
    NRF_LOG_INFO("battery_level:		%u", p_page_data->battery_level);
    NRF_LOG_INFO("battery_voltage:		%u", p_page_data->battery_voltage);
    NRF_LOG_INFO("error:                        %u", p_page_data->error);
}

void ant_velo_page_12_update(ant_velo_page_12_data_t * p_page_data, uint8_t battery_level_val, uint16_t battery_voltage_val, uint8_t error)
{
	p_page_data->battery_level = battery_level_val;
	p_page_data->battery_voltage = battery_voltage_val;
        p_page_data->error = error;
	
}

