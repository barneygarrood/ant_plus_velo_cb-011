
#include "sdk_common.h"
#include "ant_velo_page_10.h"
#include "nrf_log.h"


void ant_velo_page_10_freeze(ant_velo_page_10_data_t * p_page_data, uint8_t packet_count_)
{
	//Average values and store:
	p_page_data->sdp_1 = (uint16_t)(p_page_data->sdp_1_register[p_page_data->register_toggle]/p_page_data->count);
	p_page_data->sdp_2 = (uint16_t)(p_page_data->sdp_2_register[p_page_data->register_toggle]/p_page_data->count);
	p_page_data->baro_temp = (uint16_t)(p_page_data->baro_temp_register[p_page_data->register_toggle]/p_page_data->count);
	
	p_page_data->packet_count=packet_count_;
	
	if (VELOSENSE_LOG_PAGES_ENABLED)
	{
		page_10_data_log(p_page_data);
	}
	// toggle parameter to say we have taken data so reset register going forward:
	p_page_data->reset_register=true;
}

void ant_velo_page_10_encode(uint8_t * p_page_buffer, ant_velo_page_10_data_t  * p_page_data)
{
	ant_velo_page_10_data_layout_t * p_outcoming_data = (ant_velo_page_10_data_layout_t *)p_page_buffer;
	p_outcoming_data->packet_count=p_page_data->packet_count;
	UNUSED_PARAMETER(uint16_encode(p_page_data->sdp_1,p_outcoming_data->sdp_1));
	UNUSED_PARAMETER(uint16_encode(p_page_data->sdp_2,p_outcoming_data->sdp_2));
	UNUSED_PARAMETER(uint16_encode(p_page_data->baro_temp,p_outcoming_data->baro_temp));
}
		
/**@brief Function for logging page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
void page_10_data_log(ant_velo_page_10_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 10 START ******************************");
    NRF_LOG_INFO("sample_count:					%u", p_page_data->count);
    NRF_LOG_INFO("packet count:					%u", p_page_data->packet_count);
    NRF_LOG_INFO("sdp_1:					%u", p_page_data->sdp_1);
    NRF_LOG_INFO("sdp_2:					%u", p_page_data->sdp_2);
    NRF_LOG_INFO("baro_temp:                                    %u", p_page_data->baro_temp);
}

void ant_velo_page_10_update(ant_velo_page_10_data_t * p_page_data, uint16_t sdp_1_val, uint16_t sdp_2_val,uint16_t baro_temp_val)
{
	//We have two registers, idea being to take data from completed register.  
	// Say current register is register A, and other register is register B, we 
	// add new data to register A and store it in B, then toggle the register to say complete.
	if (p_page_data->reset_register)
	{
		p_page_data->sdp_1_register[p_page_data->register_toggle^ 0x01]=	(uint32_t)sdp_1_val;
		p_page_data->sdp_2_register[p_page_data->register_toggle^ 0x01]=	(uint32_t)sdp_2_val;
		p_page_data->baro_temp_register[p_page_data->register_toggle^ 0x01]=	(uint32_t)baro_temp_val;
		p_page_data->count=1;
		p_page_data->reset_register=false;
	}
	else
	{
		p_page_data->sdp_1_register[p_page_data->register_toggle^ 0x01] =	p_page_data->sdp_1_register[p_page_data->register_toggle] +(uint32_t)(sdp_1_val);
		p_page_data->sdp_2_register[p_page_data->register_toggle^ 0x01] =	p_page_data->sdp_2_register[p_page_data->register_toggle] +(uint32_t)(sdp_2_val);
		p_page_data->baro_temp_register[p_page_data->register_toggle^ 0x01] =	p_page_data->baro_temp_register[p_page_data->register_toggle] +(uint32_t)(baro_temp_val);
		p_page_data->count++;
	}
	// toggle the register (just toggles least significant bit):
	p_page_data->register_toggle^= 0x01;
	
}

