
#ifndef ANT_VELO_PAGE_11_H__
#define ANT_VELO_PAGE_11_H__

#include <stdint.h>

/**@brief Data packet struct.
*/
typedef struct
{
	uint8_t packet_count;
	uint8_t baro_press_1[3];
	uint8_t baro_press_2[3];
	
}ant_velo_page_11_data_layout_t;

/**@brief Page data struct.
*/
typedef struct
{
	uint8_t count;
	
	uint32_t baro_press_1_register[2];
	uint32_t baro_press_2_register[2];
	
	uint8_t packet_count;
	uint32_t baro_press_1;
	uint32_t baro_press_2;
	
	uint8_t register_toggle;
	
	bool reset_register;
	
	
} ant_velo_page_11_data_t;

//Initialiser
#define ANT_VELO_PAGE_11(count_,baro_press_1_,baro_press_2_)  \
    (ant_velo_page_11_data_t)                      	\
    {                                               \
        .count     = (count_),        \
        .baro_press_1_register[0] = (baro_press_1_),                			\
        .baro_press_2_register[0] = (baro_press_2_),                	  \
        .register_toggle = 0,                	  \
        .reset_register = false,                	  \
    }

		
/**@brief Function for averaging register data".
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
void ant_velo_page_11_freeze(ant_velo_page_11_data_t * p_page_data, uint8_t packet_count_);	
		
		
/**@brief Function for encoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_velo_page_11_encode(uint8_t * p_page_buffer, ant_velo_page_11_data_t  * p_page_data);	
		
/**@brief Function for logging page data.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
static void page_11_data_log(ant_velo_page_11_data_t const * p_page_data);	

		
/**@brief Function to update page with latest data:
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[in]  range_val	Latest value for range parameter.
 */
void ant_velo_page_11_update(ant_velo_page_11_data_t * p_page_data, uint32_t baro_press_1_val, uint32_t baro_press_2_val);
	
#endif // ANT_VELO_PAGE_11_H__
