#ifndef ANT_VELO_PERIODS_H__
#define ANT_VELO_PERIODS_H__

#include <stdint.h>

uint8_t ant_velo_index_from_period(uint16_t period);
uint16_t ant_velo_period_from_index(uint8_t period_index);
_Bool period_id_supported(uint8_t period_id);

#endif // ANT_VELO_PERIODS_H__