
#ifndef ANT_VELO_PAGE_102_H__
#define ANT_VELO_PAGE_102_H__

#include <stdint.h>
#include "ant_velo_periods.h"

/**@brief Data packet struct.
*/
typedef struct
{
	uint8_t period_id;
        uint8_t period[2];
        uint8_t unused[4];
	
}ant_velo_page_102_data_layout_t;

/**@brief Page data struct.
*/
typedef struct
{
	uint8_t period_id;
        uint16_t period;
} ant_velo_page_102_data_t;

//Initialiser
#define ANT_VELO_PAGE_102(period_id_, period_)      \
    (ant_velo_page_102_data_t)                      \
    {                                               \
        .period_id = period_id_,                    \
        .period = period_,                          \
    }

		
		
/**@brief Function for encoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_velo_page_102_encode(uint8_t * p_page_buffer, ant_velo_page_102_data_t  * p_page_data);	

/**@brief Function for decoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_velo_page_102_decode(uint8_t * p_page_buffer, ant_velo_page_102_data_t  * p_page_data);	
	
/**@brief Function for logging page data.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
static void page_102_data_log(ant_velo_page_102_data_t const * p_page_data);	

		
/**@brief Function to update page with latest data:
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[in]  range_val	Latest value for range parameter.
 */
void ant_velo_page_102_update(ant_velo_page_102_data_t * p_page_data, uint8_t period_id);
	
#endif // ANT_VELO_PAGE_102_H__
