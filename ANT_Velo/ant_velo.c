
//#include "velo_driver.h"
#include "ant_velo.h"
#include "twi_scanner.h"

uint32_t packet_count=0;

static uint8_t ant_payload[ANT_STANDARD_DATA_PAYLOAD_SIZE] = {0};

/**@brief Message data layout structure. */
typedef struct
{
    uint8_t page_number;
    uint8_t page_payload[7];
} ant_velo_message_layout_t;

// Profile intialisation:
ret_code_t ant_velo_init(ant_velo_profile_t           * p_profile,
                              ant_channel_config_t const   * p_channel_config,
                              ant_velo_config_t const * p_sens_config)
{
	
  ASSERT(p_profile != NULL);
  ASSERT(p_channel_config != NULL);
  ASSERT(p_sens_config != NULL);
      
  p_profile->evt_handler   = p_sens_config->evt_handler;
  p_profile->p_sens_cb = p_sens_config->p_cb;
  
  p_profile->p_sens_cb->regular_message_counter = 0;
  p_profile->p_sens_cb->battery_message_counter = 0;
  p_profile->p_sens_cb->acknowledge_message = false;
  p_profile->p_sens_cb->acknowledge_messsage_counter=0;
  p_profile->p_sens_cb->ant_period=VELO_MSG_PERIOD;
        
  p_profile->channel_number = p_channel_config->channel_number;

  p_profile->page_10  = ANT_VELO_PAGE_10(0,0,0,0,0);
  p_profile->page_11  = ANT_VELO_PAGE_11(0,0,0);
  p_profile->page_12  = ANT_VELO_PAGE_12(0,0,0,VELO_SW_REVISION_MAIN, VELO_SW_REVISION_SUP );
  p_profile->page_101 = ANT_VELO_PAGE_101(0,0);
  p_profile->page_102 = ANT_VELO_PAGE_102(0,0);

  p_profile->p_msg_payload = ant_payload;

  return ant_channel_init(p_channel_config);
	
}

/**@brief Function for getting next page number to send.
 *
 * @param[in]  p_profile        Pointer to the profile instance.
 *
 * @return     Next page number.
 */
static ant_velo_page_t next_page_number_get(ant_velo_profile_t * p_profile)
{
    ant_velo_page_t page_number;

    if (p_profile->p_sens_cb->acknowledge_message)
    {
      page_number = (ant_velo_page_t) 102;
      p_profile->p_sens_cb->acknowledge_messsage_counter++;
      if (p_profile->p_sens_cb->acknowledge_messsage_counter==p_profile->p_sens_cb->acknowledge_message_max_count)
      {
        p_profile->p_sens_cb->acknowledge_message=false;
      }
    }
    else
    {
      if (p_profile->p_sens_cb->regular_message_counter==(VELO_PAGE_COUNT-1))
      {
        p_profile->p_sens_cb->regular_message_counter=0;
        page_number = (ant_velo_page_t) 10;
      }
      else
      {
        p_profile->p_sens_cb->regular_message_counter++;
        if ((packet_count) % VELO_BATTERY_PAGE_FREQUENCY == 0)
        {
          page_number=(ant_velo_page_t)12;
        }
        else
        {
          page_number=(ant_velo_page_t)11;
        }
      }
    }
    return page_number;
}

/**@brief Function for encoding message.
 *
 * @note Assume to be call each time when Tx window will occur.
 */
static void sens_message_encode(ant_velo_profile_t * p_profile, uint8_t * p_message_payload)
{
    ant_velo_message_layout_t * p_velo_message_payload = (ant_velo_message_layout_t *)p_message_payload;

    p_velo_message_payload->page_number = next_page_number_get(p_profile);

    //Note we encode data always on the 0th page so that all 3 pages have the same timestamp.
    switch (p_velo_message_payload->page_number)
    {
      case ANT_VELO_PAGE_10:
        ant_velo_page_10_freeze(&(p_profile->page_10),packet_count);
        ant_velo_page_11_freeze(&(p_profile->page_11),packet_count);
        ant_velo_page_12_freeze(&(p_profile->page_12),packet_count);
        packet_count++;
        ant_velo_page_10_encode(p_velo_message_payload->page_payload,&(p_profile->page_10));
        break;
      case ANT_VELO_PAGE_11:
        ant_velo_page_11_encode(p_velo_message_payload->page_payload,&(p_profile->page_11));
        break;
      case ANT_VELO_PAGE_12:
        ant_velo_page_12_encode(p_velo_message_payload->page_payload,&(p_profile->page_12));
        break;
      case ANT_VELO_PAGE_102:
        //Copy over data from page 101:
        if (period_id_supported(p_profile->page_101.period_id))
        {
          ant_velo_page_102_update(&(p_profile->page_102),p_profile->page_101.period_id);
        }
        else
        {
          ant_velo_page_102_update(&(p_profile->page_102),255);
        }
        ant_velo_page_102_encode(p_velo_message_payload->page_payload,&(p_profile->page_102));
        break;
      default:
        return;
    }

    p_profile->evt_handler(p_profile, (ant_velo_page_t)p_velo_message_payload->page_number);
}


/**@brief Function for encoding message.
 *
 * @note Assume to be call each time when Tx window will occur.
 */
static void sens_message_decode(ant_velo_profile_t * p_profile, uint8_t * p_message_payload)
{
    ant_velo_message_layout_t * p_velo_message_payload = (ant_velo_message_layout_t *)p_message_payload;

    NRF_LOG_INFO("Received page %u",p_velo_message_payload->page_number);
    switch (p_velo_message_payload->page_number)
    {
      case ANT_VELO_PAGE_101:       
        ant_velo_page_101_decode(p_velo_message_payload->page_payload,&(p_profile->page_101));
        break;
      default:
        return;
    }
    p_profile->evt_handler(p_profile, (ant_velo_page_t)p_velo_message_payload->page_number);
}



// Queues up message to be sent on next ANT event.
static void ant_message_send(ant_velo_profile_t * p_profile)
{
    uint32_t err_code;
    //uint8_t  p_message_payload[ANT_STANDARD_DATA_PAYLOAD_SIZE];
		//OVerwrite second byte with packet counter.
    uint8_t * p_message_payload = p_profile->p_msg_payload;
    sens_message_encode(p_profile, p_message_payload);
    err_code = sd_ant_broadcast_message_tx(p_profile->channel_number,sizeof (p_message_payload),p_message_payload);
    APP_ERROR_CHECK(err_code);
}

// Open ANT channel
ret_code_t ant_velo_open(ant_velo_profile_t * p_profile)
{
    // Fill tx buffer for the first frame
    ant_message_send(p_profile);

    return sd_ant_channel_open(p_profile->channel_number);
}


// Ant event handler
void ant_velo_stack_evt_handler(ant_evt_t * p_ant_event, void * p_context)
{
    ant_velo_profile_t * p_profile = ( ant_velo_profile_t *)p_context;
    
    if (p_ant_event->channel == p_profile->channel_number)
    {
      uint8_t * p_message_payload = p_profile->p_msg_payload;

      if (p_ant_event->channel == p_profile->channel_number)
      {
        switch (p_ant_event->event)
        {
          case EVENT_TX:
            //NRF_LOG_INFO("VELO TX.");
            p_profile->p_sens_cb->event_is_tx = true;
            ant_message_send(p_profile);
            break;

          case EVENT_RX:
            //NRF_LOG_INFO("Message received")
            p_profile->p_sens_cb->event_is_tx=false;
            if (p_ant_event->message.ANT_MESSAGE_ucMesgID == MESG_BROADCAST_DATA_ID)
            {
            /* NEED TO DEAL WITH INCOMING MESSAGE!! */							
              sens_message_decode(p_profile, p_ant_event->message.ANT_MESSAGE_aucPayload);
            }
            break;

          default:
            // No implementation needed
            break;
        }
      }
    }
}

void ant_velo_set_channel_id(uint8_t ant_channel_id,ant_channel_config_t ant_chan_cfg)
{
	uint8_t err_code = sd_ant_channel_id_set(ant_channel_id,ant_chan_cfg.device_number,ant_chan_cfg.device_type,
									ant_chan_cfg.transmission_type);
	APP_ERROR_CHECK(err_code);	
	
}

void ant_velo_period_set(uint8_t ant_channel_id, uint16_t ant_channel_period)
{
  sd_ant_channel_period_set (ant_channel_id, ant_channel_period);
}

