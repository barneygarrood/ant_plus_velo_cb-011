
#ifndef ANT_VELO_PAGE_12_H__
#define ANT_VELO_PAGE_12_H__

#include <stdint.h>

typedef struct
{
	uint8_t packet_count;
	uint8_t battery_level;
	uint8_t battery_voltage[2];
        uint8_t error;
        uint8_t maj_ver;
        uint8_t min_ver;
	
}ant_velo_page_12_data_layout_t;

typedef struct
{
	uint8_t packet_count;
	uint8_t battery_level;
	uint16_t battery_voltage;
        uint8_t error;
        uint8_t maj_ver;
        uint8_t min_ver;
	
} ant_velo_page_12_data_t;


//Initialiser
#define ANT_VELO_PAGE_12(packet_count_,battery_level_,battery_voltage_, maj_ver_, min_ver_ )  \
    (ant_velo_page_12_data_t)                                   \
    {                                                           \
        .packet_count     = (packet_count_),                    \
        .battery_level = (battery_level_),                	\
        .battery_voltage = (battery_voltage_),                  \
        .maj_ver = (maj_ver_),                			\
        .min_ver = (min_ver_),                			\
        .min_ver = (min_ver_),                			\
        .error = 0,                                             \
    }

/**@brief Function for freezing page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
void ant_velo_page_12_freeze(ant_velo_page_12_data_t  * p_page_data, uint8_t packet_count_);	
		
/**@brief Function for encoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_velo_page_12_encode(uint8_t * p_page_buffer, ant_velo_page_12_data_t  * p_page_data);	
		
/**@brief Function for logging page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
static void page_12_data_log(ant_velo_page_12_data_t const * p_page_data);	

/**@brief Function to update page with latest data:
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[in]  battery_level_val	battery level.
 */
void ant_velo_page_12_update(ant_velo_page_12_data_t * p_page_data, uint8_t battery_level_val, uint16_t battery_voltage_val, uint8_t error);

#endif // ANT_VELO_PAGE_12_H__
