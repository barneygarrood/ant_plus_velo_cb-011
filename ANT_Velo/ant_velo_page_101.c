
#include "sdk_common.h"
#include "ant_velo_page_101.h"
#include "nrf_log.h"

 void ant_velo_page_101_encode(uint8_t * p_page_buffer, ant_velo_page_101_data_t  * p_page_data)
{
  ant_velo_page_101_data_layout_t * p_outcoming_data = (ant_velo_page_101_data_layout_t *)p_page_buffer;
  p_outcoming_data->period_id=p_page_data->period_id;
}

 void ant_velo_page_101_decode(uint8_t const * p_page_buffer, ant_velo_page_101_data_t  * p_page_data)
{
	
  ant_velo_page_101_data_layout_t const * p_incoming_data = (ant_velo_page_101_data_layout_t *)p_page_buffer;
  p_page_data->period_id=p_incoming_data->period_id;

  p_page_data->period=ant_velo_period_from_index(p_page_data->period_id);
}
		
/**@brief Function for logging page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
static void page_101_data_log(ant_velo_page_101_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 101 START ******************************");
    NRF_LOG_INFO("period_id:					%u", p_page_data->period_id);
    NRF_LOG_INFO("period:                                         %u", p_page_data->period);
}

void ant_velo_page_101_update(ant_velo_page_101_data_t * p_page_data, uint8_t period_id)
{
  p_page_data->period_id = period_id;

  p_page_data->period=ant_velo_period_from_index(p_page_data->period_id);
 
  
}


