
#ifndef ANT_VELO_PAGE_10_H__
#define ANT_VELO_PAGE_10_H__

#include <stdint.h>

/**@brief Data packet struct.
*/
typedef struct
{
	uint8_t packet_count;
	uint8_t sdp_1[2];
	uint8_t sdp_2[2];
	uint8_t baro_temp[2];
	
}ant_velo_page_10_data_layout_t;

/**@brief Page data struct.
*/
typedef struct
{
	uint8_t count;
	uint8_t error_code;
	
	uint32_t sdp_1_register[2];
	uint32_t sdp_2_register[2];
	uint32_t baro_temp_register[2];
	
	uint8_t packet_count;
	uint16_t sdp_1;
	uint16_t sdp_2;
	uint16_t baro_temp;
	
	uint8_t register_toggle;
	
	bool reset_register;
	
} ant_velo_page_10_data_t;


//Initialiser
#define ANT_VELO_PAGE_10(count_,error_code_, sdp_1_, sdp_2_,baro_temp_)  \
    (ant_velo_page_10_data_t)                      	\
    {                                               \
        .count     = (count_),        \
        .sdp_1_register[0] = (sdp_1_),                			\
        .sdp_2_register[0] = (sdp_2_),                	  \
				.baro_temp_register[0]= (baro_temp_),				\
        .register_toggle = 0,                	  \
        .reset_register = false,                	  \
    }
		
/**@brief Function for averaging register data".
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
void ant_velo_page_10_freeze(ant_velo_page_10_data_t * p_page_data, uint8_t packet_count_);	

		/**@brief Function for encoding page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[out] p_page_buffer    Pointer to the data buffer.
 */
void ant_velo_page_10_encode(uint8_t * p_page_buffer, ant_velo_page_10_data_t * p_page_data);	
		
/**@brief Function for logging page data.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
//static void page_10_data_log(ant_velo_page_10_data_t const * p_page_data);	
void page_10_data_log(ant_velo_page_10_data_t const * p_page_data);	

		
/**@brief Function to update page with latest data:
 *
 * @param[in]  p_page_data      Pointer to the page data.
 * @param[in]  range_val	Latest value for range parameter.
 */
void ant_velo_page_10_update(ant_velo_page_10_data_t * p_page_data, uint16_t sdp_1_val, uint16_t sdp_2_val, uint16_t baro_temp_val);

	
#endif // ANT_VELO_PAGE_10_H__
