
#include "sdk_common.h"
#include "ant_velo_page_102.h"
#include "nrf_log.h"

 void ant_velo_page_102_encode(uint8_t * p_page_buffer, ant_velo_page_102_data_t  * p_page_data)
{
  ant_velo_page_102_data_layout_t * p_outcoming_data = (ant_velo_page_102_data_layout_t *)p_page_buffer;
  p_outcoming_data->period_id=p_page_data->period_id;
  UNUSED_PARAMETER(uint16_encode(p_page_data->period,p_outcoming_data->period));
  p_outcoming_data->unused[0]=0xFF;
  p_outcoming_data->unused[1]=0xFF;
  p_outcoming_data->unused[2]=0xFF;
  p_outcoming_data->unused[3]=0xFF;
}

 void ant_velo_page_102_decode(uint8_t * p_page_buffer, ant_velo_page_102_data_t  * p_page_data)
{
	
  ant_velo_page_102_data_layout_t * p_incoming_data = (ant_velo_page_102_data_layout_t *)p_page_buffer;
  p_page_data->period=p_incoming_data->period_id;
  p_page_data->period=ant_velo_period_from_index(p_page_data->period_id);
}
		
/**@brief Function for logging page.
 *
 * @param[in]  p_page_data      Pointer to the page data.
 */
static void page_102_data_log(ant_velo_page_102_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 102 START ******************************");
    NRF_LOG_INFO("period_id:					%u", p_page_data->period_id);
    NRF_LOG_INFO("period:                                         %u", p_page_data->period);
}

void ant_velo_page_102_update(ant_velo_page_102_data_t * p_page_data, uint8_t period_id)
{
  p_page_data->period_id = period_id;
  p_page_data->period = ant_velo_period_from_index(p_page_data->period_id);
  
}


