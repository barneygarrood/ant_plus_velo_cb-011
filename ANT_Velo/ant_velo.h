#ifndef ANT_VELO_H__
#define ANT_VELO_H__

#include <stdint.h>
#include <stdbool.h>
#include "ant_parameters.h"
#include "nrf_sdh_ant.h"
#include "ant_channel_config.h"
#include "ant_velo_pages.h"
#include "sdk_errors.h"

#include "sdk_common.h"
 
#include "nrf_assert.h"
#include "app_error.h"
#include "ant_interface.h"

#include "app_timer.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"

#include "velo_us_timer.h"

#include "velo_config.h"

#define VELO_DEVICE_TYPE            111u               ///< Device type
#define VELO_ANTPLUS_RF_FREQ        67u               ///< Frequency, decimal 57 (2457 MHz).
#define VELO_MSG_PERIOD             4091u //512u               ///< /32768s
#define VELO_EXT_ASSIGN             0x00u                ///< ANT ext assign (see Ext. Assign Channel Parameters in ant_parameters.h: @ref ant_parameters).
#define VELO_CHANNEL_TYPE      CHANNEL_TYPE_MASTER 		//< device is master, i.e. broadcasting.
#define VELO_PAGE_COUNT			2									//number of regular pages sent.
#define VELO_BATTERY_PAGE_FREQUENCY     64					//battery sent every N pages in place of page 1.

#define PAIRING_TIME_MS 10000			//Pairing stays on for 10s.

#define MAX_DEVICE_ID 	0xFFF
#define MAX_SERIAL_NO 	0xFFFFFFF

// Define ANT frequencies:
static const uint8_t ant_rf_freq = 67;

/** @brief Control block. */
typedef struct
{
    uint8_t regular_message_counter;		
    uint8_t battery_message_counter;
    uint16_t acknowledge_messsage_counter;
    uint16_t acknowledge_message_max_count;
    bool acknowledge_message;
    uint16_t ant_period;
    bool event_is_tx;
} ant_velo_cb_t;

/**@brief Initialize an ANT channel configuration structure.
 *
 * @param[in]  NAME                 Name of related instance.
 * @param[in]  CHANNEL_NUMBER       Number of the channel assigned to the profile instance.
 * @param[in]  TRANSMISSION_TYPE    Type of transmission assigned to the profile instance.
 * @param[in]  DEVICE_NUMBER        Number of the device assigned to the profile instance.
 * @param[in]  NETWORK_NUMBER       Number of the network assigned to the profile instance.
 */
#define VELO_CHANNEL_CONFIG_DEF(NAME,                                      	\
                                     CHANNEL_NUMBER,                            \
                                     TRANSMISSION_TYPE,                         \
                                     NETWORK_NUMBER)                          \
static ant_channel_config_t   CONCAT_2(NAME, _channel_config) = 		\
    {                                                                           \
        .channel_number    = (CHANNEL_NUMBER),                                  \
        .channel_type      = VELO_CHANNEL_TYPE,                            	\
        .ext_assign        = VELO_EXT_ASSIGN,                                   \
        .rf_freq           = VELO_ANTPLUS_RF_FREQ ,                                                \
        .transmission_type = (TRANSMISSION_TYPE),                               \
        .device_type       = VELO_DEVICE_TYPE, 					\
        .channel_period    = VELO_MSG_PERIOD,                                   \
        .network_number    = (NETWORK_NUMBER),                                  \
    }

#define VELO_CHANNEL_CONFIG(NAME) &CONCAT_2(NAME, _channel_config)

/**@brief Initialize an ANT profile configuration structure
 *
 * @param[in]  NAME                 Name of related instance.
 * @param[in]  EVT_HANDLER          Event handler to be called for handling events in the BPWR profile.
 */
#define VELO_PROFILE_CONFIG_DEF(NAME, EVT_HANDLER)                                   \
static ant_velo_cb_t            CONCAT_2(NAME, _cb);                 \
static const ant_velo_config_t  CONCAT_2(NAME, _profile_config) =    \
    {                                                                               \
        .p_cb               = &CONCAT_2(NAME, _cb),                       \
        .evt_handler        = (EVT_HANDLER),                                        \
    }
#define VELO_PROFILE_CONFIG(NAME) &NAME##_profile_config
		
 /**@brief Page number type. */
typedef enum
{
    ANT_VELO_PAGE_10 = 10,  	
    ANT_VELO_PAGE_11 = 11, 
    ANT_VELO_PAGE_12 = 12,
    ANT_VELO_PAGE_101 = 101,
    ANT_VELO_PAGE_102 = 102,
	
} ant_velo_page_t;

// Forward declaration of the ant_velo_profile_t type.
typedef struct ant_velo_profile_s ant_velo_profile_t;

/**@brief Event handler type - this defines pointer to the function. */
typedef void (* ant_velo_evt_handler_t) (ant_velo_profile_t *, ant_velo_page_t page);

/**@brief Configuration structure. */
typedef struct
{
    ant_velo_cb_t     * p_cb;          ///< Pointer to the data buffer for internal use.
    ant_velo_evt_handler_t   evt_handler;   ///< Event handler to be called for handling events .
	
} ant_velo_config_t;

 /**@brief Profile structure. */
struct ant_velo_profile_s
{
  uint8_t                     channel_number; ///< Channel number assigned to the profile.
  ant_velo_cb_t *             p_sens_cb;			///< Pointer to internal control block
  ant_velo_evt_handler_t      evt_handler;    ///< Event handler
  ant_velo_page_10_data_t     page_10;      
  ant_velo_page_11_data_t     page_11;      
  ant_velo_page_12_data_t     page_12;       
  ant_velo_page_101_data_t    page_101;      
  ant_velo_page_102_data_t    page_102; 
  uint8_t *                   p_msg_payload;
};
 

/**@brief Function for initializing the profile instance.
 *
 * @param[in]  p_profile        Pointer to the profile instance.
 * @param[in]  p_channel_config Pointer to the ANT channel configuration structure.
 * @param[in]  p_sens_config    Pointer to the configuration structure.
 *
 * @retval     NRF_SUCCESS      If initialization was successful. Otherwise, an error code is returned.
 */
ret_code_t ant_velo_init(ant_velo_profile_t           * p_profile,
                              ant_channel_config_t const   * p_channel_config,
                              ant_velo_config_t const * p_sens_config);

															
/**@brief Function for opening the profile instance channel for ANT Aerodynamics Sensor.
 *
 * Before calling this function, pages should be configured.
 *
 * @param[in]  p_profile        Pointer to the profile instance.
 *
 * @retval     NRF_SUCCESS      If the channel was successfully opened. Otherwise, an error code is returned.
 */
ret_code_t ant_velo_open(ant_velo_profile_t * p_profile);

/**@brief Function for handling the Sensor ANT events.
 *
 * @details This function handles all events from the ANT stack that are of interest to theprofile.
 *
 * @param[in]   p_ant_evt     Event received from the ANT stack.
 * @param[in]   p_context       Pointer to the profile instance.
 */
void ant_velo_stack_evt_handler(ant_evt_t * p_ant_evt, void * p_context);


void velo_start_pairing(void);
void velo_pairing_off(void * p_context);
void ant_velo_set_channel_id(uint8_t ant_channel_id,ant_channel_config_t ant_chan_cfg);
void ant_velo_period_set(uint8_t ant_channel_id, uint16_t ant_channel_period);
#endif // ANT_VELO_H__
