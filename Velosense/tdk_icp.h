#ifndef TDK_ICP_H__
#define TDK_ICP_H__

#include <stdint.h>
#include "twi_scanner.h"

#define TDK_ICP_ADDRESS 0x63
#define TDK_ICP_START_MEAS_LP 0x609C
#define TDK_ICP_START_MEAS_N 0x6825
#define TDK_ICP_START_MEAS_LN 0x70DF
#define TDK_ICP_START_MEAS_ULN 0x7866

extern uint8_t tdk_min_meas_time_ms;

/**
 * @brief Enum to hold oversample rate of tdk barometer.
 */
typedef enum
{
	TDK_ICP_LOW_POWER = 0x00,
	TDK_ICP_NORMAL = 0x01,
	TDK_ICP_LOW_NOISE = 0x02,
	TDK_ICP_ULTRA_LOW_NOISE = 0x03
} tdk_icp_osr_t;

/**
 * @brief Data structure to hold all of tdk barometer data.
 */
typedef struct 
{
	uint8_t twi_instance;
	uint32_t last_request_us;
	uint8_t pressure_en;
	uint8_t temperature_en;

        uint8_t read_requested;
        uint8_t read_started;

        uint8_t error;
	
	float sensor_constants[4]; // OTP values
	float p_Pa_calib[3];
	float LUT_lower;
	float LUT_upper;
	float quadr_factor;
	float offst_factor;

	int32_t raw_t;
	int32_t raw_p;
	float temperature;
	float pressure;
	
} tdk_icp_sensor_t;


void tdk_icp_init_all(uint8_t twi_instance_count,tdk_icp_sensor_t tdk_icp_sensors[], tdk_icp_osr_t oversample_rate,nrf_twi_mngr_transfer_t * p_trans);

void tdk_icp_init(tdk_icp_sensor_t * s, tdk_icp_osr_t oversample_rate);

void tdk_icp_read_otp(tdk_icp_sensor_t * s, short *out);

void tdk_icp_init_base(tdk_icp_sensor_t * s, short *otp);

void tdk_icp_process_data(tdk_icp_sensor_t * s);

void tdk_icp_calc_conversion_constants(tdk_icp_sensor_t * s, float *p_Pa,
	float *p_LUT, float *out);

void tdk_meas_start_transfer(tdk_icp_sensor_t * s, nrf_twi_mngr_transfer_t * p_trans);
void tdk_get_meas_transfer(tdk_icp_sensor_t * s, uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans);
void tdk_get_data_from_raw(tdk_icp_sensor_t * s, uint8_t * p_data);


#endif //TDK_ICP_H__
