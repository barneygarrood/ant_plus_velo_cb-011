#include "velo_us_timer.h"

uint64_t accumulated_us=0;
uint32_t last_time_us=0;

const nrf_drv_timer_t us_timer = NRF_DRV_TIMER_INSTANCE(1);


/* --------------------------- Setup functions start----------------------------------------------*/

void us_timer_event_handler(nrf_timer_event_t event_type, void* p_context)
{
}

void us_timer_config(void)
{
	uint32_t err_code=NRF_SUCCESS;
		nrf_drv_timer_config_t timer_cfg = 
	{
		.frequency = NRF_TIMER_FREQ_1MHz,	//Sets prescaler to get a counter increment at 1MHz.
		.mode = NRF_TIMER_MODE_TIMER,		// in this mode counter increments by 1 on each clock cycle (clock x prescaler actually)
		.bit_width = NRF_TIMER_BIT_WIDTH_32,    
		.interrupt_priority = TIMER_DEFAULT_CONFIG_IRQ_PRIORITY, 
    .p_context          = NULL     
		
	};
		err_code=nrf_drv_timer_init(&us_timer,&timer_cfg,us_timer_event_handler);
		APP_ERROR_CHECK(err_code);
    nrf_drv_timer_enable(&us_timer);
    //Initialise time in ms:
    last_time_us = nrf_drv_timer_capture(&us_timer,NRF_TIMER_CC_CHANNEL0);
}

// Timestamp function for logging
uint32_t time_us(void)
{
	uint32_t time = nrf_drv_timer_capture(&us_timer,NRF_TIMER_CC_CHANNEL0);
	return time;
}


// Timestamp ijn milliseconds
// Uses a register to increase maximum number of ms by a factor of 1000.  Means we can count entire battery life in ms.
// In us at 32 bit integer, max you can get is 4294 seconds (1 hour 11 mins).  Now we are up at 1000 hours.
uint32_t time_ms(void)
{
	uint32_t time = nrf_drv_timer_capture(&us_timer,NRF_TIMER_CC_CHANNEL0);
        accumulated_us+=(nrf_drv_timer_capture(&us_timer,NRF_TIMER_CC_CHANNEL0)-last_time_us);
        last_time_us=time;
	return accumulated_us/1000;
}



