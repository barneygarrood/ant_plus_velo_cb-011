
#ifndef VELO_CALIBRATION_H__
#define VELO_CALIBRATION_H__

#include "velo_config.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"

static uint8_t velo_cal_nrows;
static uint8_t velo_cal_ncols;
static float * velo_cal_cxdiff;
static float * velo_cal_x;
static float ** velo_cal_angle;
static float ** velo_cal_pdyn;

void velo_load_calibration();


#endif //VELO_CALIBRATION_H__
