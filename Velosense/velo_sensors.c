#include "velo_sensors.h"

#include <math.h>

// ################################################## Velosense data objects ##################################################
// SDP array:
static sdp3x_sensor_t sdp3x_sensors[TWI_INSTANCE_COUNT];
// TDK array:
static tdk_icp_sensor_t tdk_icp_sensors[TWI_INSTANCE_COUNT];

// Pointer to callback function
velo_sensor_evt_t parent_evt_handler;

velo_sensors_t velo_sensors;
velo_data_t velo_data;

static uint8_t bat_read = false;

// ################################################## TWI variables ##################################################
#define BUFFER_SIZE  22
static uint8_t m_buffer[2][BUFFER_SIZE];
static nrf_twi_mngr_transfer_t tdk_start_meas_trans[4];

// ################################################## Velosense calibration function ##################################################
/*
// Function to calculate dynamic pressure and angle from the two differential pressures:
*/
void velo_calibration()
{
  double p1 = (double) velo_data.sdp_1/60.0;
  double p2 = (double) velo_data.sdp_2/60.0;
  if (p1<0)p1=0;
  if (p2<0)p2=0;
  double x = pow( ( sqrt( (double) p1 ) + sqrt( (double) p2 ) ) /2.0 ,2.0);
  double cx = (double)(p1 - p2)/x;
  int x_index = (int) floor((x - velo_cal_x[0]) / (velo_cal_x[1] - velo_cal_x[0]));
  int cx_index = (int) floor((cx - velo_cal_cxdiff[0]) / (velo_cal_cxdiff[1]-velo_cal_cxdiff[0]));
  if (x_index < 0) x_index = 0;
  if (cx_index<0) cx_index = 0;
  if (x_index > velo_cal_nrows-2) x_index = velo_cal_nrows-2;
  if (cx_index > velo_cal_ncols-2) cx_index = velo_cal_ncols-2;
  
  //bilinear interpolation:
  //do angle first:
  double x1 = velo_cal_angle[x_index][cx_index] + (x - velo_cal_x[x_index]) / (velo_cal_x[x_index+1] - velo_cal_x[x_index]) * (velo_cal_angle[x_index+1][cx_index] - velo_cal_angle[x_index][cx_index]);
  double x2 = velo_cal_angle[x_index][cx_index+1] + (x - velo_cal_x[x_index]) / (velo_cal_x[x_index+1] - velo_cal_x[x_index]) * (velo_cal_angle[x_index+1][cx_index+1] - velo_cal_angle[x_index][cx_index+1]);
  velo_data.angle = (float)(x1 + (cx - velo_cal_cxdiff[cx_index]) / (velo_cal_cxdiff[cx_index+1] - velo_cal_cxdiff[cx_index]) * (x2 - x1));
  //then dynamic pressure
  x1 = velo_cal_pdyn[x_index][cx_index] + (x - velo_cal_x[x_index]) / (velo_cal_x[x_index+1] - velo_cal_x[x_index]) * (velo_cal_pdyn[x_index+1][cx_index] - velo_cal_pdyn[x_index][cx_index]);
  x2 = velo_cal_pdyn[x_index][cx_index+1] + (x - velo_cal_x[x_index]) / (velo_cal_x[x_index+1] - velo_cal_x[x_index]) * (velo_cal_pdyn[x_index+1][cx_index+1] - velo_cal_pdyn[x_index][cx_index+1]);
  velo_data.pdyn = (float)(x1 + (cx - velo_cal_cxdiff[cx_index]) / (velo_cal_cxdiff[cx_index+1] - velo_cal_cxdiff[cx_index]) * (x2 - x1));
}


// ################################################## Sensor initialise functions ##################################################

void velo_sensors_init(velo_sensor_evt_t evt_handler)
{
  // Initialise twi driver:
  twi_init_all();

  //Initialise sensors:
  sdp3x_sensors_init(TWI_INSTANCE_COUNT,sdp3x_sensors);
  tdk_icp_init_all(TWI_INSTANCE_COUNT,tdk_icp_sensors,TDK_ICP_OSR,tdk_start_meas_trans);
  // Put in a small delay to make sure we don't try and read sensors before they are ready;
  nrf_delay_ms(10);
  parent_evt_handler= evt_handler;
}


// ################################################## Sensor read functions ##################################################

/*
// callback function for restarting TDK read.
*/
void tdk_start_meas_cb(ret_code_t result, void * p_user_data)
{
  uint8_t chan = *(uint8_t *)p_user_data;
  //Set tdk time etc:
  tdk_icp_sensors[chan].read_requested = 1;
  tdk_icp_sensors[chan].last_request_us=time_us();
}

uint8_t twi_cb[] = {0,1};

/*
/ callback function for when data comes back from twi.
*/
static void velo_sensors_process(ret_code_t result, void * p_user_data)
{
//  NRF_LOG_INFO("VELO READ-CB START");
//  NRF_LOG_FLUSH();

  uint8_t twi_instance = *(uint8_t *) p_user_data;
  twi_status[twi_instance]=0;
  twi_event_time[twi_instance]=time_us();
  velo_sensors=velo_sensors_none;
  switch (twi_instance)
  {
    case 0:
      velo_twi_ctrl |= velo_twi_ctrl_0;
      break;
    case 1:
      velo_twi_ctrl |= velo_twi_ctrl_1;
      break;
  }

  // Get sdp data
  sdp3x_get_data_from_raw(&sdp3x_sensors[twi_instance],&m_buffer[twi_instance][0]);

  // Get battery data:
  if ((twi_instance == BAT_GAUGE_TWI_INSTANCE) && bat_read)
  {
    velo_data.bat_soc = (uint8_t)uint16_decode(&m_buffer[twi_instance][18]);
    velo_data.bat_voltage = uint16_decode(&m_buffer[twi_instance][20]);
    bat_read = false;
    velo_sensors|=velo_sensors_bat_gauge;
    

  }

  // Get tdk data - only if a measurement was startedL
  if (tdk_icp_sensors[twi_instance].read_started==1)
  {
    tdk_icp_sensors[twi_instance].read_requested = 0;
    tdk_icp_sensors[twi_instance].read_started = 0;
    // Setup next tdk read
    twi_schedule_transfers(&tdk_start_meas_trans[twi_instance], 1, tdk_start_meas_cb, &twi_cb[twi_instance]);
    tdk_get_data_from_raw(&tdk_icp_sensors[twi_instance],&m_buffer[twi_instance][9]);
    velo_data.baro_temp_1 = tdk_icp_sensors[0].temperature;
    velo_data.baro_press_1 = tdk_icp_sensors[0].pressure;
    velo_data.baro_temp_2 = tdk_icp_sensors[1].temperature;
    velo_data.baro_press_2 = tdk_icp_sensors[1].pressure;
  }

  //if we have done both twi channels, then process and return to velo_driver:
  if ((velo_twi_ctrl & velo_twi_ctrl_0) && (velo_twi_ctrl & velo_twi_ctrl_1))
  {

    velo_data.sdp_1=sdp3x_sensors[0].data.pressure;
    velo_data.sdp_2=sdp3x_sensors[1].data.pressure;
    //average the two baro temps:
    velo_data.baro_temp=(velo_data.baro_temp_1+velo_data.baro_temp_2)/2;
    //Return to parent:
    parent_evt_handler();
  }
}

/*
// Function for setting up twi transfers to read data from sensors.
*/
void velo_sensors_read(uint8_t read_bat)
{
//  NRF_LOG_INFO("VELO READ START");
//  NRF_LOG_FLUSH();
  uint8_t tdk_error=0;
  uint8_t transfer_count[2] = {0};
  static nrf_twi_mngr_transfer_t trans[2][6];
  // Setup transactions to read tdk and sdp data on each twi channel.
  // This is done using teh twi manager which schedules those read requests and returns on a call-back, to avoid blocking the code while
  // waiting.
  velo_sensors = velo_sensors_none;
  velo_twi_ctrl = velo_twi_ctrl_none;
  bat_read=false;
  for (int twi_instance=0;twi_instance<TWI_INSTANCE_COUNT;twi_instance++)
  {
    // Two transfers, one for sdp (9 bytes) and one for tdk (9 bytes), and potentially another 4 bytes for battery gauge.
    // ############################################### TO DO: MOVE THIS TO A COMMON BLOCK - NO NEED TO KEEP REDEFINING!!! ######################
    sdp3x_get_data_transfer(&sdp3x_sensors[twi_instance],&m_buffer[twi_instance][0],&trans[twi_instance][0]);
    transfer_count[twi_instance]++;

    // Add in a read request for battery sensor, if required and on correct twi channel.
    // Note as this reads state of charge and voltage, it is two transfers:
    if ((twi_instance == BAT_GAUGE_TWI_INSTANCE) && read_bat)
    {
      bat_gauge_get_soc_transfer(&m_buffer[twi_instance][18],&trans[twi_instance][transfer_count[twi_instance]]);
      transfer_count[twi_instance]+=2;
      bat_gauge_get_voltage_transfer(&m_buffer[twi_instance][20],&trans[twi_instance][transfer_count[twi_instance]]);
      transfer_count[twi_instance]+=2;
      bat_read=true;
    }

    //Can only request data from tdk if we have requested a measurement.
    if (tdk_icp_sensors[twi_instance].read_requested==1)
    {
      // ... and waited long enough for sensor to take a measurement.  Register an error if we haven't waited long enough.
      uint16_t delay_ms=(uint16_t)(tdk_min_meas_time_ms - (time_us() - tdk_icp_sensors[twi_instance].last_request_us)/1000.0f);
      if (delay_ms > 0)
      {
        tdk_icp_sensors[twi_instance].error=1;
      }
      else
      {
        tdk_icp_sensors[twi_instance].error=0;
      }
    }
    else  
    {
      tdk_icp_sensors[twi_instance].error=2;
    }
    if (tdk_icp_sensors[twi_instance].error == 0)
    { 
      // Send both SDP and TDK read requests:
      // ############################################### TO DO: MOVE THIS TO A COMMON BLOCK - NO NEED TO KEEP REDEFINING!!! ######################
      tdk_get_meas_transfer(&tdk_icp_sensors[twi_instance],&m_buffer[twi_instance][9],&trans[twi_instance][transfer_count[twi_instance]]);
      transfer_count[twi_instance]++;
      tdk_icp_sensors[twi_instance].read_started=1;
    }
    else
    {
      //NRF_LOG_INFO("TDK error = %u",tdk_icp_sensors[twi_instance].error);
      // Just send SDP read request:
      tdk_icp_sensors[twi_instance].read_started=0;
    }

    // Send transfers:
    twi_schedule_transfers(trans[twi_instance],transfer_count[twi_instance],velo_sensors_process, &twi_cb[twi_instance]);
    twi_status[twi_instance]=1;
    twi_event_time[twi_instance]=time_us();
  }
//  NRF_LOG_INFO("VELO READ END");
//  NRF_LOG_FLUSH();
}