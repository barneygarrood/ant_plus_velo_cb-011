#ifndef COKSTOCKS_H_
#define COKSTOCKS_H_

#include <stdio.h>
#include "nrf_twi_mngr.h"


/* TWI instance ID. */
#define TWI_INSTANCE_ID_0     0
#define TWI_INSTANCE_ID_1     1
#define MAX_PENDING_TRANSACTIONS    5

extern uint32_t twi_event_time[];
extern uint8_t twi_status[];

    
void twi_init_all();
void twi_uninit_all();
void twi_schedule_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t transfer_count, nrf_twi_mngr_callback_t cb_function, uint8_t * p_twi_id);
void twi_perform_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t trans_count, uint8_t p_twi_id);

void twi_write_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans);
void twi_read_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans);
void twi_read_register_transfer(uint8_t twi_address, uint8_t * p_reg_addr, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans);

#endif //TWI_SCANNER_H__
