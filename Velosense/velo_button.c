
#include "velo_button.h"
#include "twi_scanner.h"


static nrf_drv_pwm_t m_pwm = NRF_DRV_PWM_INSTANCE(0);		//PWM instance:
static nrf_pwm_values_common_t simple_flash[] = {0,2,5,10,20,10,5,2,0};
static nrf_pwm_values_individual_t seq_values[128];
static uint8_t tap_count=0;
static uint32_t tap_start_time=0;

bool velo_power_connected=true;
bool velo_is_pairing=false;
bool velo_buthold=false;
bool velo_low_bat=false;
bool velo_shutdown_warning=false;
bool velo_butt_period_1=false;
bool velo_butt_period_2=false;

//Timer object to run button timer off. 
//Enables us to have different actions depending on press duration.
APP_TIMER_DEF(m_button_tick_timer);

//static bool turn_off = false;
//stat bool velo_start_dfu = false;

static void flash_pattern_off()
{
  //All off.
  for (int i=0;i<128;i++)
  {
    seq_values[i].channel_0=0;
    seq_values[i].channel_1=0;
    seq_values[i].channel_2=0;
  }
}

static void flash_pattern_blue()
{
  if (velo_low_bat)
  {
    //Just sets LED 0 to flash (red), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_1=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_1=0;
      }
      seq_values[i].channel_0=0;
      seq_values[i].channel_2=0;
    }
  }
  else
  {
    //Just sets LED 1 to flash (blue), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_0=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_0=0;
      }
      seq_values[i].channel_1=0;
      seq_values[i].channel_2=0;
    }
  }
}

static void flash_pattern_lilac()
{
  if (velo_low_bat)
  {
    //Just sets LED 1 to flash (red), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_1=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_1=0;
      }
      seq_values[i].channel_0=0;
      seq_values[i].channel_2=0;
    }
  }
  else
  {
    //Just sets LED 0 and 1 to flash (blue + red = purps?), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_0=simple_flash[i];
        seq_values[i].channel_1=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_0=0;
        seq_values[i].channel_1=0;
      }
      seq_values[i].channel_2=0;
    }
  }
}

static void flash_pattern_yellow()
{
  if (velo_low_bat)
  {
    //Just sets LED 1 to flash (red), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_1=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_1=0;
      }
      seq_values[i].channel_0=0;
      seq_values[i].channel_2=0;
    }
  }
  else
  {
    //Just sets LED 0 and 1 to flash (blue + red = purps?), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_1=simple_flash[i];
        seq_values[i].channel_2=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_1=0;
        seq_values[i].channel_2=0;
      }
      seq_values[i].channel_0=0;
    }
  }
}

static void flash_pattern_red()
{
  if (velo_low_bat)
  {
    //Just sets LED 1 to flash (red), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_1=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_1=0;
      }
      seq_values[i].channel_0=0;
      seq_values[i].channel_2=0;
    }
  }
  else
  {
    //Just sets LED 0 and 1 to flash (blue + red = purps?), others are off:
    for (int i=0;i<128;i++)
    {
      //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
      if (i<9)
      {
        seq_values[i].channel_1=simple_flash[i];
      }
      else
      {
        seq_values[i].channel_1=0;
      }
      seq_values[i].channel_0=0;
      seq_values[i].channel_2=0;
    }
  }
}

static void flash_pattern_buthold()
{
  //Just sets LED 1 to flash, others are off:
  uint8_t ctr=0;
  bool fl=true;
  for (int i=0;i<128;i++)
  {
    //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
    
    seq_values[i].channel_1=20*ctr/32.0;
    fl? ctr++:ctr--;
    if (ctr>=8)
    {
      fl=false;
    }
    else if (ctr<=0)
    {
      fl=true;
    }
    seq_values[i].channel_0=0;
    seq_values[i].channel_2=0;
  }
}

static void flash_pattern_enter_dfu()
{
  //Just sets LED 1 to flash, others are off:
  uint8_t ctr=0;
  bool fl=true;
  for (int i=0;i<128;i++)
  {
    //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
    
    seq_values[i].channel_0=20*ctr/32.0;
    seq_values[i].channel_2=20*ctr/32.0;
    fl? ctr++:ctr--;
    if (ctr>=8)
    {
      fl=false;
    }
    else if (ctr<=0)
    {
      fl=true;
    }
    seq_values[i].channel_1=0;
  }
}

static void flash_pattern_green()
{
  //Just sets LED 1 to flash, others are off:
  for (int i=0;i<128;i++)
  {
    //if (i<sizeof(simple_flash)/sizeof(simple_flash[0]))
    if (i<9)
    {
      seq_values[i].channel_2=simple_flash[i];
    }
    else
    {
      seq_values[i].channel_2=0;
    }
    seq_values[i].channel_0=0;
    seq_values[i].channel_1=0;
  }
}

static void flash_pattern_solid_green()
{
  //Solid Green
  for (int i=0;i<128;i++)
  {
    seq_values[i].channel_0=0;
    seq_values[i].channel_1=0;
    seq_values[i].channel_2=20;
  }
}

static void flash_pattern_solid_red()
{
  //Solid Green
  for (int i=0;i<128;i++)
  {
    seq_values[i].channel_0=0;
    seq_values[i].channel_1=20;
    seq_values[i].channel_2=0;
  }
}

static void flash_pattern_solid_amber(void)
{
  //Solid Green
  for (int i=0;i<128;i++)
  {
    seq_values[i].channel_0=0;
    seq_values[i].channel_1=20;
    seq_values[i].channel_2=20;
  }
}

//Set LED config based on what the current status is:
void velo_set_led_pattern(void)
{
  //Starter for 10: is charge plugged in:
  if (velo_power_connected)
  {
    if (velo_buthold)
    {
      flash_pattern_buthold();
    }
    else
    {
      if (velo_is_pairing)
      {
        flash_pattern_green();
      }
      else
      {
        if (!nrf_gpio_pin_read(PIN_STAT_2))
        {
          if(nrf_gpio_pin_read(PIN_STAT_1))
          {
            //Charge complete
           flash_pattern_solid_green();
          }
        }
        else if (!nrf_gpio_pin_read(PIN_STAT_1))
        {
          //Charging
          flash_pattern_solid_amber();
        }else
        {
          //Problem with the charger..
          flash_pattern_solid_red();
        }
      }
    }
  }else
  {
//We need to have some sort of status enumeration here..
    if (velo_buthold)
    {
      flash_pattern_buthold();
    }
    else if (velo_is_pairing)
    {
      flash_pattern_green();
    }
    else if (velo_shutdown_warning)
    {
      flash_pattern_red();
    }
    else
    {
      switch (ant_velo_period_index)
      {
        case 0: // 4091/32768
          flash_pattern_blue();
          break;
        case 13:
          flash_pattern_lilac();
          break;
      }
    }
  }
}


void velo_check_charge_status(void)
{
  //Pin is low on plugged in or high otherwise.
  if (nrf_gpio_pin_read(PIN_PG))
  {
    //not connected:
    velo_power_connected=false;
  }
  else
  {
    velo_power_connected=true;
  }
  velo_set_led_pattern();
}

//Handler for when charging status changes
static void velo_status_change_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
  //Either we are plugging in or unplugging.
  velo_check_charge_status();
}

// Initialise LEDs
void velo_led_init(void)
{
    //Configure PWM:
    nrf_drv_pwm_config_t const pwm_cfg =
    {
        .output_pins =
        {
          PIN_LED1 | NRF_DRV_PWM_PIN_INVERTED,		// channel 0
          PIN_LED2 | NRF_DRV_PWM_PIN_INVERTED,          // channel 1
          PIN_LED3 | NRF_DRV_PWM_PIN_INVERTED,          // channel 2
          NRF_DRV_PWM_PIN_NOT_USED,                     // channel 3
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = 100,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };
    APP_ERROR_CHECK(nrf_drv_pwm_init(&m_pwm, &pwm_cfg, NULL));
		
    nrf_pwm_sequence_t const pwm_seq0 =
    {
        .values.p_common = (nrf_pwm_values_common_t *) seq_values,
        .length          = 128,
        .repeats         = 200,
        .end_delay       = 0,
    };
    (void) nrf_drv_pwm_simple_playback(&m_pwm,&pwm_seq0,1,NRF_DRV_PWM_FLAG_LOOP);
}

static void shutdown_handler(void * p_event_data, uint16_t event_size)
{
  sd_power_system_off();
}

/*
// callback function for twi shutdown commands
*/
void velo_shutdown_cb(ret_code_t result, void * p_user_data)
{
  //Unninitialise PWM (which controls LEDs - otherwise if shutdown exactly during a flash they can stay on).
  nrf_drv_pwm_uninit(&m_pwm);
  //uninitialise twi:
  twi_uninit_all();
  nrf_gpio_pin_write(PIN_REG, 0);
  nrf_gpio_pin_write(PIN_TS, 0);
  //Go to sleep..
  //sd_power_system_off();
  //Need to put into scheduler:
  app_sched_event_put(NULL,0,shutdown_handler);
}



void velo_switch_off()
{

  //Uninitialise plugin and charge status detection
  nrf_drv_gpiote_in_uninit(PIN_PG);
  nrf_drv_gpiote_in_uninit(PIN_STAT_1);
  nrf_drv_gpiote_in_uninit(PIN_STAT_2);

  //Turn off battery gauge:
  static nrf_twi_mngr_transfer_t trans[2];
  static uint8_t twi_instance = BAT_GAUGE_TWI_INSTANCE;
  bat_gauge_enter_shutdown_transfer(trans);
  twi_schedule_transfers(trans,2,velo_shutdown_cb,&twi_instance);

}

void velo_enter_dfu(void * p_event_data, uint16_t event_size)
{
  sd_power_gpregret_set(0,BOOTLOADER_DFU_START);
  sd_nvic_SystemReset();   

}

// Following allows button event handler to be scheduled, which prevents conflicts with things running in different contexts.
typedef struct 
{
  uint8_t button;
  uint8_t action;
} button_event_t;

static void velo_button_handler(uint8_t button, uint8_t action)
{
  uint32_t err_code;
  switch(action) 
  {
    case APP_BUTTON_PUSH:
      if (velo_is_pairing)
      {
        velo_stop_pairing();

        app_timer_start(m_button_tick_timer, APP_TIMER_TICKS(BUTTON_OFF_MS), NULL);
        velo_buthold=true;
        velo_check_charge_status();
      }
      else
      {
      //Start pairnig mode:
        velo_start_pairing();
        app_timer_start(m_button_tick_timer, APP_TIMER_TICKS(BUTTON_OFF_MS), NULL);
        velo_buthold=true;
        velo_check_charge_status();
      }
      break;
  
    case APP_BUTTON_RELEASE:
      if (velo_buthold)
      {
        err_code = app_timer_stop(m_button_tick_timer);
        APP_ERROR_CHECK(err_code);
        velo_buthold=false;
        if (velo_butt_period_1)
        {
          velo_switch_off();
        } 
        else
        {
          velo_check_charge_status();
        }
      }
      //Deal with button count:
      uint32_t time=time_us();
      if ((time-tap_start_time)>TAP_TIME_MS*1000)
      {
        tap_count=0;
        tap_start_time=time;
      }
      tap_count++;
      if (tap_count == BOOTLOADER_TAP_COUNT)
      {
        app_sched_event_put(NULL,0,velo_enter_dfu);
      }
      // Started to do this but then backed out of it.  Leave in case we come back.
      // Idea was to have ANT period change on a button press rather than via radio.
      else if (tap_count == PERIOD_TOGGLE_TAP_COUNT)
      {
        // Turn off pairing mode.
        velo_stop_pairing();
        app_sched_event_put(NULL,0,velo_toggle_ant_period);
      }
      NRF_LOG_INFO("Button taps = %u",tap_count);
    break;
  }
}

static void velo_button_timer_handler(void * p_context)
{
    //Turn off LED to say going to sleep, but we dont actually shut off until button unpressed:
    if (velo_butt_period_1)
    {
      flash_pattern_enter_dfu();
      velo_butt_period_2=true;

    }
    else
    {
      flash_pattern_off();
      velo_butt_period_1=true;
      //Restart timer for enter dfu mode:
      app_timer_start(m_button_tick_timer, APP_TIMER_TICKS(BUTTON_OFF_MS), NULL);
    }
}

void velo_button_init(void)
{
  uint32_t err_code;
  NRF_LOG_INFO("velo_buton_init");
  //Setup the power button:
  static app_button_cfg_t pwr_buttons[] = {{PIN_SW, APP_BUTTON_ACTIVE_LOW, NRF_GPIO_PIN_NOPULL, velo_button_handler}};
  err_code = app_button_init((app_button_cfg_t *)pwr_buttons,1,BUTTON_DETECTION_DELAY);
  APP_ERROR_CHECK(err_code);
  err_code = app_button_enable();
  APP_ERROR_CHECK(err_code);

  //Start timer to handle the power button, (power down if hold for more than x seconds)
  err_code = app_timer_create(&m_button_tick_timer, APP_TIMER_MODE_SINGLE_SHOT, velo_button_timer_handler);
  APP_ERROR_CHECK(err_code);


  //Setup handler for when unit is plugged into power.
  nrf_drv_gpiote_in_config_t pg_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
  pg_config.pull = NRF_GPIO_PIN_PULLUP;
  err_code = nrf_drv_gpiote_in_init(PIN_PG, &pg_config, velo_status_change_handler);

  APP_ERROR_CHECK(err_code);
  nrf_drv_gpiote_in_event_enable(PIN_PG, true);

  err_code = nrf_drv_gpiote_in_init(PIN_STAT_1, &pg_config, velo_status_change_handler);
  APP_ERROR_CHECK(err_code);
  nrf_drv_gpiote_in_event_enable(PIN_STAT_1, true);

  err_code = nrf_drv_gpiote_in_init(PIN_STAT_2, &pg_config, velo_status_change_handler);
  APP_ERROR_CHECK(err_code);
  nrf_drv_gpiote_in_event_enable(PIN_STAT_2, true);

  velo_check_charge_status();
  velo_led_init();

}
