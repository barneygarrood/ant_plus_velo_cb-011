#include "velo_settings.h"

// Static 1D array containing all 128 bytes of settings:
static uint8_t * s_velo_config = NULL;
static uint8_t s_active_block;

static const uint8_t n_blocks = (VELO_FLASH_END_ADDR-VELO_FLASH_START_ADDR+1)/VELO_CONFIG_BLOCK_SIZE;

static void get_default_settings(uint16_t * p_velo_settings)
{
  p_velo_settings[0] = VELO_DEFAULT_ELEVATION;
  p_velo_settings[1] = VELO_DEFAULT_CRR;
  p_velo_settings[2] = VELO_DEFAULT_TIME_AV_PERIOD; 
  p_velo_settings[3] = VELO_DEFAULT_TIME_AV_MIDPT;
  p_velo_settings[4] = VELO_DEFAULT_RIDER_WEIGHT;
  p_velo_settings[5] = VELO_DEFAULT_BIKE_WEIGHT;
  p_velo_settings[6] = VELO_DEFAULT_WHEEL_CIRC;
  p_velo_settings[7] = VELO_DEFAULT_CTF_CAL;
  p_velo_settings[8] = VELO_DEFAULT_LATITUDE;
}

static void get_crc()
{
  //Calculate crc:
  uint16_t crc = crc16_compute(&s_velo_config[VELO_SETTINGS_START], VELO_CONFIG_BLOCK_SIZE-VELO_SETTINGS_START, NULL);
  // Put crc into config array:
  memcpy(&s_velo_config[VELO_CONFIG_CRC_LOC], &crc, 2);
}

static void initialise_config()
{
  // Initialise array - this needsd to be word aligned so have to use malloc function:
  if (s_velo_config == NULL) 
  {
    s_velo_config = malloc(VELO_CONFIG_BLOCK_SIZE);
  }
  NRF_LOG_INFO("n_blocks = %u",n_blocks);
  memset(s_velo_config,0xFF,VELO_CONFIG_BLOCK_SIZE);
  uint16_t p_velo_settings[VELO_SETTINGS_COUNT];

  // Get default values:
  get_default_settings(p_velo_settings);
  // Copy over to config array:
  memcpy(&s_velo_config[VELO_SETTINGS_START],p_velo_settings,VELO_SETTINGS_COUNT*2);

  //Calculate crc:
  get_crc();

  //Put in header byte:
  velo_config_header_t * p_header = (velo_config_header_t *) &s_velo_config[0];
  p_header->uninitialised=0;
  p_header->active = 1;
  p_header->counter = 0;
  s_active_block=0;
  //Erase any old data to be sure we start blank:
  velo_flash_erase();

  //Write data:
  NRF_LOG_INFO("Source address = 0x%x)",(uint32_t)s_velo_config);
  NRF_LOG_FLUSH();
  velo_flash_write(s_active_block * VELO_CONFIG_BLOCK_SIZE, s_velo_config, VELO_CONFIG_BLOCK_SIZE);
  NRF_LOG_INFO("Settings initialised.");
}

static void save_config(bool increment_counter)
{
  // Deactivate old settings.
  // Assume that header block hasn't changed, so we just dactivate, and save that 4-byte block.
  velo_config_header_t * p_header = (velo_config_header_t *) &s_velo_config[0];

  //Deactivate
  p_header->active = 0;
  //Write back the deactivated block
  velo_flash_write(s_active_block * VELO_CONFIG_BLOCK_SIZE, s_velo_config, 8);

  // Find new block location.
  s_active_block+=1;

  // Erase page if we are at end of it, and reset active_block variable:
  if (s_active_block>=n_blocks)
  {
    velo_flash_erase();
    s_active_block=0;
    NRF_LOG_INFO("Settings page erased.");
  }

  //Calcualte crc:
  get_crc();

  // Update rest of header:
  p_header->active = 1;
  if (increment_counter) {p_header->counter += 1;}  // Note that this rolls-over automatically in same way as in aero profile.
  p_header->uninitialised = 0;  // Should already be like this, but just to be sure..
  
  //Write data:
  NRF_LOG_INFO("About to write %u bytes to address 0x%x",VELO_CONFIG_BLOCK_SIZE,s_active_block * VELO_CONFIG_BLOCK_SIZE);
  velo_flash_write(s_active_block * VELO_CONFIG_BLOCK_SIZE,s_velo_config,VELO_CONFIG_BLOCK_SIZE);
  NRF_LOG_INFO("Settings saved.");
}



void velo_load_config()
{
  // Initialise array - this needsd to be word aligned so have to use malloc function:
  if (s_velo_config == NULL) 
  {
    s_velo_config = malloc(VELO_CONFIG_BLOCK_SIZE);
  }

  // Read 128 byte settings.
  // Check for active.  If not active, load next set.
  // If we get through all 32 blocks and find no active, then need to initialise first block.
  s_active_block = 255;

  // Create pointer to header block:
  velo_config_header_t * p_header = (velo_config_header_t *) &s_velo_config[0];
  
  for (int i=0;i<n_blocks;i++)
  {
    velo_flash_read(i * VELO_CONFIG_BLOCK_SIZE, s_velo_config, VELO_CONFIG_BLOCK_SIZE);
     NRF_LOG_INFO("Block %u header = 0x%x",i,s_velo_config[0]);
    if (!p_header->uninitialised & p_header->active)
    {
      s_active_block=i;
      
      NRF_LOG_INFO("Active config found = %u.",s_active_block);
      break;
    }
  }
  // If active block found, then we're done as array already copied over.
  if (s_active_block >= n_blocks)
  {
    NRF_LOG_INFO("No active config found: Initialising config.");
    initialise_config();
  }
}
 
void velo_settings_get(uint16_t * p_velo_settings)
{
  // Gets settings, returning to the given array.
  // See Proprietary_Packet_Layout_vX.x.xls for ordering.
  // If values aren't initialised, then this routine writes defaults to memory.
  
  // Assume settings already loaded into the static 1D array, and just copy across:
  // Convert to 16 bit integers:
  memcpy(p_velo_settings,&s_velo_config[VELO_SETTINGS_START],VELO_SETTINGS_COUNT*2);
}

void velo_settings_set(uint16_t * p_velo_settings)
{
  memcpy(&s_velo_config[VELO_SETTINGS_START],p_velo_settings,VELO_SETTINGS_COUNT*2);
  save_config(true);
}


// -------------------------------------------------------------------------------------------------------
// Saved peripheral device handling - load and save.
// -------------------------------------------------------------------------------------------------------
static void load_sensor(ant_aero_peripheral_device_t * p_device, bool is_speed)
{
  // Get raw setting:
  uint8_t starting_pos = is_speed ? VELO_SPD_DEVICE_SETTINGS_START : VELO_PWR_DEVICE_SETTINGS_START;
  uint8_t device_settings[7];
  memcpy(device_settings,&s_velo_config[starting_pos],7);

  // Convert to device type:
  p_device->transmission_type.byte = device_settings[0];
  p_device->device_type=device_settings[1];
  p_device->device_number = uint16_decode(&device_settings[2]);
  p_device->rf_freq = device_settings[4];
  p_device->channel_period = uint16_decode(&device_settings[5]);
  p_device->channel_state.bitfields.saved=true;
}

void velo_load_pwr_sensor(ant_aero_peripheral_device_t * p_device)
{
  load_sensor(p_device,false);
}

void velo_load_spd_sensor(ant_aero_peripheral_device_t * p_device)
{
  load_sensor(p_device,true);
}

static void save_sensor(ant_aero_peripheral_device_t * p_device, bool is_speed)
{
  // Get raw setting:
  uint8_t starting_pos = is_speed ? VELO_SPD_DEVICE_SETTINGS_START : VELO_PWR_DEVICE_SETTINGS_START;

  uint8_t device_settings[7];
  // Convert to device type:
  device_settings[0] = p_device->transmission_type.byte;
  device_settings[1] = p_device->device_type;
  UNUSED_PARAMETER(uint16_encode(p_device->device_number,&device_settings[2]));
  device_settings[4] = p_device->rf_freq;
  UNUSED_PARAMETER(uint16_encode(p_device->channel_period,&device_settings[5]));

  // Put into settings array and save to flash.
  memcpy(&s_velo_config[starting_pos],device_settings,7);
  save_config(false);

  //Change flag to show this device has been saved.
  p_device->channel_state.bitfields.saved=true;
}

void velo_save_pwr_sensor(ant_aero_peripheral_device_t * p_device)
{
  save_sensor(p_device, false);
}

void velo_save_spd_sensor(ant_aero_peripheral_device_t * p_device)
{
  save_sensor(p_device, true);
}

// -------------------------------------------------------------------------------------------------------
// Config id handling - load and save.
// -------------------------------------------------------------------------------------------------------


void velo_get_cids(velo_controller_id_t * p_cids)
{
  //We just shunt the data in the already-loaded settings array into the cids array:
  for (int i=0;i<VELO_CID_COUNT;i++)
  {
    memcpy(&p_cids[i].serial_number,&s_velo_config[VELO_CID_START+i*VELO_CID_SIZE],4);
    p_cids[i].cid=s_velo_config[VELO_CID_START+i*VELO_CID_SIZE+4];
  }
}

void velo_put_cids(velo_controller_id_t * p_cids)
{
  //We just shunt the data in the already-loaded settings array into the cids array:
  for (int i=0;i<VELO_CID_COUNT;i++)
  {
    memcpy(&s_velo_config[VELO_CID_START+i*VELO_CID_SIZE],&p_cids[i].serial_number,4);
    s_velo_config[VELO_CID_START+i*VELO_CID_SIZE+4] = p_cids[i].cid;
  }
  save_config(false);
}

void velo_get_create_cid(velo_controller_id_t * p_controller_id)
{    
    // Log execution mode.
    if (current_int_priority_get() == APP_IRQ_PRIORITY_THREAD)
    {
        NRF_LOG_INFO("velo_get_create_cid executing in thread/main mode.");
    }
    else
    {
        NRF_LOG_INFO("velo_get_create_cid executing in interrupt handler mode.");
    }
  // Assume that settings are loaded already.  So we check if already exists, if so get the value, otherwise create a new one:
  velo_controller_id_t cids[VELO_CID_COUNT];
  velo_get_cids(cids);
  NRF_LOG_INFO("Looking for CID for device SN# %u",p_controller_id->serial_number);
  NRF_LOG_FLUSH();
  for (int i=0;i<VELO_CID_COUNT;i++)
  {
    NRF_LOG_INFO("Display ID %u ID %u",cids[i].serial_number, cids[i].cid);
  }
  NRF_LOG_FLUSH();
  int8_t found=-1;
  //Does serial number exist already?
  for (int i=0;i<VELO_CID_COUNT;i++)
  {
    if (cids[i].serial_number == p_controller_id->serial_number)
    {
      found=i;
      p_controller_id->cid=cids[i].cid;
      break;
    }
  }
  //If found then move to front of queue:
  if (found>-1)
  {
    if (found>0)
    {
      for (int i=found;i>0;i--)
      {
        cids[i].serial_number=cids[i-1].serial_number;
        cids[i].cid=cids[i-1].cid;
      }
    }
    cids[0].serial_number=p_controller_id->serial_number;
    cids[0].cid=p_controller_id->cid;
    NRF_LOG_INFO("CID for current display unit found.");
    NRF_LOG_FLUSH();
  }
  else
  {
    //if it didn't exist then take the cid of the last one in the register:
    //Assumption is that we fill up the register with up to 7 controllers, starting at 1 and cid up to 7.
    //if one is replaced, then number of last in register is taken then new one put at front of register.
    //This way all numbers should exist, unless register isn't full in which case it is the highest number it came across.
  
    //Is there an empty spot?  If so, fill it.
    for (int i=0;i<VELO_CID_COUNT;i++)
    {
      if (cids[i].cid>VELO_CID_COUNT)   // Initialised value is 255, so true if this slot is not yet initalised.
      {
        found=i+1;
        cids[i].serial_number=p_controller_id->serial_number;
        cids[i].cid=found;
        p_controller_id->cid=cids[i].cid;
        break;
      }
    }
    // If alls pots are taken, take that from the last one in the register, put new one at front, and shuffle everything down one.
    if (found<0)
    {
      p_controller_id->cid  = cids[VELO_CID_COUNT-1].cid;
      for (int i=VELO_CID_COUNT-1;i>0;i--)
      {
        cids[i].serial_number=cids[i-1].serial_number;
        cids[i].cid=cids[i-1].cid;
      }
      cids[0].serial_number=p_controller_id->serial_number;
      cids[0].cid=p_controller_id->cid;
      NRF_LOG_INFO("CID not found for current display.  Creating new one.");
      NRF_LOG_FLUSH();
    }
  }
  //Write back data, only if the cid wasn't already at the front of the queue:
  if (found!=0)
  {
    NRF_LOG_INFO("CIDs changed, so saving config.");
    NRF_LOG_FLUSH();
    velo_put_cids(cids);
    NRF_LOG_INFO("CIDs saved.");
    NRF_LOG_FLUSH();
  }
  NRF_LOG_INFO("Leaving velo_get_create_cid()");
  NRF_LOG_FLUSH();
}