#include "velo_driver.h"
#include "nrf_delay.h"

#include "ant_key_manager.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_gpio.h"

#include "velo_us_timer.h"

#include "velo_sensors.h"
#include "velo_settings.h"


// ############################ STUFF CARRIED OVER FROM ANT BSC EXAMPLE - WILL GET RID OF IT ALL EVENTUALLY #################################
#define WHEEL_CIRCUMFERENCE         2070                                                            /**< Bike wheel circumference [mm] */
#define BSC_EVT_TIME_FACTOR         1024                                                            /**< Time unit factor for BSC events */
#define BSC_RPM_TIME_FACTOR         60                                                              /**< Time unit factor for RPM unit */
#define BSC_MS_TO_KPH_NUM           36                                                              /**< Numerator of [m/s] to [kph] ratio */
#define BSC_MS_TO_KPH_DEN           10                                                              /**< Denominator of [m/s] to [kph] ratio */
#define BSC_MM_TO_M_FACTOR          1000                                                            /**< Unit factor [m/s] to [mm/s] */
#define SPEED_COEFFICIENT           (WHEEL_CIRCUMFERENCE * BSC_EVT_TIME_FACTOR * BSC_MS_TO_KPH_NUM \
                                     / BSC_MS_TO_KPH_DEN / BSC_MM_TO_M_FACTOR)                      /**< Coefficient for speed value calculation */
#define CADENCE_COEFFICIENT         (BSC_EVT_TIME_FACTOR * BSC_RPM_TIME_FACTOR)                     /**< Coefficient for cadence value calculation */

#define APP_ANT_OBSERVER_PRIO       1                                                               /**< Application's ANT observer priority. You shouldn't need to modify this value. */


#include "nrf_log.h"


typedef struct
{
    int32_t acc_rev_cnt;
    int32_t prev_rev_cnt;
    int32_t prev_acc_rev_cnt;
    int32_t acc_evt_time;
    int32_t prev_evt_time;
    int32_t prev_acc_evt_time;
} bsc_disp_calc_data_t;

static bsc_disp_calc_data_t m_speed_calc_data   = {0};
static bsc_disp_calc_data_t m_cadence_calc_data = {0};
static uint16_t velo_settings[VELO_SETTINGS_COUNT] = {255};

static uint32_t calculate_speed(int32_t rev_cnt, int32_t evt_time)
{
    static uint32_t computed_speed   = 0;

    if (rev_cnt != m_speed_calc_data.prev_rev_cnt)
    {
        m_speed_calc_data.acc_rev_cnt  += rev_cnt - m_speed_calc_data.prev_rev_cnt;
        m_speed_calc_data.acc_evt_time += evt_time - m_speed_calc_data.prev_evt_time;

        /* Process rollover */
        if (m_speed_calc_data.prev_rev_cnt > rev_cnt)
        {
            m_speed_calc_data.acc_rev_cnt += UINT16_MAX + 1;
        }
        if (m_speed_calc_data.prev_evt_time > evt_time)
        {
            m_speed_calc_data.acc_evt_time += UINT16_MAX + 1;
        }

        m_speed_calc_data.prev_rev_cnt  = rev_cnt;
        m_speed_calc_data.prev_evt_time = evt_time;

        computed_speed = SPEED_COEFFICIENT *
                         (m_speed_calc_data.acc_rev_cnt  - m_speed_calc_data.prev_acc_rev_cnt) /
                         (m_speed_calc_data.acc_evt_time - m_speed_calc_data.prev_acc_evt_time);

        m_speed_calc_data.prev_acc_rev_cnt  = m_speed_calc_data.acc_rev_cnt;
        m_speed_calc_data.prev_acc_evt_time = m_speed_calc_data.acc_evt_time;
    }

    return (uint32_t)computed_speed;
}

static uint32_t calculate_cadence(int32_t rev_cnt, int32_t evt_time)
{
    static uint32_t computed_cadence = 0;

    if (rev_cnt != m_cadence_calc_data.prev_rev_cnt)
    {
        m_cadence_calc_data.acc_rev_cnt  += rev_cnt - m_cadence_calc_data.prev_rev_cnt;
        m_cadence_calc_data.acc_evt_time += evt_time - m_cadence_calc_data.prev_evt_time;

        /* Process rollover */
        if (m_cadence_calc_data.prev_rev_cnt > rev_cnt)
        {
            m_cadence_calc_data.acc_rev_cnt += UINT16_MAX + 1;
        }
        if (m_cadence_calc_data.prev_evt_time > evt_time)
        {
            m_cadence_calc_data.acc_evt_time += UINT16_MAX + 1;
        }

        m_cadence_calc_data.prev_rev_cnt  = rev_cnt;
        m_cadence_calc_data.prev_evt_time = evt_time;

        computed_cadence = CADENCE_COEFFICIENT *
                        (m_cadence_calc_data.acc_rev_cnt  - m_cadence_calc_data.prev_acc_rev_cnt) /
                        (m_cadence_calc_data.acc_evt_time - m_cadence_calc_data.prev_acc_evt_time);

        m_cadence_calc_data.prev_acc_rev_cnt  = m_cadence_calc_data.acc_rev_cnt;
        m_cadence_calc_data.prev_acc_evt_time = m_cadence_calc_data.acc_evt_time;
    }

    return (uint32_t)computed_cadence;
}

// ############################ Common variables and timers #################################
static uint8_t bat_ctr=0;
uint8_t ant_velo_period_index=0;

// Callback user data - helps to know where callbacks are called from.
static uint8_t callerA=0;
static uint8_t callerB=1;
static uint8_t twi_cb[2] = {0,1};
static uint8_t poll_count;
static uint8_t max_poll_count;

uint8_t log_ctr=0;
						
APP_TIMER_DEF(m_pairing_tick_timer);  // Timer for pairing
APP_TIMER_DEF(m_read_timer);          // Timer for reading data from sensors.

// ############################ Device ID function: ############################ 

static uint16_t deviceID() {
	
	uint64_t result = *((uint64_t*) NRF_FICR->DEVICEADDR);

	// Mask off upper bytes, to match over-the-air length of 6 bytes.
	uint16_t result_x16	= result & MAX_DEVICE_ID;
	return result_x16;
}

static uint32_t serialNo() {
	
	uint64_t result = *((uint64_t*) NRF_FICR->DEVICEADDR);

	// Mask off upper bytes, to match over-the-air length of 6 bytes.
	uint32_t result_x32	= result & MAX_SERIAL_NO;
	return result_x32;
}

// ############################ ANT objects: ############################

//ANT aero object declarations:
void ant_aero_evt_handler(ant_aero_profile_t * p_profile, ant_aero_page_t page);
AERO_CHANNEL_CONFIG_DEF(m_ant_aero, AERO_CHANNEL_NUM, AERO_TRANS_TYPE,ANT_AERO_MSG_DEFAULT_PERIOD, ANTPLUS_NETWORK_NUM);
AERO_PROFILE_CONFIG_DEF(m_ant_aero, ant_aero_evt_handler);
static ant_aero_profile_t m_ant_aero;
NRF_SDH_ANT_OBSERVER(m_ant_aero_observer, ANT_AERO_ANT_OBSERVER_PRIO, ant_aero_stack_evt_handler, &m_ant_aero);
 
// ANT Velosense object declarations:
void ant_velo_evt_handler(ant_velo_profile_t * p_profile, ant_velo_page_t page);
VELO_CHANNEL_CONFIG_DEF(m_ant_velo,VELO_CHANNEL_NUM,VELO_TRANS_TYPE,ANT_PUBLIC_NETWORK_NUM);
VELO_PROFILE_CONFIG_DEF(m_ant_velo,ant_velo_evt_handler);
static ant_velo_profile_t m_ant_velo;
NRF_SDH_ANT_OBSERVER(m_ant_velo_observer, ANT_VELO_ANT_OBSERVER_PRIO, ant_velo_stack_evt_handler, &m_ant_velo);

//ANT Bikepower object declarations:
void ant_bpwr_evt_handler(ant_bpwr_profile_t * p_profile, ant_bpwr_evt_t event);
BPWR_DISP_CHANNEL_CONFIG_DEF(m_ant_bpwr,BPWR_CHANNEL_NUM,BPWR_TRANS_TYPE,BWPR_DEFAULT_DEV_NUM, ANTPLUS_NETWORK_NUM);
BPWR_DISP_PROFILE_CONFIG_DEF(m_ant_bpwr, ant_bpwr_evt_handler);
static ant_bpwr_profile_t m_ant_bpwr;
//NRF_SDH_ANT_OBSERVER(m_ant_bpwr_observer, ANT_BPWR_ANT_OBSERVER_PRIO, ant_bpwr_disp_evt_handler, &m_ant_bpwr);

//ANT speed/cadence object declarations:
void ant_bsc_evt_handler(ant_bsc_profile_t * p_profile, ant_bsc_evt_t event);
BSC_DISP_CHANNEL_CONFIG_DEF(m_ant_bsc, BSC_CHANNEL_NUM, BSC_TRANS_TYPE, BSC_TYPE, BSC_DEFAULT_DEV_NUM, ANTPLUS_NETWORK_NUM, BSC_MSG_PERIOD_4Hz);
BSC_DISP_PROFILE_CONFIG_DEF(m_ant_bsc, ant_bsc_evt_handler);
static ant_bsc_profile_t m_ant_bsc;
//NRF_SDH_ANT_OBSERVER(m_ant_bsc_observer, ANT_BSC_ANT_OBSERVER_PRIO,ant_bsc_disp_evt_handler, &m_ant_bsc);


// ################################################## ANT VELO SENSOR STUFF ##################################################
/*
/   Start a new read timer, if necessary, and read data from device sensors:
*/
void velo_read_all_callback(void * p_context)
{
  uint8_t * p_caller = p_context;
  //NRF_LOG_INFO("Read callback from %u",*p_caller);
  if (VELO_SYNC_TWI_ANT)
  {
    poll_count++;
    // Setup next timer if needed:
    if (poll_count<max_poll_count)
    {
      app_timer_start(m_read_timer, POLL_PERIOD, &callerB);
    }
  }
  bool read_bat=false;
  if (bat_ctr==BAT_CHECK_FREQUENCY)
  {
    read_bat=true;
    bat_ctr=0;
  }
  velo_sensors_read(read_bat);
  bat_ctr++;
}

/*
/   Callback from sensor read function - put data into ant packets, and deal with warnings and shutdown.
*/
void velo_post_read_callback()
{
  // Deal with battery gauge if a measurement was taken:
  if (velo_sensors & velo_sensors_bat_gauge)
  {
      // Update page 12:
    ant_velo_page_12_update(&m_ant_velo.page_12,velo_data.bat_soc, velo_data.bat_voltage, velo_data.error);
    ant_common_page_82_update(&m_ant_aero.page_82,velo_data.bat_voltage,velo_data.bat_soc);
    //Reset error since this is only sent on page 12:
    velo_data.error=0;
    //Reset battery counter (triggers battery measurement every N ant cycles).
    bat_ctr=0;
    if (velo_data.bat_voltage<VELO_LOW_VOLTAGE_SHUTDOWN)
    {
      // Go into shutdown mode.
      velo_switch_off();
    }
    else if (velo_data.bat_voltage<VELO_LOW_VOLTAGE_WARNING)
    {
      if (!velo_low_bat)
      {
        //  Change to red flashing light:
        velo_low_bat=true;
        velo_check_charge_status();
      }
    }
    else 
    {
      if (velo_low_bat)
      {
        velo_low_bat=false;
        velo_check_charge_status();
      }
    }
  }
  
  //Check for inactivity:
  if ((abs(velo_data.sdp_1)>SDP_INACT_THRESHOLD) | (abs(velo_data.sdp_2)>SDP_INACT_THRESHOLD))
  {
    velo_data.time_inact=time_ms();
    if (velo_shutdown_warning)
    {
      velo_shutdown_warning=false;
      velo_set_led_pattern();
    }
  }
  else
  {
    uint32_t time = time_ms();
    //NRF_LOG_INFO("Shutdown warning = %u, Time inact = %u, Time = %u",velo_shutdown_warning,time-velo_data.time_inact,time);
    //NRF_LOG_FLUSH();
    if ((time-velo_data.time_inact) > INACTIVITY_SHUTDOWN_TIMEOUT*1000)
    {
      velo_switch_off();
    }
    else if ((time-velo_data.time_inact) > INACTIVITY_WARNING_TIMEOUT*1000)
    {
      velo_shutdown_warning=true;
      velo_set_led_pattern();
    }
  }

//  if (VELOSENSE_SEND_VELOCITY_ANGLE)
//  {
//    velo_calibration();
//    ant_velo_page_10_update(&m_ant_velo.page_10 , (uint16_t)(velo_data.pdyn * 60.0f), (uint16_t)((velo_data.angle+90.0f)/0.05f) ,(uint16_t)((velo_data.baro_temp+30.0f)*100.0f));
//  }
//  else
//  {
    //Otherwise just send the sdps.
    ant_velo_page_10_update(&m_ant_velo.page_10,(uint16_t)(velo_data.sdp_1+32768),(uint16_t)(velo_data.sdp_2+32768),(uint16_t)((velo_data.baro_temp+30.0f)*100.0f));
//  }
  //######################################  Need to think what to do here - just repeat previous measurement?? Not really correct..  maybe makes no difference?
  ant_velo_page_11_update(&m_ant_velo.page_11,(uint32_t)(velo_data.baro_press_1-30000.0f)/0.005f,(uint32_t)(velo_data.baro_press_2-30000.0f)/0.005f);  
}

// ################################################## ANT VELO STUFF ##################################################

static void velo_start_cid_loop(ant_aero_profile_t * p_profile)
{
  velo_controller_id_t cid;
  cid.serial_number=p_profile->page_89.data;
  velo_get_create_cid(&cid);
  //Put the controller id into the packet:
  p_profile->page_89.controller_id=cid.cid;
  p_profile->page_89.data=cid.serial_number;
  // Setup profile to send next 4 messages page 89:
  p_profile->p_sens_cb->response_count=0;
  p_profile->p_sens_cb->send_p89=true;
}

static void velo_send_req_page_loop(ant_aero_profile_t * p_profile)
{
  bool send_page = false;
  //queue data:
  switch (p_profile->page_70.page_number)
  {
    case (ANT_AERO_PAGE_31):  // Settings page.
    {
      // Only do something if requested frequency is valid:
      if ( ant_aero_frequency_permitted(p_profile->page_70.radio_frequency))
      {
        //Load settings:
        uint16_t velo_settings[VELO_SETTINGS_COUNT];
        velo_settings_get(velo_settings);
         // Calculate total number of bytes:
        uint8_t buffer_size = 8 + VELO_SETTINGS_COUNT * 3;
        // Need to round up to a multiple of 8, so integer number of packets:
        if (buffer_size % 8)
        {
          buffer_size += 8 - (buffer_size % 8);
        }
        uint8_t burst_buffer[buffer_size];
        NRF_LOG_INFO("buffer_size = %u",buffer_size);
        // Start to fill buffer:
        burst_buffer[0]=31u;  // page number
        burst_buffer[1]=0;
        burst_buffer[2] = VELO_SETTINGS_COUNT;
        memset(&burst_buffer[3],0xFF,5);
        for (int i=0;i<VELO_SETTINGS_COUNT;i++)
        {
          burst_buffer[8+3*i] = velo_settings_layout[i];
          UNUSED_PARAMETER(uint16_encode(velo_settings[i], &burst_buffer[8+3*i+1]));
        }
        // Last values are null padding:
        for (int i=8+3*(VELO_SETTINGS_COUNT);i<buffer_size;i++)
        {
          burst_buffer[i]=0x00;
        }
        ant_aero_page_31_fill_buffer(burst_buffer,buffer_size);
        send_page=true;
      }
    }
  }
  //Set flags:
  if (send_page)
  {
    p_profile->p_sens_cb->send_requested_page=true;
    p_profile->p_sens_cb->response_count=0;
  }
}

void ant_aero_evt_handler(ant_aero_profile_t * p_profile, ant_aero_page_t page)
{
  nrf_pwr_mgmt_feed();
  switch (page)
  {
    case ANT_AERO_PAGE_1:
      break;
    case ANT_AERO_PAGE_32:
      // If page was received, then may need to change period:
      if (!p_profile->p_sens_cb->event_is_tx)
      {
        if (p_profile->page_32.enabled_8hz != p_profile->p_sens_cb->enabled_8hz)
        {
          // Swap period:
          // NOTE I think requirement that only can switch from 4Hz to 8Hz but not vice versa?  For now always switch.
          if (p_profile->page_32.enabled_8hz)
          {
            sd_ant_channel_period_set (p_profile->p_chan_config->channel_number, AERO_MSG_8HZ_PERIOD);
          }
          else
          {
            sd_ant_channel_period_set (p_profile->p_chan_config->channel_number, AERO_MSG_4HZ_PERIOD);
          }
          p_profile->p_sens_cb->enabled_8hz = p_profile->page_32.enabled_8hz;
        }
        // Send page 32 back 4 x regardless.
        // Do this by setting page 70
        p_profile->page_70.page_number=32;
        p_profile->page_70.radio_frequency=0xFF;
        p_profile->page_70.page_number=32;
        p_profile->page_70.transmission_response.items.transmit_count=4;
        p_profile->page_70.transmission_response.items.ack_resposne=0;
        velo_send_req_page_loop(p_profile);
      }
      break;
    case ANT_COMMON_PAGE_70:
      // Data page request, only on specific page requests:
      switch (p_profile->page_70.page_number)
      {
        case ANT_AERO_PAGE_1:
        case ANT_AERO_PAGE_31:
        case ANT_AERO_PAGE_32:
        case ANT_AERO_PAGE_80:
        case ANT_AERO_PAGE_81:
        case ANT_AERO_PAGE_82:
          velo_send_req_page_loop(p_profile);
          break;
        default:
          // Do nothing, only want to send supported pages.
          break;
      }
      break;
    case ANT_COMMON_PAGE_89:
      // need to create a controller id, then send back 4x:
      // only do if waas an rx event:
      if (!p_profile->p_sens_cb->event_is_tx)
      {
        velo_start_cid_loop(p_profile);
      }
      //velo_controller_id_t cid;
      //cid.serial_number = p_profile->page_9.serial_number;
      break;
    default:
      break;
  }
  // NRF_LOG_INFO("Radio frequency = %u",m_ant_aero_channel_config.rf_freq);
}

void ant_velo_evt_handler(ant_velo_profile_t * p_profile, ant_velo_page_t page)
{
  nrf_pwr_mgmt_feed();

  switch (page)
  {
    case ANT_VELO_PAGE_10:
                    /* fall through */
    case ANT_VELO_PAGE_11:
                    /* fall through */
    case ANT_VELO_PAGE_12:
                    /* fall through */
            //poll_count=0;
            //app_timer_start(m_read_timer, APP_TIMER_TICKS(3), &callerA);
            //bat_ctr++;
            break;
    case ANT_VELO_PAGE_102:
        // Send acknowledge messages for a fixed time duration before switching period.
        if (!p_profile->p_sens_cb->acknowledge_message) // ANT_VELO changes this flag to false on last send of ack msg.
        {
          if (period_id_supported(p_profile->page_101.period_id))
          {
            NRF_LOG_INFO("Switching to new frequency %u",p_profile->page_101.period);
            p_profile->p_sens_cb->ant_period = p_profile->page_101.period;
            ant_velo_period_index=ant_velo_index_from_period(p_profile->p_sens_cb->ant_period);
            ant_velo_period_set(p_profile->channel_number,p_profile->p_sens_cb->ant_period); 
            velo_set_led_pattern();
            max_poll_count = p_profile->p_sens_cb->ant_period/POLL_PERIOD;
            NRF_LOG_INFO("Max poll count = %u",max_poll_count);
          }
          else
          {
            NRF_LOG_INFO("Requested period not supported.");
          }
        }

      break;
    case ANT_VELO_PAGE_101:
      // Request ANT period change from display.
      p_profile->p_sens_cb->acknowledge_message=true;
      p_profile->p_sens_cb->acknowledge_messsage_counter=0;
      p_profile->p_sens_cb->acknowledge_message_max_count = (int16_t) (ANT_VELO_ACK_TIME / p_profile->p_sens_cb->ant_period);
      NRF_LOG_INFO("acknowledge_message_max_coun = %u",p_profile->p_sens_cb->acknowledge_message_max_count);
      break;
    default:
      break;
  }
  // If event was tx then need to start twi read timer, if necessary:
  if (p_profile->p_sens_cb->event_is_tx)
  {
    if (VELO_SYNC_TWI_ANT)
    {
      //NRF_LOG_INFO("Starting timer");
      poll_count=0;
      app_timer_start(m_read_timer, APP_TIMER_TICKS(3), &callerA);
    }
  }
}



// ##################################################    ANT BPWR STUFF    ##################################################

void ant_bpwr_evt_handler(ant_bpwr_profile_t * p_profile, ant_bpwr_evt_t event)
{
    nrf_pwr_mgmt_feed();

    switch (event)
    {
        case ANT_BPWR_PAGE_1_UPDATED:
            // calibration data received from sensor
            NRF_LOG_DEBUG("Received calibration data");
            break;

        case ANT_BPWR_PAGE_16_UPDATED:
            /* fall through */
        case ANT_BPWR_PAGE_17_UPDATED:
            /* fall through */
        case ANT_BPWR_PAGE_18_UPDATED:
            /* fall through */
        case ANT_BPWR_PAGE_80_UPDATED:
            /* fall through */
        case ANT_BPWR_PAGE_81_UPDATED:
            // data actualization
            //NRF_LOG_DEBUG("Page was updated");
            break;

        case ANT_BPWR_CALIB_TIMEOUT:
            // calibration request time-out
            NRF_LOG_DEBUG("ANT_BPWR_CALIB_TIMEOUT");
            break;

        case ANT_BPWR_CALIB_REQUEST_TX_FAILED:
            // Please consider retrying the request.
            NRF_LOG_DEBUG("ANT_BPWR_CALIB_REQUEST_TX_FAILED");
            break;

        default:
            // never occurred
            break;
    }
}


// ######################### ANT BSC STUFF #############################################
static void ant_evt_handler(ant_evt_t * p_ant_evt, void * p_context)
{
  // NRF_LOG_INFO("Channel = %u, event = %u",p_ant_evt->channel, p_ant_evt->event);
    switch (p_ant_evt->event)
    {
        case EVENT_RX_FAIL_GO_TO_SEARCH:
            /* Reset speed and cadence values */
            memset(&m_speed_calc_data, 0, sizeof(m_speed_calc_data));
            memset(&m_cadence_calc_data, 0, sizeof(m_cadence_calc_data));
            break;

        default:
            // No implementation needed
            break;
    }
}

NRF_SDH_ANT_OBSERVER(m_ant_observer, APP_ANT_OBSERVER_PRIO, ant_evt_handler, NULL); // I think this fires on every single ant event?

void ant_bsc_evt_handler(ant_bsc_profile_t * p_profile, ant_bsc_evt_t event)
{
    switch (event)
    {
        case ANT_BSC_PAGE_0_UPDATED:
            /* fall through */
        case ANT_BSC_PAGE_1_UPDATED:
            /* fall through */
        case ANT_BSC_PAGE_2_UPDATED:
            /* fall through */
        case ANT_BSC_PAGE_3_UPDATED:
            /* fall through */
        case ANT_BSC_PAGE_4_UPDATED:
            /* fall through */
        case ANT_BSC_PAGE_5_UPDATED:
            /* Log computed value */

//            if (BSC_TYPE == BSC_SPEED_DEVICE_TYPE)
//            {
//                NRF_LOG_INFO("Computed speed value:                 %u kph",
//                              (unsigned int) calculate_speed(p_profile->BSC_PROFILE_rev_count,
//                                                             p_profile->BSC_PROFILE_event_time));
//            }
//            else if (BSC_TYPE == BSC_CADENCE_DEVICE_TYPE)
//            {
//                NRF_LOG_INFO("Computed cadence value:               %u rpm",
//                              (unsigned int) calculate_cadence(p_profile->BSC_PROFILE_rev_count,
//                                                               p_profile->BSC_PROFILE_event_time));
//            }
            break;

        case ANT_BSC_COMB_PAGE_0_UPDATED:
//            NRF_LOG_INFO("Computed speed value:                         %u kph",
//                          (unsigned int) calculate_speed(p_profile->BSC_PROFILE_speed_rev_count,
//                                                         p_profile->BSC_PROFILE_speed_event_time));
//            NRF_LOG_INFO("Computed cadence value:                       %u rpms",
//                          (unsigned int) calculate_cadence(p_profile->BSC_PROFILE_cadence_rev_count,
//                                                           p_profile->BSC_PROFILE_cadence_event_time));
            break;

        default:
            break;
    }
}

// ################################################## Profile setup functions ##################################################


static void aero_profile_setup(void)
{
  ret_code_t err_code;

  // set device number:
  m_ant_aero_channel_config.device_number=deviceID();
  // Set up channel:
  ant_aero_init(&m_ant_aero,AERO_CHANNEL_CONFIG(m_ant_aero),AERO_PROFILE_CONFIG(m_ant_aero));
  //Setup channel using variables I don't want to put in the more generic aero profile:
  m_ant_aero.page_80 = ANT_COMMON_page80(VELO_HW_REVISION, VELO_MANUF_ID,VELO_MODEL_NO);
  m_ant_aero.page_81 = ANT_COMMON_page81(VELO_SW_REVISION_MAIN,VELO_SW_REVISION_SUP,serialNo());
  // Set default period:
  m_ant_aero.p_sens_cb->enabled_8hz = (ANT_AERO_MSG_DEFAULT_PERIOD == AERO_MSG_8HZ_PERIOD);
  // Open channel:
  
  if (ANT_AERO_CHANNEL_ACTIVE)
  {
    err_code = ant_aero_open(&m_ant_aero);
    APP_ERROR_CHECK(err_code);
  }

//  //Load peripheral devices:
//  velo_load_pwr_sensor(&m_ant_aero.p_sens_cb->power_sensor);
//  // If sensor valid, then open:
//  if (!m_ant_aero.p_sens_cb->power_sensor.device_number == 0xFFFF)
//  {
//    
//  }
}

static void velo_profile_setup(void)
  {
  ret_code_t err_code;
  // set device number:
  m_ant_velo_channel_config.device_number=deviceID();
  // Set ANT frequency
  m_ant_velo_channel_config.rf_freq=ant_rf_freq;
  err_code = ant_velo_init(&m_ant_velo,VELO_CHANNEL_CONFIG(m_ant_velo),VELO_PROFILE_CONFIG(m_ant_velo));
  APP_ERROR_CHECK(err_code);

  //set velo max poll count:
  ant_velo_period_index=ant_velo_index_from_period(m_ant_velo.p_sens_cb->ant_period);
  velo_set_led_pattern();
  max_poll_count=m_ant_velo.p_sens_cb->ant_period/POLL_PERIOD;
  NRF_LOG_INFO("Max poll count = %u",max_poll_count);


  if (ANT_VELO_CHANNEL_ACTIVE)
  {
    err_code = ant_velo_open(&m_ant_velo);
    APP_ERROR_CHECK(err_code);
  }
}

static void bpwr_profile_setup(ant_aero_peripheral_device_t * p_device)
{
  ret_code_t err_code;
// NOTE The following object is constant so we can't change it.
// I think we are going to want to create our own version of ant_bpwr to allow more flexibility.
//  m_ant_bpwr_channel_bpwr_disp_config.rf_freq = p_device->rf_freq;
//  m_ant_bpwr_channel_bpwr_disp_config.transmission_type = p_device->transmission_type;
//  m_ant_bpwr_channel_bpwr_disp_config.device_type = p_device->device_type;
//  m_ant_bpwr_channel_bpwr_disp_config.device_number = p_device->device_number;
//  m_ant_bpwr_channel_bpwr_disp_config.channel_period = p_device->channel_period;

  err_code = ant_bpwr_disp_init(&m_ant_bpwr, BPWR_DISP_CHANNEL_CONFIG(m_ant_bpwr), BPWR_DISP_PROFILE_CONFIG(m_ant_bpwr));
  APP_ERROR_CHECK(err_code);
  err_code = ant_bpwr_disp_open(&m_ant_bpwr);
  APP_ERROR_CHECK(err_code);
}


static void bsc_profile_setup(void)
{
/** @snippet [ANT BSC RX Profile Setup] */
    uint32_t err_code;

    err_code = ant_bsc_disp_init(&m_ant_bsc, BSC_DISP_CHANNEL_CONFIG(m_ant_bsc), BSC_DISP_PROFILE_CONFIG(m_ant_bsc));
    APP_ERROR_CHECK(err_code);

    err_code = ant_bsc_disp_open(&m_ant_bsc);
    APP_ERROR_CHECK(err_code);

}


// ################################################## Pairing functions ##################################################


//Handle pairing.  
void velo_start_pairing(void)
{
	if (!velo_is_pairing)
	{
		m_ant_velo_channel_config.device_type = VELO_DEVICE_TYPE | ANT_ID_DEVICE_TYPE_PAIRING_FLAG;
		app_timer_start(m_pairing_tick_timer, APP_TIMER_TICKS(PAIRING_TIME_MS), NULL);
		ant_velo_set_channel_id(VELO_CHANNEL_NUM,m_ant_velo_channel_config);
		velo_is_pairing=true;
	}
}

static void velo_stop_pairing_callback(void * p_context)
{
	velo_stop_pairing();
}

void velo_stop_pairing(void)
{
	if (velo_is_pairing)
	{
		m_ant_velo_channel_config.device_type = VELO_DEVICE_TYPE;
		ant_velo_set_channel_id(VELO_CHANNEL_NUM,m_ant_velo_channel_config);
		velo_is_pairing=false;
                velo_check_charge_status();
	}
}

void velo_toggle_ant_period(void * p_event_data, uint16_t event_size)
{
  // Toggle period from 4091 to 512.
  // Switch period to 4091 unless it is already 4091 in which case switch to 512.
  if (m_ant_velo.p_sens_cb->ant_period == 4091)
  {
    m_ant_velo.p_sens_cb->ant_period = 512;
  }
  else
  {
    m_ant_velo.p_sens_cb->ant_period = 4091;
  }
  ant_velo_period_set(m_ant_velo.channel_number,m_ant_velo.p_sens_cb->ant_period); 
  ant_velo_period_index=ant_velo_index_from_period(m_ant_velo.p_sens_cb->ant_period);
  velo_set_led_pattern();
  max_poll_count = m_ant_velo.p_sens_cb->ant_period/POLL_PERIOD;
  NRF_LOG_INFO("Max poll count = %u",max_poll_count);
}

void velo_init(void)
{
  NRF_LOG_FLUSH();
  nrf_gpio_cfg(PIN_LED1,NRF_GPIO_PIN_DIR_OUTPUT,NRF_GPIO_PIN_INPUT_DISCONNECT,NRF_GPIO_PIN_NOPULL,NRF_GPIO_PIN_S0D1,NRF_GPIO_PIN_NOSENSE );
  nrf_gpio_pin_write(PIN_LED1, 1);
  
  nrf_gpio_cfg(PIN_LED2,NRF_GPIO_PIN_DIR_OUTPUT,NRF_GPIO_PIN_INPUT_DISCONNECT,NRF_GPIO_PIN_NOPULL,NRF_GPIO_PIN_S0D1,NRF_GPIO_PIN_NOSENSE );
  nrf_gpio_pin_write(PIN_LED2,1);
  
  nrf_gpio_cfg_input(PIN_PG,NRF_GPIO_PIN_PULLUP);

  //Toggle GPOut 
  nrf_gpio_cfg_input(PIN_GPOUT,NRF_GPIO_PIN_PULLDOWN);
  nrf_delay_ms(1);
  nrf_gpio_cfg_input(PIN_GPOUT,NRF_GPIO_PIN_PULLUP);

  
  //Switch on regulators - powers up the sensors.
  //Eventually this needs to go into switch handling.
  nrf_gpio_cfg_output(PIN_REG);
  nrf_gpio_cfg_output(PIN_TS);
  
  nrf_gpio_pin_write(PIN_REG, 1);
  nrf_gpio_pin_write(PIN_TS, 1);
  //Wait for 50ms to give i2c devices a change to boot up.
  nrf_delay_ms(50);
  // Initialise lights and buttons
  velo_button_init();

  //Initialise flash storage:
  velo_flash_init();

  // Load calibration data:
  // NOTE Turned off for now so it works with no data on chip.  If there is none it crashes.
  velo_load_calibration();

  // Load config from flash memory:
  velo_load_config();

  // Populate settings array (probably not necessary long term);
  velo_settings_get(velo_settings);

  //Create timer to handle pairing mode:
  uint8_t err_code;
  err_code = app_timer_create(&m_pairing_tick_timer, APP_TIMER_MODE_SINGLE_SHOT, velo_stop_pairing_callback);
  APP_ERROR_CHECK(err_code);

  //Initialise sensors:
  velo_sensors_init(velo_post_read_callback);

  //Create timer to handle data read
  if (VELO_SYNC_TWI_ANT)
  {
    err_code = app_timer_create(&m_read_timer, APP_TIMER_MODE_SINGLE_SHOT, velo_read_all_callback);
    APP_ERROR_CHECK(err_code);
  }
  else
  {
    err_code = app_timer_create(&m_read_timer, APP_TIMER_MODE_REPEATED, velo_read_all_callback);
    APP_ERROR_CHECK(err_code);
    //Set that timer going:
    app_timer_start(m_read_timer, POLL_PERIOD, &callerA);
  }
  // Setup velosense ANT channel:
  velo_profile_setup();
  // Setup ANT AERO channel:
  // aero_profile_setup();
  // Setup bike power ANT Plus channel:
  //bpwr_profile_setup();
  // Setup bike speed/cadence channel:
  //bsc_profile_setup();  

}



