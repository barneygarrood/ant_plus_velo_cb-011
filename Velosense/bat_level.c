#include "sdp3x_sensor.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"
#include "bat_level.h"


static uint8_t soc_register = 0x1C;
static uint8_t voltage_register = 0x04;

void bat_gauge_get_soc_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  twi_read_register_transfer(BAT_GAUGE_ADDRESS, &soc_register, p_data, 2, p_trans);
}


void bat_gauge_get_voltage_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  twi_read_register_transfer(BAT_GAUGE_ADDRESS, &voltage_register, p_data, 2, p_trans);
}


void bat_gauge_enter_shutdown_transfer(nrf_twi_mngr_transfer_t * p_trans)
{
  // enable shutdown mode.
   uint8_t reg[3]={0x00,0x1B,0x00};
   twi_write_transfer(BAT_GAUGE_ADDRESS,reg,3,&p_trans[0]);
  // enter shutdown mode.
   reg[1] = 0x1C;
   twi_write_transfer(BAT_GAUGE_ADDRESS,reg,3,&p_trans[1]);
}
