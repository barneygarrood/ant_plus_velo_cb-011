#ifndef BAT_LEVEL_H__
#define BAT_LEVEL_H__

#include <stdint.h>
#include "twi_scanner.h"

#define BAT_GAUGE_ADDRESS 0x55
#define BAT_GAUGE_TWI_INSTANCE 1



/**
 * @brief 		Get data from fuel gauge.
 */
void bat_gauge_get_soc_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans);


void bat_gauge_get_voltage_transfer(uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans);


void bat_gauge_enter_shutdown_transfer(nrf_twi_mngr_transfer_t * p_trans);

//uint16_t bat_gauge_convert_soc(int8_t * raw_data);
//
//uint16_t bat_gauge_convert_voltage(int8_t * raw_data);

#endif //BAT_LEVEL_H__

