

#include "twi_scanner.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "velo_config.h"

NRF_TWI_MNGR_DEF(m_nrf_twi_mngr_0, MAX_PENDING_TRANSACTIONS, TWI_INSTANCE_ID_0);
NRF_TWI_MNGR_DEF(m_nrf_twi_mngr_1, MAX_PENDING_TRANSACTIONS, TWI_INSTANCE_ID_1);

uint32_t twi_event_time[2] = {0,0};
//twi_status says if waiting for response or not.
// One value for each twi channel, = 1 if a transaction scheduled.
uint8_t twi_status[2]={0,0};

/*
 * --------------------------- TWI setup start ----------------------------------------------*
 */

// TWI (with transaction manager) initialization.
void twi_init_all()
{
  ret_code_t err_code;
  nrf_drv_twi_config_t twi_config[] = {{
           .scl				= PIN_SCL_1,
           .sda				= PIN_SDA_1,
       .frequency                       = NRF_TWI_FREQ_400K,
           .interrupt_priority          = APP_IRQ_PRIORITY_HIGHEST,
           .clear_bus_init		= false
          },
          {
           .scl				= PIN_SCL_2,
           .sda				= PIN_SDA_2,
           .frequency			= NRF_TWI_FREQ_400K,
           .interrupt_priority          = APP_IRQ_PRIORITY_HIGHEST,
           .clear_bus_init		 = false
          }};
  // Initilaise TWI 0
  err_code = nrf_twi_mngr_init(&m_nrf_twi_mngr_0, &twi_config[0]);
  APP_ERROR_CHECK(err_code);
  // Initialise TWI 1
  err_code = nrf_twi_mngr_init(&m_nrf_twi_mngr_1, &twi_config[1]);
  APP_ERROR_CHECK(err_code);
}

void twi_uninit_all()
{
  nrf_twi_mngr_uninit(&m_nrf_twi_mngr_0);
  
  nrf_twi_mngr_uninit(&m_nrf_twi_mngr_1);
}

static nrf_twi_mngr_transaction_t NRF_TWI_MNGR_BUFFER_LOC_IND transact[2];

// Function to schedule transfers.  On completion of transfers the callback function is called.
void twi_schedule_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t transfer_count, nrf_twi_mngr_callback_t cb_function, uint8_t * p_twi_id)
{    
  ret_code_t err_code;
    
  transact[*p_twi_id].callback = cb_function,
  transact[*p_twi_id].p_user_data = p_twi_id,
  transact[*p_twi_id].p_transfers = p_trans,
  transact[*p_twi_id].number_of_transfers = transfer_count;
    
  switch (*p_twi_id)
  {
    case 0:
      err_code=nrf_twi_mngr_schedule(&m_nrf_twi_mngr_0, &transact[*p_twi_id]);
      break;
    case 1:
      err_code=nrf_twi_mngr_schedule(&m_nrf_twi_mngr_1, &transact[*p_twi_id]);
      break;
  }
  APP_ERROR_CHECK(err_code);
}

// Function to perform a transfer and wait for it to be returned.
void twi_perform_transfers(nrf_twi_mngr_transfer_t * p_trans, uint8_t trans_count, uint8_t twi_id)
{
  ret_code_t err_code;
  switch (twi_id)
  {
    case 0:
      err_code = nrf_twi_mngr_perform(&m_nrf_twi_mngr_0, NULL, p_trans, trans_count, NULL);
      break;
    case 1:
      err_code = nrf_twi_mngr_perform(&m_nrf_twi_mngr_1, NULL, p_trans, trans_count, NULL);
      break;
  }
  APP_ERROR_CHECK(err_code);
}



// Function to create write transfer:
void twi_write_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans)
{
  p_trans[0] = (nrf_twi_mngr_transfer_t)NRF_TWI_MNGR_WRITE(twi_address,p_data,data_len,0);

//  p_trans->operation=NRF_TWI_MNGR_WRITE_OP(twi_address);
//  p_trans->p_data=p_data;
//  p_trans->length=data_len;
//  p_trans->flags=0;
}

// Function to create read transfer:
void twi_read_transfer(uint8_t twi_address, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans)
{
  p_trans[0] = (nrf_twi_mngr_transfer_t)NRF_TWI_MNGR_READ(twi_address,p_data,data_len,0);

//  p_trans->operation=NRF_TWI_MNGR_READ_OP(twi_address);
//  p_trans->p_data=p_data;
//  p_trans->length=data_len;
//  p_trans->flags=0;
}

//Function to create transfers to read a specific register, which means first writing register address, then reading data.
void twi_read_register_transfer(uint8_t twi_address, uint8_t * p_reg_addr, uint8_t * p_data, uint8_t data_len, nrf_twi_mngr_transfer_t * p_trans)
{
  twi_write_transfer(twi_address,p_reg_addr,1,&p_trans[0]);
  twi_read_transfer(twi_address,p_data,data_len,&p_trans[1]);

}
