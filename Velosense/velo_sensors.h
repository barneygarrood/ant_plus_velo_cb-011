#ifndef VELO_SENSORS_H__
#define VELO_SENSORS_H__

#include "velo_config.h"
#include "twi_scanner.h"
#include "sdp3x_sensor.h"
#include "tdk_icp.h"

#include "velo_button.h"
#include "bat_level.h"
#include "velo_calibration.h"

#include "velo_us_timer.h"

//#define VELO_SENSOR_CONFIG_DEF(NAME,EVT_HANDLER)        \
////static velo_sensor_cb_t CONCAT_2(NAME,_cb);             \
//static velo_sensor_config_t CONCAT_2(NAME,_config) =    \
//        {                                               \
////          .p_cb = &CONCAT_2(NAME,_cb)                   \
//          .evt_handler = (EVT_HANDLER),                 \
//        }                                               


typedef struct
{
	float baro_press_1;
	float baro_press_2;
	int16_t sdp_1;
	int16_t sdp_2;
	float baro_temp_1;
	float baro_temp_2;
	float baro_temp;
	float pdyn;
	float angle;
        uint32_t time_inact;
        uint8_t error;
        uint8_t bat_soc;
        uint16_t bat_voltage;
	
}velo_data_t;

typedef enum 
{
  velo_sensors_none = 0,
  velo_sensors_sdp_0 = 1,
  velo_sensors_sdp_1 = 2,
  velo_sensors_tdk_0 = 4,
  velo_sensors_tdk_1 = 8,
  velo_sensors_bat_gauge = 16,
}velo_sensors_t;

typedef enum
{
  velo_twi_ctrl_none = 0,
  velo_twi_ctrl_0 = 1,
  velo_twi_ctrl_1 = 2,
} velo_twi_ctrl_t;

//typdef struct {
//  velo_sensors_t * p_velo_sensors;
//  velo_data_t * p_velo_data;
//  uint8_t twi_chan;
//  twi_control_t * p_twi_control;
//}velo_sensor_cb_t;


static sdp3x_sensor_t sdp3x_sensors[];
static tdk_icp_sensor_t tdk_icp_sensors[];
static velo_twi_ctrl_t velo_twi_ctrl;
extern velo_sensors_t velo_sensors;
extern velo_data_t velo_data;


// Declare event type - this allows us to have a callback from read functions;
typedef void (* velo_sensor_evt_t)();

//typedef struct {
//  //velo_sensor_cb_t    * p_cb;
//  velo_sensor_evt_t   evt_handler;
//}velo_sensor_config_t;


void velo_sensors_init(velo_sensor_evt_t callback_function);
void velo_calibration();
//void velo_read_all_callback(void * p_context);

void velo_sensors_read(uint8_t read_bat);


#endif //VELO_SENSORS_H__
