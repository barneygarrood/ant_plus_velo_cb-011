#ifndef SDP3X_SENSOR_H__
#define SDP3X_SENSOR_H__

#include <stdint.h>
#include "twi_scanner.h"

#define SDP_ADDRESS 0x21


typedef struct {
	int16_t temperature;
	int16_t pressure;
} sdp3x_data_t;

typedef struct {
	uint8_t device_address;
	uint8_t twi_instance;
	sdp3x_data_t data;
} sdp3x_sensor_t;

/**
 * @brief 	Initialise sdp sensors..
 * @param			twi_instance_count				TWI instance count
 * @param			sdp3x_sensors						2D array of barometer objects (TWI INSTANCES X BAROS PER INSTANCE)
 */
void sdp3x_sensors_init(uint8_t twi_instance_count,sdp3x_sensor_t sdp3x_sensors[]);

//void InitSDP(nrf_twi_mngr_t *p_twi_mngr_0, nrf_twi_mngr_t *p_twi_mngr_1);

/**
 * @brief 		Reset all sdp sensors on the given twi bus.
 * @param			twi_instance				ID of twi bus on which to reset sensors.
 */
void sdp3x_reset_all(uint8_t twi_instance);


/**
 * @brief 		Start continuous measurement with averaging.
 * @param			p_sdp3x_sensor				Pointer to sdp3x sensor object.
 */
void sdp3x_start_read(sdp3x_sensor_t *p_sdp3x_sensor);

/**
 * @brief 		Stop continuous measurement.
 * @param			p_sdp3x_sensor				Pointer to sdp3x sensor object.
 */
void sdp3x_stop_read(sdp3x_sensor_t *p_sdp3x_sensor);

/**
 * @brief 		Get data from sdp sensor.
 * @param			p_sdp3x_sensor				Pointer to sdp3x sensor object.
 */
void sdp3x_get_data(sdp3x_sensor_t *p_sdp3x_sensor);


void sdp3x_get_data_transfer(sdp3x_sensor_t *p_sdp3x_sensor,uint8_t *p_data, nrf_twi_mngr_transfer_t *p_trans);
void sdp3x_get_data_from_raw(sdp3x_sensor_t *p_sdp3x_sensor,uint8_t * p_data);

#endif //SDP3X_SENSOR_H__
