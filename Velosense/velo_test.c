#include "velo_test.h"

uint32_t twi_event_time[2] = {0,0};
//twi_status says if waiting for response or not.
// One value for each twi channel, = 1 if a transaction scheduled.
uint8_t twi_status[2]={0,0};
