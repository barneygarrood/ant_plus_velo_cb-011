#include "sdp3x_sensor.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"

/*  The valid addresses for SDP3x sensors. */
uint8_t sdp3x_address_1 = 0x21;
uint8_t sdp3x_address_2 = 0x22;
uint8_t sdp3x_address_3 = 0x23;

/* I2C Command Definitions 
 * Note I not const so we can pass pointer to these to twi functions.
 */
uint8_t sdp3x_start_cont_avg[2]   = { 0x36, 0x15 };
uint8_t sdp3x_stop_cont[2]        = { 0x3F, 0xF9 };
uint8_t sdp3x_soft_reset[2]       = { 0x00, 0x06 };

void sdp3x_sensors_init(uint8_t twi_instance_count,sdp3x_sensor_t sdp3x_sensors[])
{
  for (int _twi_instance=0;_twi_instance<twi_instance_count;_twi_instance++)
  {
    sdp3x_sensors[_twi_instance].device_address=sdp3x_address_1;
    sdp3x_sensors[_twi_instance].twi_instance=_twi_instance;
    sdp3x_sensors[_twi_instance].data.temperature=0;
    sdp3x_sensors[_twi_instance].data.pressure=0;
    //Reset all on this twi channel:
    sdp3x_reset_all(_twi_instance);
  }
  //wait for sensors to reset:
  nrf_delay_ms(50);
  //sent commands to start continuous measurement:
  for (int _twi_instance=0;_twi_instance<twi_instance_count;_twi_instance++)
  {
    sdp3x_start_read(&sdp3x_sensors[_twi_instance]);
  }
}

// Updated to use twi_mngr!
void sdp3x_reset_all(uint8_t twi_instance)
{
  nrf_twi_mngr_transfer_t trans[1];
  twi_write_transfer(sdp3x_soft_reset[0],&sdp3x_soft_reset[1],1,trans);
  twi_perform_transfers(trans,1,twi_instance);
}

// Updated to use twi_mngr!
void sdp3x_start_read(sdp3x_sensor_t * p_sdp3x_sensor)
{
  nrf_twi_mngr_transfer_t trans[1];
  twi_write_transfer(p_sdp3x_sensor->device_address,sdp3x_start_cont_avg,2,trans);
  twi_perform_transfers(trans,1,p_sdp3x_sensor->twi_instance);

}	

// Updated to use twi_mngr!
void sdp3x_stop_read(sdp3x_sensor_t * p_sdp3x_sensor)
{
  nrf_twi_mngr_transfer_t trans[1];
  twi_write_transfer(p_sdp3x_sensor->device_address,sdp3x_stop_cont,2,trans);
  twi_perform_transfers(trans,1, p_sdp3x_sensor->twi_instance);
}	

void sdp3x_get_data_transfer(sdp3x_sensor_t * p_sdp3x_sensor,uint8_t *p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  //All we need here is a pure read of 9 bytes:
  twi_read_transfer(p_sdp3x_sensor->device_address,p_data,9,p_trans);
}

void sdp3x_get_data_from_raw(sdp3x_sensor_t * p_sdp3x_sensor,uint8_t * p_data)
{
  // This includes CRC checks, but I am going to ignore this for now as so far I have never seen it being necessary.
  p_sdp3x_sensor->data.pressure = (p_data[0]<<8)|p_data[1];
  p_sdp3x_sensor->data.temperature = (p_data[3]<<8)|p_data[4];
  
}
