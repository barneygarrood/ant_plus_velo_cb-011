#include "velo_calibration.h"


void velo_load_calibration()
{
  // Loads calibration data from flash memory where it is loaded via hex file separately to everything else.
  // Idea is that we can load a calibration into a unit separately from the code etc.
  // First two bytes are calibration file size, including the two bytes for the cal file itself:

  uint16_t cal_file_len = 0;
  uint32_t addr = CAL_START_ADDR;
  memcpy(&cal_file_len,(uint32_t*) addr,2);
  NRF_LOG_INFO("Loading calibration file from flash memory: %u bytes",cal_file_len);
  addr+=2;
  
  //sizes:
  memcpy(&velo_cal_ncols,(uint32_t*) addr, 1);
  addr++;
  
  memcpy(&velo_cal_nrows,(uint32_t*) addr, 1);
  addr++;
  
  // List of "CxDiff" values:
  velo_cal_cxdiff = malloc(velo_cal_ncols*4);
  for (int i=0;i<velo_cal_ncols;i++)
  {
    memcpy(&velo_cal_cxdiff[i],(uint32_t*) addr,4);
    addr+=4;
  }

  // list of "x" values:
  velo_cal_x = malloc(velo_cal_nrows*4);
  for (int i=0;i<velo_cal_nrows;i++)
  {
    memcpy(&velo_cal_x[i],(uint32_t*) addr,4);
    addr+=4;
  }

  // Yaw angle calibration table:
  velo_cal_angle = malloc(velo_cal_nrows*sizeof(float));
  for (int i=0;i<velo_cal_nrows;i++)
  {
    velo_cal_angle[i]=malloc(velo_cal_ncols*sizeof(float));
    for (int j=0;j<velo_cal_ncols;j++)
    {
      memcpy(&velo_cal_angle[i][j],(uint32_t *)addr,4);
      addr+=4;
    }
  }
  
  // Dynamic pressure calibration table:
  velo_cal_pdyn = malloc(velo_cal_nrows*sizeof(float));
  for (int i=0;i<velo_cal_nrows;i++)
  {
    velo_cal_pdyn[i]=malloc(velo_cal_ncols*sizeof(float));
    for (int j=0;j<velo_cal_ncols;j++)
    {
      memcpy(&velo_cal_pdyn[i][j],(uint32_t *)addr,4);
      addr+=4;
    }
  }
}