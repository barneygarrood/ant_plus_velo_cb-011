#ifndef VELO_FLASH_H__
#define VELO_FLASH_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "nrf_fstorage_sd.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_soc.h"
#include "nrf_sdh.h"

#include "app_scheduler.h"
#include "velo_config.h"


typedef struct 
{
  uint8_t * p_data;
  uint32_t address;
}velo_flash_write_data_t;

void velo_flash_init();
void velo_flash_read(uint16_t first_byte_number, uint8_t * p_data, uint16_t byte_count);
void velo_flash_write(uint16_t first_byte_number, uint8_t * p_data, uint16_t byte_count);
void velo_flash_erase();

#endif //VELO_FLASH_H__
