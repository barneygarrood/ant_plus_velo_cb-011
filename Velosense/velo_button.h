
#ifndef VELO_BUTTON_H__
#define VELO_BUTTON_H__

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "app_button.h"
#include "app_timer.h"
#include "nrf_drv_pwm.h"
#include "velo_config.h"
#include "velo_driver.h"
#include "bat_level.h"
#include "nrf_drv_gpiote.h"
#include "nrf_power.h"
#include "velo_flash.h"

#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50) 
#define BUTTON_OFF_MS			2000
#define TAP_TIME_MS                     10000   // Time in ms within which taps have to be complete.
#define BOOTLOADER_TAP_COUNT            7       // Number of times to tap button to enter bootloader mode 
#define PERIOD_TOGGLE_TAP_COUNT         3       // Number of times to tap button to toggle between low and high freqnency ANT periods.

#define BOOTLOADER_DFU_START 0xB1


#define APP_TIMER_TICKS_US(US)                                \
            ((uint32_t)ROUNDED_DIV(                        \
            (US) * (uint64_t)APP_TIMER_CLOCK_FREQ,         \
            1000000 * (APP_TIMER_CONFIG_RTC_FREQUENCY + 1)))
					
extern bool velo_power_connected;
extern bool velo_is_pairing;
extern bool velo_buthold;
extern bool velo_low_bat;
extern bool velo_shutdown_warning;

void velo_led_timer_init(void);
void velo_button_init(void);

void velo_check_charge_status(void);
void velo_set_led_pattern(void);
void velo_switch_off(void);

#endif //VELO_BUTTON_H__



