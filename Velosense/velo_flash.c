#include "velo_flash.h"
#include "nrf_delay.h"

static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);
static uint8_t * m_data = NULL;


NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    .evt_handler = fstorage_evt_handler,
    .start_addr = VELO_FLASH_START_ADDR,
    .end_addr   = VELO_FLASH_END_ADDR,
};


static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        NRF_LOG_INFO("--> Event received: ERROR while executing an fstorage operation.");
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
            NRF_LOG_INFO("--> Event received: wrote %d bytes at address 0x%x.",
                         p_evt->len, p_evt->addr);
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
            NRF_LOG_INFO("--> Event received: erased %d page from address 0x%x.",
                         p_evt->len, p_evt->addr);
        } break;

        default:
            break;
    }
}

void wait_for_flash_ready(nrf_fstorage_t const * p_fstorage)
{
    /* While fstorage is busy, sleep and wait for an event. */
    while (nrf_fstorage_is_busy(p_fstorage))
    {
    (void) sd_app_evt_wait();
    }
}

void velo_flash_init()
{
  // Initialise array - this needsd to be word aligned so have to use malloc function:
  if (m_data == NULL) 
  {
    m_data = malloc(VELO_FLASH_BLOCK_SIZE);
  }
  nrf_fstorage_init(&fstorage, &nrf_fstorage_sd, NULL);
  wait_for_flash_ready(&fstorage);
  NRF_LOG_INFO("Flash memory initialised, address range: 0x%x to 0x%x.",fstorage.start_addr, fstorage.end_addr);
}

void velo_flash_read(uint16_t first_byte_number, uint8_t * p_data, uint16_t byte_count)
{
  uint32_t address = VELO_FLASH_START_ADDR + first_byte_number;

  ret_code_t err_code;

  err_code = nrf_fstorage_read(&fstorage,address,p_data,byte_count);
  NRF_LOG_INFO("Read in flash_read %u bytes starting address 0x%x",byte_count, address);

  NRF_LOG_FLUSH();
  APP_ERROR_CHECK(err_code);
}

//static object to hold write data:
//static velo_flash_write_data_t write_data;

void velo_flash_write_cb(void *  p_data, uint16_t byte_count)
{
  // Write callback so we can use scheduler.  This avoids running from same context as soft device interrupt handler which I think causes issues.

  ret_code_t err_code = nrf_fstorage_write(&fstorage,((velo_flash_write_data_t *) p_data)->address,((velo_flash_write_data_t *) p_data)->p_data,byte_count,NULL);
  // Wait for write operation to complete.
  wait_for_flash_ready(&fstorage);
  APP_ERROR_CHECK(err_code);
  NRF_LOG_INFO("Data written");
}

void velo_flash_write(uint16_t first_byte_number, uint8_t * p_data, uint16_t byte_count)
{
  //Byte_count has to be a multiple of 4.  We will just assert this:
  uint32_t addr = VELO_FLASH_START_ADDR + first_byte_number;
 
  ret_code_t err_code;

  NRF_LOG_INFO("Writing %u bytes starting address 0x%x",byte_count, addr);
  NRF_LOG_FLUSH();
  nrf_delay_ms(100);
  //Copy data to static array:  
  memcpy(m_data,p_data,byte_count);
  //Schedule transfer:
  velo_flash_write_data_t data_to_write;
  data_to_write.p_data = m_data;
  data_to_write.address=addr;
  app_sched_event_put(&data_to_write,byte_count,velo_flash_write_cb);

}


void velo_flash_erase_cb(void *  p_data, uint16_t unused)
{
  // Callback to allow us to call from scheduler, so it runs in main context.
  // Don't require any input parameters.

  // We are putting all settings in the one page for now, so we just erase the lot.
  // Note in flash this sets all bytes to 0xFF.  Subsequent writes can only change bits to '0'.
  ret_code_t err_code = nrf_fstorage_erase(&fstorage, VELO_FLASH_START_ADDR, 1, NULL);
  
  wait_for_flash_ready(&fstorage);
  APP_ERROR_CHECK(err_code);
  NRF_LOG_INFO("Flash config memory erased");
}

void velo_flash_erase()
{
  app_sched_event_put(NULL,0,velo_flash_erase_cb);

}
