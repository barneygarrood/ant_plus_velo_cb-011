#ifndef VELO_CONFIG_H__
#define VELO_CONFIG_H__

#include "ant_aero.h"

#define VERSION                   2.5.0         // Current code version
#define VELO_SW_REVISION_MAIN     2
#define VELO_SW_REVISION_SUP      5

#define VELO_HW_REVISION          0
#define VELO_MANUF_ID             296
#define VELO_MODEL_NO             0

// Calibration location in flash memory:
#define CAL_START_ADDR  0x75000
#define CAL_SIZE        0x2000

// Settings location in flash memory:
#define VELO_FLASH_START_ADDR 0x77000
#define VELO_FLASH_END_ADDR 0x77FFF
#define VELO_FLASH_SIZE 0x1000
#define VELO_FLASH_BLOCK_SIZE 128


#define TWI_INSTANCE_COUNT        2				// Number of twi channels being used

#define TDK_ICP_OSR  TDK_ICP_NORMAL

#define BAT_CHECK_FREQUENCY 128
#define POLL_PERIOD 240          // Data measurement frequency

#define ANT_VELO_ACK_TIME  16384           //Amount of time in ticks (/32768) for which we should send acknowledge message.

#define AERO_TRANS_TYPE             5
#define VELO_TRANS_TYPE             5               // Velosense device transmission type.

#define BPWR_TRANS_TYPE             0               // Bike power device transmission type.
#define BWPR_DEFAULT_DEV_NUM        0               // Bike power device default device number.  Eventually will replace with pass pair.

#define BSC_TRANS_TYPE             0                // Bike power device transmission type.
#define BSC_DEFAULT_DEV_NUM        0                // Bike power device default device number.  Eventually will replace with pass pair.

#define VELO_LOW_VOLTAGE_WARNING 3560 // Low battery warning when voltage drops below this level.
#define VELO_LOW_VOLTAGE_SHUTDOWN 2000        //3550 // Device shutdown when voltage drops below this value.  Below this value, regulator stops regulating, and in any case around 1 hour of operation left (shutdown at 32.2/33.5 hours of operation)

#define ANT_AERO_MSG_DEFAULT_PERIOD AERO_MSG_4HZ_PERIOD

// BSC display type.  need to figure out how this comes across pass pairing. Device type?
// <121=> Combined data 
// <122=> Cadence data 
// <123=> Speed data 
#ifndef BSC_TYPE
#define BSC_TYPE 123
#endif

#define SDP_INACT_THRESHOLD     60      // 60 = 1Pa.
#define INACTIVITY_WARNING_TIMEOUT 6600  // 1 hour 50 minutes
#define INACTIVITY_SHUTDOWN_TIMEOUT 7200 // 2 hours.

//Define pins to use:
#define PIN_SCL_1			2
#define PIN_SDA_1			3
#define PIN_SCL_2			4
#define PIN_SDA_2			5

#define PIN_LED1			8
#define PIN_LED2			6
#define PIN_LED3			11
#define PIN_REG				12
#define PIN_STAT_1                      13
#define PIN_STAT_2                      14
#define PIN_TS				15
#define PIN_PG				16
#define PIN_SW				18
#define PIN_GPOUT			20


#endif //VELO_CONFIG_H__
