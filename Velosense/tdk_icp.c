#include "tdk_icp.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"
#include "velo_driver.h"
#include "velo_us_timer.h"

static uint8_t tdk_meas_start_cmd[2] = {0};
uint8_t tdk_min_meas_time_ms = 0;

void tdk_icp_init_all(uint8_t twi_instance_count,tdk_icp_sensor_t * tdk_icp_sensors, tdk_icp_osr_t oversample_rate,nrf_twi_mngr_transfer_t * p_trans)
{
  for (int i=0;i<twi_instance_count;i++)
  {
    tdk_icp_sensors[i].twi_instance=i;
    tdk_icp_init(&tdk_icp_sensors[i],oversample_rate);
    // Setup start measurement transfer:
    tdk_meas_start_transfer(&tdk_icp_sensors[i], &p_trans[i]);
    //perform start measurement transfer for first time.
    twi_perform_transfers(&p_trans[i],1,tdk_icp_sensors[i].twi_instance);
    tdk_icp_sensors[i].read_requested=1;
  }
}

void  tdk_icp_init(tdk_icp_sensor_t * s, tdk_icp_osr_t oversample_rate)
{
  short otp[4];
  tdk_icp_read_otp(s, otp);
  tdk_icp_init_base(s, otp);
  uint16_t cmd=0;
  //Set osr:
  switch (oversample_rate)
  {
    case TDK_ICP_LOW_POWER:
      cmd = TDK_ICP_START_MEAS_LP;
      tdk_meas_start_cmd[0] = (cmd >> 8) & 0xff;
      tdk_meas_start_cmd[1] = cmd & 0xff;
      tdk_min_meas_time_ms = 3;
      break;
    case TDK_ICP_NORMAL:
      cmd = TDK_ICP_START_MEAS_N;
      tdk_meas_start_cmd[0] = (cmd >> 8) & 0xff;
      tdk_meas_start_cmd[1] = cmd & 0xff;
      tdk_min_meas_time_ms = 7;
      break;
    case TDK_ICP_LOW_NOISE:
      cmd = TDK_ICP_START_MEAS_LN;
      tdk_meas_start_cmd[0] = (cmd >> 8) & 0xff;
      tdk_meas_start_cmd[1] = cmd & 0xff;
      tdk_min_meas_time_ms = 24;
      break;
    case TDK_ICP_ULTRA_LOW_NOISE:
      cmd = TDK_ICP_START_MEAS_ULN;
      tdk_meas_start_cmd[0] = (cmd >> 8) & 0xff;
      tdk_meas_start_cmd[1] = cmd & 0xff;
      tdk_min_meas_time_ms = 95;
      break;
  }
  s->read_requested=false;
  s->read_started=false;
}


void tdk_icp_read_otp(tdk_icp_sensor_t * s, short *out)
{
  uint8_t data_read[12] = { 0 };
  // OTP Read mode command:
  uint8_t otp_cmd[5] = {0xC5,0x95,0x00,0x66,0x9C};

  // OTP incremental read command:
  uint8_t otp_inc_read_cmd[2] = {0xC7,0xF7};
  // Need 9 transactions: first to put device into OTP read mode, then 4 x OTP incremental read command followed by data reads.
  nrf_twi_mngr_transfer_t trans[9];
  twi_write_transfer(TDK_ICP_ADDRESS,otp_cmd,5,&trans[0]);
  for (int i=0;i<4;i++)
  {
    twi_write_transfer(TDK_ICP_ADDRESS,otp_inc_read_cmd,2,&trans[i*2+1]);
    twi_read_transfer(TDK_ICP_ADDRESS,&data_read[i*3],3,&trans[i*2+2]);
  }
  // Perform transfers:
  twi_perform_transfers(trans,9,s->twi_instance);
  // Decode data:
  for (uint8_t i = 0; i < 4; i++) 
  {
    out[i] = data_read[3*i] << 8 | data_read[3*i+1];
  }
}

void tdk_icp_init_base(tdk_icp_sensor_t * s, short *otp)
{
  for (int i = 0; i < 4; i++)
  {
    s->sensor_constants[i] = (float)otp[i];
    NRF_LOG_INFO("otp[%u] = %u",i,otp[i]);
  }
  s->p_Pa_calib[0] = 45000.0;
  s->p_Pa_calib[1] = 80000.0;
  s->p_Pa_calib[2] = 105000.0;
  s->LUT_lower = 3.5f * (float)((uint32_t)1 << 20);
  s->LUT_upper = 11.5 * ((uint32_t)1 << 20);
  s->quadr_factor = 1.0 / 16777216.0;
  s->offst_factor = 2048.0;
}

// p_LSB -- Raw pressure data from sensor
// T_LSB -- Raw temperature data from sensor
void tdk_icp_process_data(tdk_icp_sensor_t * s)
{
  float t;
  float s1, s2, s3;
  float in[3];
  float out[3];
  float A, B, C;
  t = (float)(s->raw_t - 32768);
  s1 = s->LUT_lower + (float)(s->sensor_constants[0] * t * t) * s->quadr_factor;
  s2 = s->offst_factor * s->sensor_constants[3] + (float)(s->sensor_constants[1] * t * t) * s->quadr_factor;
  s3 = s->LUT_upper + (float)(s->sensor_constants[2] * t * t) * s->quadr_factor;
  in[0] = s1;
  in[1] = s2;
  in[2] = s3;
  tdk_icp_calc_conversion_constants(s, s->p_Pa_calib, in, out);

  A = out[0];
  B = out[1];
  C = out[2];

  s->pressure = A + B / (C + s->raw_p);
  s->temperature = -45.f + 175.f / 65536.f * s->raw_t;

}

// p_Pa -- List of 3 values corresponding to applied pressure in Pa
// p_LUT -- List of 3 values corresponding to the measured p_LUT values at the applied pressures.
void tdk_icp_calc_conversion_constants(tdk_icp_sensor_t * s, float *p_Pa, float *p_LUT, float *out)
{
  float A, B, C;
  C = (p_LUT[0] * p_LUT[1] * (p_Pa[0] - p_Pa[1]) +
          p_LUT[1] * p_LUT[2] * (p_Pa[1] - p_Pa[2]) +
          p_LUT[2] * p_LUT[0] * (p_Pa[2] - p_Pa[0])) /
          (p_LUT[2] * (p_Pa[0] - p_Pa[1]) +
          p_LUT[0] * (p_Pa[1] - p_Pa[2]) +
          p_LUT[1] * (p_Pa[2] - p_Pa[0]));
  A = (p_Pa[0] * p_LUT[0] - p_Pa[1] * p_LUT[1] - (p_Pa[1] - p_Pa[0]) * C) / (p_LUT[0] - p_LUT[1]);
  B = (p_Pa[0] - A) * (p_LUT[0] + C);
  out[0] = A;
  out[1] = B;
  out[2] = C;
}

void tdk_meas_start_transfer(tdk_icp_sensor_t * s, nrf_twi_mngr_transfer_t * p_trans)
{
  twi_write_transfer(TDK_ICP_ADDRESS,tdk_meas_start_cmd,2,p_trans);
  // Assume this will be sent right away, so set timer.
}

void tdk_get_meas_transfer(tdk_icp_sensor_t  * s, uint8_t * p_data, nrf_twi_mngr_transfer_t * p_trans)
{
  twi_read_transfer(TDK_ICP_ADDRESS, p_data, 9, p_trans);
}

void tdk_get_data_from_raw(tdk_icp_sensor_t * s, uint8_t * p_data)
{
  s->raw_t = ((int32_t)p_data[0] << 8) | ((int32_t)p_data[1]);
  s->raw_p = ((int32_t)p_data[3] << 16) | ((int32_t)p_data[4] << 8) | ((int32_t)p_data[6]);
  tdk_icp_process_data(s);
}
