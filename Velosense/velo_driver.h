#ifndef VELO_DRIVER_H__
#define VELO_DRIVER_H__

#include <string.h>
#include <stdint.h>
#include <math.h>
#include "velo_config.h"

#include "ant_velo.h"
#include "ant_bpwr.h"
#include "ant_bsc.h"
#include "ant_aero.h"

#include "velo_settings.h"
#include "velo_calibration.h"

extern uint8_t ant_velo_period_index;
extern uint8_t velo_poll_count;
extern uint8_t velo_max_poll_count;

/**@brief Returns microsecond clock count. */
uint32_t time_us(void);

///**@brief Function for initializing timer. */
//void velo_timer_init(void);

///**@brief Callback function - does nothing. */
//void velo_timer_handler(nrf_timer_event_t event_type, void* p_context);

/**@brief Function for initializing twi reader. */
void twi_read_init(void);

/**@brief Function for reading sensors. */
void read_all(void);
	
 /**@brief Initialisation function. */
void velo_init(void);

void velo_start_pairing(void);
void velo_stop_pairing(void);
void cid_debug(void);
void velo_toggle_ant_period(void * p_event_data, uint16_t event_size);
#endif //VELO_DRIVER_H__

