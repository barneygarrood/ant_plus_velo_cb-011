#ifndef VELO_SETTINGS_H__
#define VELO_SETTINGS_H__


#include <stdint.h>
#include "velo_flash.h"
#include "velo_config.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#include "crc16.h"

#include "ant_aero.h"   // <- so we can pass peripheral device types around.

#define VELO_CONFIG_BLOCK_SIZE      128
#define VELO_CONFIG_CRC_LOC         4
#define VELO_SETTINGS_COUNT         9
#define VELO_SETTINGS_START         8 
// Velo settings buffer has to be an integer number of bytes, so pad out to a multiple of 8.
#define VELO_SETTINGS_BUFFER_SIZE   (8 + VELO_SETTINGS_COUNT*3 + ((VELO_SETTINGS_COUNT*3) % 8 ? 8 - (VELO_SETTINGS_COUNT*3) % 8 : 0))    ///< Number of burst packets to send.

#define VELO_PWR_DEVICE_SETTINGS_START         96 
#define VELO_SPD_DEVICE_SETTINGS_START         104 

#define VELO_CID_START  32
#define VELO_CID_COUNT  7
#define VELO_CID_SIZE  8

// Define all default settings:
#define VELO_DEFAULT_ELEVATION      0     // 0.3m
#define VELO_DEFAULT_CRR            140   // 0.00005Crr
#define VELO_DEFAULT_TIME_AV_PERIOD 960   // 1/32s
#define VELO_DEFAULT_TIME_AV_MIDPT  480   // 1/32s
#define VELO_DEFAULT_RIDER_WEIGHT   7000  // 0.01kg
#define VELO_DEFAULT_BIKE_WEIGHT    500   // 0.01kg
#define VELO_DEFAULT_WHEEL_CIRC     2070  // mm
#define VELO_DEFAULT_CTF_CAL        0     // Hz
#define VELO_DEFAULT_LATITUDE       0     // 0.005 deg

// Structure to contain config header:
typedef struct {

  uint8_t uninitialised :1;
  uint8_t active        :1;
  uint8_t unused        :2;
  uint8_t counter       :4;

  uint8_t unused_1[3];
  uint16_t crc;
  uint8_t unused_2[2];
 
} velo_config_header_t;
  

 
typedef struct {
    uint32_t serial_number;
    uint8_t cid;
  } velo_controller_id_t;

static uint8_t velo_settings_layout[VELO_SETTINGS_COUNT] = {
                                              19,
                                              223,
                                              222,
                                              220,
                                              10,
                                              17,
                                              18,
                                              221,
                                              219,
                                            };

void velo_load_config();
void velo_settings_get(uint16_t * p_velo_settings);
void velo_settings_set(uint16_t * p_velo_settings);
void velo_get_create_cid(velo_controller_id_t * p_controller_id);
void velo_load_pwr_sensor(ant_aero_peripheral_device_t * p_device);
void velo_load_spd_sensor(ant_aero_peripheral_device_t * p_device);
void velo_save_pwr_sensor(ant_aero_peripheral_device_t * p_device);
void velo_save_spd_sensor(ant_aero_peripheral_device_t * p_device);



#endif //VELO_SETTINGS_H__