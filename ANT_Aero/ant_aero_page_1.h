#ifndef ANT_AERO_PAGE_1_H__
#define ANT_AERO_PAGE_1_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

/*
/  Data packet structure
*/
typedef struct 
{
  struct 
  {
    uint8_t aero_event_count  : 5;
    uint8_t valid_cda         : 1;
    uint8_t valid_wind_yaw    : 1;
    uint8_t valid_crr         : 1;
  } event_count_and_valids;

  uint8_t cda_accumulated[2];
  uint8_t wind_yaw_accumulated[2];
  uint8_t crr_accumulated[2];

} ant_aero_page_1_data_layout_t;

/*
/  Page data structure
*/
typedef struct 
{
  uint8_t aero_event_count  : 5;
  uint8_t valid_cda         : 1;
  uint8_t valid_wind_yaw    : 1;
  uint8_t valid_crr         : 1;
  uint16_t cda_accumulated;
  uint16_t wind_yaw_accumulated;
  uint16_t crr_accumulated;
} ant_aero_page_1_data_t;

//Initialiser
#define ANT_AERO_PAGE_1(event_count_, valid_cda_, valid_wind_yaw_, valid_crr_, cda_accumulated_, wind_yaw_accumulated_, crr_accumulated_)  \
    (ant_aero_page_1_data_t)                                \
    {                                                       \
        .aero_event_count = (event_count_),                 \
        .valid_cda = (valid_cda_),                          \
        .valid_wind_yaw = (valid_wind_yaw_),                \
        .valid_crr = (valid_crr_),                          \
        .cda_accumulated = (cda_accumulated_),              \
        .wind_yaw_accumulated = (wind_yaw_accumulated_),    \
        .crr_accumulated = (crr_accumulated_),              \
    }

void ant_aero_page_1_encode(uint8_t * p_page_buffer, ant_aero_page_1_data_t * p_page_data);

void ant_aero_page_1_data_log(ant_aero_page_1_data_t const * p_page_data);

void ant_aero_page_1_update(ant_aero_page_1_data_t * p_page_data, uint8_t event_count, uint8_t valid_cda, uint8_t valid_wind_yaw, uint8_t valid_crr, uint16_t cda, uint16_t wind_yaw, uint16_t crr);
           
#endif //ANT_AERO_PAGE_1_H__
