#include "ant_aero_page_31.h"

static uint8_t m_burst_buffer[BURST_BLOCK_SIZE];
static uint8_t m_buffer_size = 0;

void ant_aero_page_31_fill_buffer(uint8_t * burst_buffer, uint8_t buffer_size)
{
  m_buffer_size = buffer_size;
  memcpy(m_burst_buffer,burst_buffer,buffer_size);
}

// Transfer data to buffer, and return size of resulting buffer.
// I wonder if there is a better way to do this, for example return pointer to static buffer?
// haven't managed to make that work in a neat way though.
void ant_aero_page_31_get_buffer(uint8_t * p_buffer, uint8_t * p_buffer_size)
{
  *p_buffer_size =m_buffer_size;
  memcpy(p_buffer,m_burst_buffer,*p_buffer_size);
}