#include "ant_common_page_86.h"

void ant_common_page_86_encode(uint8_t * p_page_buffer, ant_common_page_86_data_t * p_page_data)
{
  ant_common_page_86_data_layout_t * p_page_out  = (ant_common_page_86_data_layout_t *) p_page_buffer;

  p_page_out->device_index = p_page_data->device_index;
  p_page_out->device_count = p_page_data->device_count;
  p_page_out->channel_state.byte = p_page_data->channel_state.byte;
  UNUSED_PARAMETER(uint16_encode(p_page_data->device_number,p_page_out->device_number));
  p_page_out->transmission_type = p_page_data->transmission_type.byte;
  p_page_out->device_type = p_page_data->device_type;
}


void ant_common_page_86_data_log(ant_common_page_86_data_t * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 86 START ******************************");
    NRF_LOG_INFO("device index:                                 %u", p_page_data->device_index);
    NRF_LOG_INFO("device count:                                 %u", p_page_data->device_count);
    NRF_LOG_INFO("channel state:                                %u", p_page_data->channel_state.byte);
    NRF_LOG_INFO("device number:                                %u", p_page_data->device_index);
    NRF_LOG_INFO("transmission type:                            %u", p_page_data->transmission_type.byte);
    NRF_LOG_INFO("device type:                                  %u", p_page_data->device_type);
}
