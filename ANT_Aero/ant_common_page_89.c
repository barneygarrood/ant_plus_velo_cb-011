#include "ant_common_page_89.h"

void ant_common_page_89_decode(uint8_t * p_page_buffer, ant_common_page_89_data_t * p_page_data)
{
  ant_common_page_89_data_layout_t * p_page_in = (ant_common_page_89_data_layout_t *) p_page_buffer;

  // Translate data from data object into data layout:
  p_page_data->controller_id =   p_page_in->cid_and_reserved.controller_id;
  p_page_data->data = uint32_decode(p_page_in->data);
  ant_common_page_89_data_log(p_page_data);
 }

void ant_common_page_89_encode(uint8_t * p_page_buffer, ant_common_page_89_data_t * p_page_data)
{
  ant_common_page_89_data_layout_t * p_page_out = (ant_common_page_89_data_layout_t *) p_page_buffer;

  // Translate data from data object into data layout:
  p_page_out->type=0;
  p_page_out->reserved_1=0xFF;
  p_page_out->cid_and_reserved.controller_id = p_page_data->controller_id;
  p_page_out->cid_and_reserved.reserved_2 = 0x00;
  UNUSED_PARAMETER(uint32_encode(p_page_data->data,p_page_out->data));
  //ant_common_page_89_data_log(p_page_data);
 }

void ant_common_page_89_data_log(ant_common_page_89_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 89 START ******************************");
    NRF_LOG_INFO("controller_id:                                %u", p_page_data->controller_id);
    NRF_LOG_INFO("data:                                         %u", p_page_data->data);
}


void ant_common_page_89_update(ant_common_page_89_data_t * p_page_data, uint8_t controller_id, uint32_t data)
{
  p_page_data->controller_id = controller_id;
  p_page_data->data = data;
}