#ifndef ANT_AERO_PAGE_19_H__
#define ANT_AERO_PAGE_19_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

/*
/  Data packet structure
*/
typedef struct 
{
  struct 
  {
    uint8_t event_count               : 5;
    uint8_t reserved                  : 2;
    uint8_t valid_pwr                 : 1;
  } event_count_and_valids;

  uint8_t user_pwr_pct;
  uint8_t grade_pwr_pct;
  uint8_t friction_pwr_pct;
  uint8_t wind_pwr_pct;
  uint8_t pwr_denom_accum[2];

} ant_aero_page_19_data_layout_t;

/*
/  Page data structure
*/
typedef struct 
{
  uint8_t event_count               : 5;
  uint8_t reserved                  : 2;
  uint8_t valid_pwr                 : 1;
  uint8_t user_pwr_pct;
  uint8_t grade_pwr_pct;
  uint8_t friction_pwr_pct;
  uint8_t wind_pwr_pct;
  uint16_t pwr_denom_accum;
} ant_aero_page_19_data_t;

//Initialiser
#define ANT_AERO_PAGE_19(event_count_, valid_pwr_, user_pwr_pct_, grade_pwr_pct_, friction_pwr_pct_, wind_pwr_pct_, pwr_denom_accum_)  \
    (ant_aero_page_19_data_t)                                \
    {                                                        \
        .event_count = (event_count_),                        \
        .valid_pwr = (valid_pwr_),\
        .user_pwr_pct = (user_pwr_pct_),\
        .grade_pwr_pct = (grade_pwr_pct_),\
        .friction_pwr_pct = (friction_pwr_pct_),\
        .wind_pwr_pct = (wind_pwr_pct_),\
        .pwr_denom_accum = (pwr_denom_accum_),\
    }

void ant_aero_page_19_encode(uint8_t * p_page_buffer, ant_aero_page_19_data_t * p_page_data);

void ant_aero_page_19_data_log(ant_aero_page_19_data_t const * p_page_data);

void ant_aero_page_19_update(ant_aero_page_19_data_t * p_page_data, uint8_t event_count, uint8_t valid_pwr, uint8_t user_pwr_pct, uint8_t grade_pwr_pct, 
                            uint8_t friction_pwr_pct, uint8_t wind_pwr_pct, float pwr_denom);
#endif //ANT_AERO_PAGE_19_H__
