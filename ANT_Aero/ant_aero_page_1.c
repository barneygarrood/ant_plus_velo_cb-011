#include "ant_aero_page_1.h"

void ant_aero_page_1_encode(uint8_t * p_page_buffer, ant_aero_page_1_data_t * p_page_data)
{
  ant_aero_page_1_data_layout_t * p_page_out = (ant_aero_page_1_data_layout_t *) p_page_buffer;

  // Translate data from data object into data layout:
  p_page_out->event_count_and_valids.aero_event_count = p_page_data->aero_event_count;
  p_page_out->event_count_and_valids.valid_cda = p_page_data->valid_cda;
  p_page_out->event_count_and_valids.valid_wind_yaw = p_page_data->valid_wind_yaw;
  p_page_out->event_count_and_valids.valid_crr = p_page_data->valid_crr;
  UNUSED_PARAMETER(uint16_encode(p_page_data->cda_accumulated, p_page_out->cda_accumulated));
  UNUSED_PARAMETER(uint16_encode(p_page_data->wind_yaw_accumulated, p_page_out->wind_yaw_accumulated));
  UNUSED_PARAMETER(uint16_encode(p_page_data->crr_accumulated, p_page_out->crr_accumulated));
  //ant_aero_page_1_data_log(p_page_data);
 }

void ant_aero_page_1_data_log(ant_aero_page_1_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 1 START ******************************");
    NRF_LOG_INFO("event_count:					%u", p_page_data->aero_event_count);
    NRF_LOG_INFO("valid_cda:					%u", p_page_data->valid_cda);
    NRF_LOG_INFO("valid_wind_yaw:				%u", p_page_data->valid_wind_yaw);
    NRF_LOG_INFO("valid_crr:					%u", p_page_data->valid_crr);
    NRF_LOG_INFO("cda_accumulated:                              %u", p_page_data->cda_accumulated);
    NRF_LOG_INFO("wind_yaw_accumulated:                         %u", p_page_data->wind_yaw_accumulated);
    NRF_LOG_INFO("crr_accumulated:                              %u", p_page_data->crr_accumulated);
}


void ant_aero_page_1_update(ant_aero_page_1_data_t * p_page_data, uint8_t event_count, uint8_t valid_cda, uint8_t valid_wind_yaw, uint8_t valid_crr, uint16_t cda, uint16_t wind_yaw, uint16_t crr)
{
  p_page_data->aero_event_count = event_count;
  p_page_data->valid_cda = valid_cda;
  p_page_data->valid_wind_yaw = valid_wind_yaw;
  p_page_data->valid_crr = valid_crr;
  p_page_data->cda_accumulated += cda;
  p_page_data->wind_yaw_accumulated += wind_yaw;
  p_page_data->crr_accumulated += crr;
}