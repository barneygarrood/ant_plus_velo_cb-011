#ifndef ANT_COMMON_PAGE_89_H__
#define ANT_COMMON_PAGE_89_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

#define ANT_COMMON_PAGE_89 (89)
/*
/  Data packet structure
*/
typedef struct 
{

  uint8_t type;
  uint8_t reserved_1;
  struct 
  {
    uint8_t controller_id   : 4;
    uint8_t reserved_2      : 4;
  } cid_and_reserved;
  uint8_t data[4];

} ant_common_page_89_data_layout_t;

/*
/  Page data structure
*/
typedef struct 
{
  uint8_t controller_id  : 4;
  uint32_t data;
} ant_common_page_89_data_t;

//Initialiser
#define DEFAULT_ANT_COMMON_PAGE_89(controller_id_, data_)   \
    (ant_common_page_89_data_t)                        \
    {                                               \
        .controller_id = (controller_id_),          \
        .data = (data_),                            \
    }

void ant_common_page_89_encode(uint8_t * p_page_buffer, ant_common_page_89_data_t * p_page_data);
void ant_common_page_89_decode(uint8_t * p_page_buffer, ant_common_page_89_data_t * p_page_data);

void ant_common_page_89_data_log(ant_common_page_89_data_t const * p_page_data);

void ant_common_page_89_update(ant_common_page_89_data_t * p_page_data, uint8_t controller_d, uint32_t data);
           
#endif //ANT_COMMON_PAGE_89_H__
