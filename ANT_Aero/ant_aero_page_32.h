#ifndef ANT_AERO_PAGE_32_H__
#define ANT_AERO_PAGE_32_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

// Packet structure
typedef struct
{
  struct
  {
    uint8_t settings_counter  :4;
    uint8_t reserved          :1;
    uint8_t controller_id     :3;
  } settings_counter_and_cid;

  struct 
  {
    uint8_t supported_8hz     :1;
    uint8_t supported_calibration :1;
    uint8_t reserved          :6;
  } supported_features;

  uint8_t reserved_1;
  uint8_t reserved_2;
  struct 
  {
    uint8_t enabled_8hz       :1;
    uint8_t start_end_calibration :1;
    uint8_t reserved          :6;
  } enabled_features;
  uint8_t reserved_3;
  uint8_t reserved_4;
} ant_aero_page_32_data_layout_t;

// Page data structure:
typedef struct
{
  uint8_t settings_counter;
  uint8_t enabled_8hz;
  uint8_t start_end_calibration;
  uint8_t controller_id;
} ant_aero_page_32_data_t;

// Initialiser:
#define INITIAL_ANT_AERO_PAGE_32(settings_counter_, enabled_8hz_, start_end_calibration_, controller_id_) \
   (ant_aero_page_32_data_t)              \
{                                         \
   .settings_counter = settings_counter_,  \
   .enabled_8hz = enabled_8hz_,            \
   .start_end_calibration = start_end_calibration_,\
   .controller_id = controller_id_,        \
}


// Functions:
void ant_aero_page_32_encode(uint8_t * p_page_buffer, ant_aero_page_32_data_t * p_page_data);

void ant_aero_page_32_decode(uint8_t * p_page_buffer, ant_aero_page_32_data_t * p_page_data);

void ant_aero_page_32_data_log(ant_aero_page_32_data_t * p_page_data);

#endif // ANT_AERO_PAGE_32_H__
