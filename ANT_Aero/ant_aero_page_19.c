#include "ant_aero_page_19.h"

void ant_aero_page_19_encode(uint8_t * p_page_buffer, ant_aero_page_19_data_t * p_page_data)
{
  ant_aero_page_19_data_layout_t * p_page_out = (ant_aero_page_19_data_layout_t *) p_page_buffer;

  // Translate data from data object into data layout:
  p_page_out->event_count_and_valids.event_count = p_page_data->event_count;
  p_page_out->event_count_and_valids.valid_pwr = p_page_data->valid_pwr;
  p_page_out->user_pwr_pct = p_page_data->user_pwr_pct;
  p_page_out->grade_pwr_pct = p_page_data->grade_pwr_pct;
  p_page_out->friction_pwr_pct = p_page_data->friction_pwr_pct;
  p_page_out->wind_pwr_pct = p_page_out->wind_pwr_pct;
  UNUSED_PARAMETER(uint16_encode(p_page_data->pwr_denom_accum,p_page_out->pwr_denom_accum));

  //ant_aero_page_19_data_log(p_page_data);
}

void ant_aero_page_19_data_log(ant_aero_page_19_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 19 START *****************************");
    NRF_LOG_INFO("event_count:					%u", p_page_data->event_count);
    NRF_LOG_INFO("valid_pwr:                                    %u", p_page_data->valid_pwr);
    NRF_LOG_INFO("user_pwr_pct:                                 %u", p_page_data->user_pwr_pct);
    NRF_LOG_INFO("grade_pwr_pct:                                %u", p_page_data->grade_pwr_pct);
    NRF_LOG_INFO("friction_pwr_pct:                             %u", p_page_data->friction_pwr_pct);
    NRF_LOG_INFO("wind_pwr_pct:                                 %u", p_page_data->wind_pwr_pct);
    NRF_LOG_INFO("pwr_denom_accum:                              %u", p_page_data->pwr_denom_accum);

}

void ant_aero_page_19_update(ant_aero_page_19_data_t * p_page_data, uint8_t event_count, uint8_t valid_pwr, uint8_t user_pwr_pct, uint8_t grade_pwr_pct, 
                            uint8_t friction_pwr_pct, uint8_t wind_pwr_pct, float pwr_denom)
{
  p_page_data->event_count = event_count;
  p_page_data->valid_pwr = valid_pwr;
  p_page_data->user_pwr_pct = user_pwr_pct;
  p_page_data->grade_pwr_pct = grade_pwr_pct;
  p_page_data->friction_pwr_pct = friction_pwr_pct;
  p_page_data->wind_pwr_pct = wind_pwr_pct;
  p_page_data->pwr_denom_accum += (uint16_t) pwr_denom;
}