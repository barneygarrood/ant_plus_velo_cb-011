#include "ant_common_page_82.h"

void ant_common_page_82_encode(uint8_t * p_page_buffer, ant_common_page_82_data_t * p_page_data)
{
  ant_common_page_82_data_layout_t * p_page_out = (ant_common_page_82_data_layout_t * ) p_page_buffer;

  p_page_out->reserved=0xFF;           
  p_page_out->battery_identifier.battery_count=1;
  p_page_out->battery_identifier.battery_id=0;
  p_page_out->cumulative_operating_time[0]=0;
  p_page_out->cumulative_operating_time[1]=0;
  p_page_out->cumulative_operating_time[2]=0;
  p_page_out->descriptive_bit_field.second_resolution=0;

  // Note we can put these into an intialiser!
  uint8_t coarse_voltage=p_page_data->voltage/1000;
  float fractional_voltage = (p_page_data->voltage/1000.0f-coarse_voltage)*256;
  p_page_out->fractional_voltage = fractional_voltage;
  p_page_out->descriptive_bit_field.coarse_voltage = coarse_voltage;

  // Battery status based on SOC:
  if (p_page_data->soc >80)
  {
    p_page_out->descriptive_bit_field.status = 1;
  }
  else if (p_page_data->soc>60)
  {
    p_page_out->descriptive_bit_field.status = 2;
  }
  else if (p_page_data->soc>40)
  {
    p_page_out->descriptive_bit_field.status = 3;
  }
  else if (p_page_data->soc>20)
  {
    p_page_out->descriptive_bit_field.status = 4;
  }
  else
  {
    p_page_out->descriptive_bit_field.status = 5;
  }

}

void ant_common_page_82_data_log(ant_common_page_82_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 89 START ******************************");
    NRF_LOG_INFO("voltage:                                      %u", p_page_data->voltage);
    NRF_LOG_INFO("soc :                                         %u", p_page_data->soc);
}

void ant_common_page_82_update(ant_common_page_82_data_t * p_page_data, uint16_t voltage, uint8_t soc)
{
  p_page_data->voltage = voltage;
  p_page_data->soc = soc;
}

