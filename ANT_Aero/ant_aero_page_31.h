#ifndef ANT_AERO_PAGE_31_H__
#define ANT_AERO_PAGE_31_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

// Miscellaneous defines.
#define BURST_PACKET_SIZE       8u                              ///< The burst packet size, in bytes.
#define BURST_PACKET_TOTAL_SIZE (BURST_PACKET_SIZE + 1u)        ///< The burst packet total size in bytes, including channel number/sequence number.

#define BURST_BLOCK_SIZE        NRF_SDH_ANT_BURST_QUEUE_SIZE    ///< Burst block size, in number of bytes.

void ant_aero_page_31_fill_buffer(uint8_t * burst_buffer, uint8_t buffer_size);
void ant_aero_page_31_get_buffer(uint8_t * p_buffer, uint8_t * p_buffer_size);           
#endif //ANT_AERO_PAGE_31_H__