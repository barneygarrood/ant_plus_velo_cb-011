#include "ant_aero_page_32.h"

void ant_aero_page_32_encode(uint8_t * p_page_buffer, ant_aero_page_32_data_t * p_page_data)
{
  ant_aero_page_32_data_layout_t * p_page_out = (ant_aero_page_32_data_layout_t * ) p_page_buffer;

  p_page_out->settings_counter_and_cid.settings_counter = p_page_data->settings_counter;
  p_page_out->settings_counter_and_cid.reserved = 0;
  p_page_out->settings_counter_and_cid.controller_id = 0;

  p_page_out->supported_features.supported_8hz = 1;
  p_page_out->supported_features.supported_calibration = 0; // for now..
  p_page_out->supported_features.reserved=0;

  p_page_out->reserved_1=0;
  p_page_out->reserved_2=0;

  p_page_out->enabled_features.enabled_8hz = p_page_data->enabled_8hz;
  p_page_out->enabled_features.start_end_calibration = p_page_out->enabled_features.start_end_calibration;
  p_page_out->enabled_features.reserved=0;

  p_page_out->reserved_3=0;
  p_page_out->reserved_4=0;

  ant_aero_page_32_data_log(p_page_data);
}

void ant_aero_page_32_decode(uint8_t * p_page_buffer, ant_aero_page_32_data_t * p_page_data)
{
  ant_aero_page_32_data_layout_t * p_page_in = (ant_aero_page_32_data_layout_t * ) p_page_buffer;

  p_page_data->controller_id = p_page_in->settings_counter_and_cid.controller_id;

  // If display has supported_8hz set to true, then we need to consider the enable command:
  if (p_page_in->supported_features.supported_8hz)
  {
    p_page_data->enabled_8hz = p_page_in->enabled_features.enabled_8hz;
  }

  // If display has supported_calibration set to true, then we need to consider the enable command:
  if (p_page_in->supported_features.supported_calibration)
  {
    p_page_data->start_end_calibration = p_page_in->enabled_features.start_end_calibration;
  }
  ant_aero_page_32_data_log(p_page_data);
}

void ant_aero_page_32_data_log(ant_aero_page_32_data_t * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 32 START ******************************");
    NRF_LOG_INFO("Settings counter:                             %u", p_page_data->settings_counter);
    NRF_LOG_INFO("8 Hz enabled:                                 %u", p_page_data->enabled_8hz);
    NRF_LOG_INFO("Start/end calibration:                        %u", p_page_data->start_end_calibration);
    NRF_LOG_INFO("Controller ID:                                %u", p_page_data->controller_id);
}

