#include "ant_aero_page_20.h"

void ant_aero_page_20_encode(uint8_t * p_page_buffer, ant_aero_page_20_data_t * p_page_data)
{
  ant_aero_page_20_data_layout_t * p_page_out = (ant_aero_page_20_data_layout_t *) p_page_buffer;

  // Translate data from data object into data layout:
  p_page_out->event_count_and_valids.event_count = p_page_data->event_count;
  p_page_out->event_count_and_valids.valid_baro_press = p_page_data->valid_baro_press;
  p_page_out->event_count_and_valids.valid_temp = p_page_data->valid_temp;
  UNUSED_PARAMETER(uint24_encode(p_page_data->baro_press_accum,p_page_out->baro_press_accum));
  UNUSED_PARAMETER(uint16_encode(p_page_data->temp_accum, p_page_out->temp_accum));
  p_page_out->humidity = p_page_data->humidity;
  //ant_aero_page_20_data_log(p_page_data);
}

void ant_aero_page_20_data_log(ant_aero_page_20_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 20 START *****************************");
    NRF_LOG_INFO("event_count:					%u", p_page_data->event_count);
    NRF_LOG_INFO("valid_baro_press:                             %u", p_page_data->valid_baro_press);
    NRF_LOG_INFO("valid_temp:                                   %u", p_page_data->valid_temp);
    NRF_LOG_INFO("baro_press_accum:                             %u", p_page_data->baro_press_accum);
    NRF_LOG_INFO("temp_accum:                                   %u", p_page_data->temp_accum);
    NRF_LOG_INFO("humidity:                                     %u", p_page_data->humidity);
}

void ant_aero_page_20_update(ant_aero_page_20_data_t * p_page_data, uint8_t event_count, uint8_t valid_baro_press, uint8_t valid_temp, 
                            float baro_press, float temp, uint8_t humidity)
{
  p_page_data->event_count = event_count;
  p_page_data->valid_baro_press = valid_baro_press;
  p_page_data->valid_temp = valid_temp;
  p_page_data->baro_press_accum += (uint32_t) (baro_press * 20.0f);
  p_page_data->temp_accum += (uint16_t) (temp * 100);
  p_page_data->humidity = humidity;
}