#ifndef ANT_AERO_H__
#define ANT_AERO_H__

#include <stdint.h>
#include "ant_parameters.h"
#include "ant_interface.h"
#include "nrf_sdh_ant.h"
#include "ant_channel_config.h"
#include "ant_aero_pages.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#define AERO_DEVICE_TYPE          0x2E  // ANT+ Aero device type.
#define AERO_ANTPLUS_RF_FREQ      57u   // ANT channel frequency.
#define AERO_MSG_8HZ_PERIOD       4096u // ANT messsage period: 4096/32768 = 8Hz.
#define AERO_MSG_4HZ_PERIOD       8192u // ANT messsage period: 4096/32768 = 8Hz.
#define AERO_EXT_ASSIGN           0x00u // ANT ext assign (see Ext. Assign Channel Parameters in ant_parameters.h: @ref ant_parameters).
#define AERO_SENSOR_CHANNEL_TYPE  CHANNEL_TYPE_MASTER

#define POWER_DEVICE_TYPE   0x0B
#define S_AND_C_DEVICE_TYPE 0x79
#define SPEED_DEVICE_TYPE   0x7B

/**@brief Message data layout structure. */
typedef struct
{
    uint8_t page_number;
    uint8_t page_payload[7];
} ant_aero_message_layout_t;

typedef struct
{
  uint16_t device_number;
  union
  {
    struct
    {
      uint8_t channel_type : 2;
      uint8_t global_data_pages_used : 1;
      uint8_t undefined : 1;
      uint8_t device_no_ext : 4;
    }bitfields;
    uint8_t byte;
  } transmission_type;

  uint8_t device_type;
  uint8_t rf_freq;
  uint16_t channel_period;

  union
  {
    struct 
    {
      uint8_t state : 4;
      uint8_t pair : 1;
      uint8_t saved : 1;
    } bitfields;
    uint8_t byte;
  } channel_state;

}ant_aero_peripheral_device_t;


/* 
/   Control block
*/
typedef struct
{
  uint8_t packet_count;
  uint8_t response_count;
  uint16_t master_page_count;
  uint8_t sub_page_count;

  bool enabled_8hz;
  bool event_is_tx;
  bool send_requested_page;
  bool send_p89;

  uint8_t current_ant_freq;

  ant_aero_peripheral_device_t power_sensor;
  ant_aero_peripheral_device_t speed_sensor;
  ant_aero_peripheral_device_t s_and_c_sensor;

} ant_aero_cb_t;

/**@brief Initialize an ANT channel configuration structure.
 *
 * @param[in]  NAME                 Name of related instance.
 * @param[in]  CHANNEL_NUMBER       Number of the channel assigned to the profile instance.
 * @param[in]  TRANSMISSION_TYPE    Type of transmission assigned to the profile instance.
 * @param[in]  DEVICE_NUMBER        Number of the device assigned to the profile instance.
 * @param[in]  NETWORK_NUMBER       Number of the network assigned to the profile instance.
 */
#define AERO_CHANNEL_CONFIG_DEF(NAME,                                      	\
                                     CHANNEL_NUMBER,                            \
                                     TRANSMISSION_TYPE,                         \
                                     AERO_MSG_PERIOD,                           \
                                     NETWORK_NUMBER)                            \
static ant_channel_config_t   CONCAT_2(NAME, _channel_config) = 		\
    {                                                                           \
        .channel_number    = (CHANNEL_NUMBER),                                  \
        .channel_type      = AERO_SENSOR_CHANNEL_TYPE,                          \
        .ext_assign        = AERO_EXT_ASSIGN,                                   \
        .rf_freq           = AERO_ANTPLUS_RF_FREQ ,                             \
        .transmission_type = (TRANSMISSION_TYPE),                               \
        .device_type       = AERO_DEVICE_TYPE, 					\
        .channel_period    = (AERO_MSG_PERIOD),                                   \
        .network_number    = (NETWORK_NUMBER),                                  \
    }

#define AERO_CHANNEL_CONFIG(NAME) &CONCAT_2(NAME, _channel_config)

/**@brief Initialize an ANT profile configuration structure
 *
 * @param[in]  NAME                 Name of related instance.
 * @param[in]  EVT_HANDLER          Event handler to be called for handling events in the BPWR profile.
 */
#define AERO_PROFILE_CONFIG_DEF(NAME, EVT_HANDLER)                                  \
static ant_aero_cb_t            CONCAT_2(NAME, _cb);                                \
static const ant_aero_config_t  CONCAT_2(NAME, _profile_config) =                   \
    {                                                                               \
        .p_cb               = &CONCAT_2(NAME, _cb),                                 \
        .evt_handler        = (EVT_HANDLER),                                        \
    }
#define AERO_PROFILE_CONFIG(NAME) &NAME##_profile_config
		
 /**@brief Page number type. */
typedef enum
{
    ANT_AERO_PAGE_1 = 1,
    ANT_AERO_PAGE_18 = 18,
    ANT_AERO_PAGE_19 = 19,
    ANT_AERO_PAGE_20 = 20,
    ANT_AERO_PAGE_31 = 31,
    ANT_AERO_PAGE_32 = 32,
    ANT_AERO_PAGE_70 = ANT_COMMON_PAGE_70,
    ANT_AERO_PAGE_80 = ANT_COMMON_PAGE_80,
    ANT_AERO_PAGE_81 = ANT_COMMON_PAGE_81,
    ANT_AERO_PAGE_82 = ANT_COMMON_PAGE_82,
    ANT_AERO_PAGE_86 = ANT_COMMON_PAGE_86,
    ANT_AERO_PAGE_89 = ANT_COMMON_PAGE_89,
} ant_aero_page_t;

// Forward declaration of the ant_aero_profile_t type.
typedef struct ant_aero_profile_s ant_aero_profile_t;

/**@brief Event handler type - this defines pointer to the function. */
typedef void (* ant_aero_evt_handler_t) (ant_aero_profile_t *, ant_aero_page_t page);

/**@brief Configuration structure. */
typedef struct
{
    ant_aero_cb_t         * p_cb;          ///< Pointer to the data buffer for internal use.
    ant_aero_evt_handler_t   evt_handler;   ///< Event handler to be called for handling events.
	
} ant_aero_config_t;

 /**@brief Profile structure. */
struct ant_aero_profile_s
{
    ant_channel_config_t *      p_chan_config;  ///< Channel config
    ant_aero_cb_t * 		p_sens_cb;	///< Pointer to internal control block
    ant_aero_evt_handler_t   	evt_handler;    ///< Event handler
    ant_aero_page_1_data_t      page_1; 
    ant_aero_page_18_data_t     page_18; 
    ant_aero_page_19_data_t     page_19; 
    ant_aero_page_20_data_t     page_20;                  
    ant_aero_page_32_data_t     page_32;             
    ant_common_page_70_data_t   page_70;  
    ant_common_page80_data_t    page_80;
    ant_common_page81_data_t    page_81;
    ant_common_page_82_data_t   page_82;
    ant_common_page_86_data_t   page_86;
    ant_common_page_89_data_t   page_89;           
};

bool ant_aero_frequency_permitted(uint8_t requested_frequency);

/**@brief Function for initializing the profile instance.
 *
 * @param[in]  p_profile        Pointer to the profile instance.
 * @param[in]  p_channel_config Pointer to the ANT channel configuration structure.
 * @param[in]  p_sens_config    Pointer to the configuration structure.
 *
 */
void ant_aero_init(ant_aero_profile_t * p_profile,
                          ant_channel_config_t * p_channel_config,
                          ant_aero_config_t const * p_sens_config);

															
/**@brief Function for opening the profile instance channel for ANT Aerodynamics Sensor.
 *
 * Before calling this function, pages should be configured.
 *
 * @param[in]  p_profile        Pointer to the profile instance.
 *
 * @retval     NRF_SUCCESS      If the channel was successfully opened. Otherwise, an error code is returned.
 */
ret_code_t ant_aero_open(ant_aero_profile_t * p_profile);

/**@brief Function for handling the Sensor ANT events.
 *
 * @details This function handles all events from the ANT stack that are of interest to theprofile.
 *
 * @param[in]   p_ant_evt     Event received from the ANT stack.
 * @param[in]   p_context       Pointer to the profile instance.
 */
void ant_aero_stack_evt_handler(ant_evt_t * p_ant_evt, void * p_context);

#endif // ANT_AERO_H__