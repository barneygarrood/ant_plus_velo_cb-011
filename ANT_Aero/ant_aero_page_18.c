#include "ant_aero_page_18.h"

void ant_aero_page_18_encode(uint8_t * p_page_buffer, ant_aero_page_18_data_t * p_page_data)
{
  ant_aero_page_18_data_layout_t * p_page_out = (ant_aero_page_18_data_layout_t *) p_page_buffer;

  // Translate data from data object into data layout:
  p_page_out->event_count_and_valids.event_count = p_page_data->event_count;
  p_page_out->event_count_and_valids.valid_dyn_press = p_page_data->valid_dyn_press;
  p_page_out->event_count_and_valids.valid_inst_wind_yaw = p_page_data->valid_inst_wind_yaw;
  UNUSED_PARAMETER(uint24_encode(p_page_data->dyn_press_accum, p_page_out->dyn_press_accum));
  UNUSED_PARAMETER(uint16_encode(p_page_data->inst_wind_yaw_accum, p_page_out->inst_wind_yaw_accum));
  //ant_aero_page_18_data_log(p_page_data);
 }

void ant_aero_page_18_data_log(ant_aero_page_18_data_t const * p_page_data)
{
    NRF_LOG_INFO("****************************** PAGE 18 START *****************************");
    NRF_LOG_INFO("event_count:					%u", p_page_data->event_count);
    NRF_LOG_INFO("valid_dyn_press:			%u", p_page_data->valid_dyn_press);
    NRF_LOG_INFO("valid_inst_wind_yaw:				%u", p_page_data->valid_inst_wind_yaw);
    NRF_LOG_INFO("dynamic_press_accum:                 %u", p_page_data->dyn_press_accum);
    NRF_LOG_INFO("inst_wind_yaw_accum:                    %u", p_page_data->inst_wind_yaw_accum);
}


void ant_aero_page_18_update(ant_aero_page_18_data_t * p_page_data, uint8_t event_count, uint8_t valid_dyn_press, uint8_t valid_inst_wind_yaw, 
                        float dyn_press, float inst_wind_yaw)
{
  p_page_data->event_count = event_count;
  p_page_data->valid_dyn_press = valid_dyn_press;
  p_page_data->valid_inst_wind_yaw = valid_inst_wind_yaw;

  uint32_t dyn_press_value = (uint32_t) (dyn_press * 1000.0f);
  uint16_t inst_wind_yaw_value = (uint16_t) ((inst_wind_yaw + 90.0f)*20.0f);

  p_page_data->dyn_press_accum += dyn_press_value;
  p_page_data->inst_wind_yaw_accum += inst_wind_yaw;
}