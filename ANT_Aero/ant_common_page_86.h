#ifndef ANT_COMMON_PAGE_86_H__
#define ANT_COMMON_PAGE_86_H__


#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

#define ANT_COMMON_PAGE_86 (86)


// packet structure
typedef struct 
{
  uint8_t device_index;
  uint8_t device_count;
  union
  {
    struct 
    {
      uint8_t network_key : 3;
      uint8_t state : 4;
      uint8_t pair : 1;
    } bitfields;
    uint8_t byte;
  } channel_state;
  
  uint8_t device_number[2];
  uint8_t transmission_type;
  uint8_t device_type;
} ant_common_page_86_data_layout_t;

// page data structure:
typedef struct
{
  uint8_t device_index;
  uint8_t device_count;
  
  union
  {
    struct 
    {
      uint8_t network_key : 3;
      uint8_t state : 4;
      uint8_t pair : 1;
    } bitfields;
    uint8_t byte;
  } channel_state;
  
  uint16_t device_number;

  union
  {
    struct
    {
      uint8_t channel_type : 2;
      uint8_t global_data_pages_used : 1;
      uint8_t undefined : 1;
      uint8_t device_no_ext : 4;
    }bitfields;
    uint8_t byte;
  } transmission_type;
  uint8_t device_type;
} ant_common_page_86_data_t;

// Initialiser:
#define DEFAULT_ANT_COMMON_PAGE_86(device_count_) \
   (ant_common_page_86_data_t)              \
{                                         \
   .device_index = 0,                      \
   .device_count = device_count_,          \
   .channel_state.byte = 0,               \
   .device_number = 0,                        \
   .transmission_type.byte = 0,           \
   .device_type = 0,                      \
}

// Functions
void ant_common_page_86_encode(uint8_t * p_page_buffer, ant_common_page_86_data_t * p_page_data);

void ant_common_page_86_data_log(ant_common_page_86_data_t * p_page_data);

#endif //ANT_COMMON_PAGE_86_H__