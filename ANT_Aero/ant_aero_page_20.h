#ifndef ANT_AERO_PAGE_20_H__
#define ANT_AERO_PAGE_20_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

/*
/  Data packet structure
*/
typedef struct 
{
  struct 
  {
    uint8_t event_count               : 5;
    uint8_t reserved                  : 1;
    uint8_t valid_baro_press          : 1;
    uint8_t valid_temp                : 1;
  } event_count_and_valids;
  uint8_t baro_press_accum[3];
  uint8_t temp_accum[2];
  uint8_t humidity;
} ant_aero_page_20_data_layout_t;

/*
/  Page data structure
*/
typedef struct 
{
  uint8_t event_count               : 5;
  uint8_t reserved                  : 1;
  uint8_t valid_baro_press          : 1;
  uint8_t valid_temp                : 1;
  uint32_t baro_press_accum         :24;
  uint16_t temp_accum;
  uint8_t humidity;
} ant_aero_page_20_data_t;

//Initialiser
#define ANT_AERO_PAGE_20(event_count_, valid_baro_press_, valid_temp_, baro_press_accum_, temp_accum_, humidity_)  \
    (ant_aero_page_20_data_t)                                \
    {                                                        \
        .event_count = (event_count_),                        \
        .valid_baro_press = (valid_baro_press_),\
        .valid_temp = (valid_temp_),\
        .baro_press_accum = (baro_press_accum_),\
        .temp_accum = (temp_accum_),\
        .humidity = (humidity_),\
    }

void ant_aero_page_20_encode(uint8_t * p_page_buffer, ant_aero_page_20_data_t * p_page_data);

void ant_aero_page_20_data_log(ant_aero_page_20_data_t const * p_page_data);

void ant_aero_page_20_update(ant_aero_page_20_data_t * p_page_data, uint8_t event_count, uint8_t valid_baro_press, uint8_t valid_temp, 
                            float baro_press, float temp, uint8_t humidity);
#endif //ANT_AERO_PAGE_20_H__
