#ifndef ANT_AERO_PAGE_18_H__
#define ANT_AERO_PAGE_18_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

/*
/  Data packet structure
*/
typedef struct 
{
  struct 
  {
    uint8_t event_count               : 5;
    uint8_t reserved                  : 1;
    uint8_t valid_dyn_press    : 1;
    uint8_t valid_inst_wind_yaw            : 1;
  } event_count_and_valids;

  uint8_t reserved;
  uint8_t dyn_press_accum[3];
  uint8_t inst_wind_yaw_accum[2];

} ant_aero_page_18_data_layout_t;

/*
/  Page data structure
*/
typedef struct 
{
  uint8_t event_count                   : 5;
  uint8_t valid_dyn_press        : 1;
  uint8_t valid_inst_wind_yaw           : 1;

  uint32_t dyn_press_accum  : 24;
  uint16_t inst_wind_yaw_accum;
} ant_aero_page_18_data_t;

//Initialiser
#define ANT_AERO_PAGE_18(event_count_, valid_dyn_press_, valid_inst_wind_yaw_, dyn_press_accum_, inst_wind_yaw_accum_)  \
    (ant_aero_page_18_data_t)                                \
    {                                                        \
        .event_count = (event_count_),                        \
        .valid_dyn_press  = (valid_dyn_press_),\
        .valid_inst_wind_yaw  = (valid_inst_wind_yaw_),    \
        .dyn_press_accum = (dyn_press_accum_),\
        .inst_wind_yaw_accum = (inst_wind_yaw_accum_),    \
    }

void ant_aero_page_18_encode(uint8_t * p_page_buffer, ant_aero_page_18_data_t * p_page_data);

void ant_aero_page_18_data_log(ant_aero_page_18_data_t const * p_page_data);

void ant_aero_page_18_update(ant_aero_page_18_data_t * p_page_data, uint8_t event_count, uint8_t valid_dyn_press, uint8_t valid_inst_wind_yaw, 
                        float dyn_press, float inst_wind_yaw);
#endif //ANT_AERO_PAGE_18_H__
