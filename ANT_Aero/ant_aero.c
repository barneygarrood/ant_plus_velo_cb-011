
#include "ant_aero.h"


// List of permitted frequencies allowed in data page request.
static uint8_t permitted_frequencies[] = {3,7,15,20,34,40,45,49,70,75};

// Function to determine if requested frequency is permitted.
bool ant_aero_frequency_permitted(uint8_t requested_frequency)
{
  bool ret=false;
  for (int i=0;i<sizeof(permitted_frequencies);i++)
  {
    if (requested_frequency == permitted_frequencies[i])
    {
      ret = true;
      break;
    }
  }
  return ret;
}

/* 
/   Profile intialisation:
*/
void ant_aero_init(ant_aero_profile_t           * p_profile,
                              ant_channel_config_t   * p_channel_config,
                              ant_aero_config_t const * p_sens_config)
{
  uint32_t err_code;

  ASSERT(p_profile != NULL);
  ASSERT(p_channel_config != NULL);
  ASSERT(p_sens_config != NULL);
      
  p_profile->evt_handler   = p_sens_config->evt_handler;
  p_profile->p_sens_cb = p_sens_config->p_cb;
  
  p_profile->p_sens_cb->packet_count = 0;
  p_profile->p_sens_cb->master_page_count=0;
  p_profile->p_sens_cb->sub_page_count=0;

  p_profile->p_sens_cb->send_requested_page=false;
  p_profile->p_sens_cb->send_p89=false;
  p_profile->p_sens_cb->current_ant_freq=p_channel_config->rf_freq;
        
  p_profile->p_chan_config = p_channel_config;

  p_profile->page_1  = ANT_AERO_PAGE_1(0,0,0,0,123,0,0);
  p_profile->page_18 = ANT_AERO_PAGE_18(0,0,0,0,0);
  p_profile->page_19 = ANT_AERO_PAGE_19(0,0,0,0,0,0,0);
  p_profile->page_20 = ANT_AERO_PAGE_20(0,0,0,0,0,0);
  p_profile->page_32 = INITIAL_ANT_AERO_PAGE_32(0,0,0,0);
  p_profile->page_70 = DEFAULT_ANT_COMMON_PAGE70();
  p_profile->page_82 = DEFAULT_ANT_COMMON_PAGE_82();
  p_profile->page_86 = DEFAULT_ANT_COMMON_PAGE_86(3);
  p_profile->page_89 = DEFAULT_ANT_COMMON_PAGE_89(0,0);


  err_code = ant_channel_init(p_channel_config);
  APP_ERROR_CHECK(err_code);

  uint8_t adv_burst_config[] =
  {
    ADV_BURST_MODE_ENABLE,
    ADV_BURST_MODES_MAX_SIZE,
    0,
    0,
    0,
    0,
    0,
    0
  };
  err_code = sd_ant_adv_burst_config_set(adv_burst_config, sizeof(adv_burst_config));
  APP_ERROR_CHECK(err_code);
}

/* 
/   Function to return next page number:
*/
static ant_aero_page_t next_page_number_get(ant_aero_profile_t * p_profile)
{
  ant_aero_page_t page_number;

  if (p_profile->p_sens_cb->send_p89)
  {
    page_number = (ant_aero_page_t) ANT_COMMON_PAGE_89;
    p_profile->p_sens_cb->response_count++;
    if (p_profile->p_sens_cb->response_count>=4)
    {
      p_profile->p_sens_cb->send_p89=false;
    }
  }
  else if (p_profile->p_sens_cb->send_requested_page)
  {
    page_number = (ant_aero_page_t) p_profile->page_70.page_number;
    p_profile->p_sens_cb->response_count++;
    NRF_LOG_INFO("response count = %u, requested count = %u",p_profile->p_sens_cb->response_count,p_profile->page_70.transmission_response.items.transmit_count);
    if (p_profile->p_sens_cb->response_count >= p_profile->page_70.transmission_response.items.transmit_count)
    {
      NRF_LOG_INFO("Resetting send_requestd_page");
      p_profile->p_sens_cb->send_requested_page=false;
    }
  }
  else
  {
    // Transmission pattern depends on data rate:
    if (p_profile->p_sens_cb->enabled_8hz)
    {
      switch (p_profile->p_sens_cb->master_page_count)
      {
        case 0:
        case 1:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_80;
          break;
        case 130:
        case 131:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_81;
          break;
        case 260:
        case 261:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_82;
          break;
        default:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_1;
          break;
      }
      p_profile->p_sens_cb->master_page_count++;
      if (p_profile->p_sens_cb->master_page_count > 389)
      {
        p_profile->p_sens_cb->master_page_count=0;
      }
    }
    else
    {
      switch (p_profile->p_sens_cb->master_page_count)
      {
        case 0:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_80;
          break;
        case 65:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_81;
          break;
        case 130:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_82;
          break;
        default:
          page_number=(ant_aero_page_t) ANT_AERO_PAGE_1;
          break;
      }
      p_profile->p_sens_cb->master_page_count++;
      if (p_profile->p_sens_cb->master_page_count > 194)
      {
        p_profile->p_sens_cb->master_page_count=0;
      }
    }

  }
  return page_number;
}

/* 
/   Function for encoding data:
*/
static void sens_message_decode(ant_aero_profile_t * p_profile, uint8_t * p_message_payload)
{
  ant_aero_message_layout_t * p_aero_message_payload = (ant_aero_message_layout_t*) p_message_payload;

  NRF_LOG_INFO("----------------------------------------------------------------------------- Received page %u",p_aero_message_payload->page_number);
  switch (p_aero_message_payload->page_number)
  {
    case ANT_AERO_PAGE_32:
      ant_aero_page_32_decode(p_aero_message_payload->page_payload,&p_profile->page_32);
      break;
    case ANT_COMMON_PAGE_70:       
      ant_common_page_70_decode(p_aero_message_payload->page_payload, &p_profile->page_70);
      break;
    case ANT_COMMON_PAGE_89:       
      ant_common_page_89_decode(p_aero_message_payload->page_payload, &p_profile->page_89);
      break;
    default:
      return;
  }
  p_profile->evt_handler(p_profile,(ant_aero_page_t) p_aero_message_payload->page_number);
}


/* 
/   Function for encoding data:
*/
static uint8_t * sens_message_encode(ant_aero_profile_t * p_profile, uint8_t * p_message_payload, uint8_t * p_packet_size)
{
  
  ant_aero_message_layout_t * p_aero_message_payload = (ant_aero_message_layout_t *) p_message_payload;

  // Get next page number:
  p_aero_message_payload->page_number = next_page_number_get(p_profile);
  switch (p_aero_message_payload->page_number)
  {
    case ANT_AERO_PAGE_1:
      *p_packet_size = ANT_STANDARD_DATA_PAYLOAD_SIZE;
      p_profile->page_1.aero_event_count++;
      ant_aero_page_1_encode(p_aero_message_payload->page_payload,&(p_profile->page_1));
      break;

    case ANT_AERO_PAGE_31:
      *p_packet_size=0;
      ant_aero_page_31_get_buffer(p_message_payload, p_packet_size);
      break;

    case ANT_AERO_PAGE_32:
      *p_packet_size = ANT_STANDARD_DATA_PAYLOAD_SIZE;
      ant_aero_page_32_encode(p_aero_message_payload->page_payload,&(p_profile->page_32));
      break;
    
    case ANT_AERO_PAGE_80:
      *p_packet_size = ANT_STANDARD_DATA_PAYLOAD_SIZE;
      ant_common_page_80_encode(p_aero_message_payload->page_payload,&(p_profile->page_80));
      break;

    case ANT_AERO_PAGE_81:
      *p_packet_size = ANT_STANDARD_DATA_PAYLOAD_SIZE;
      ant_common_page_81_encode(p_aero_message_payload->page_payload,&(p_profile->page_81));
      break;

    case ANT_AERO_PAGE_82:
      *p_packet_size = ANT_STANDARD_DATA_PAYLOAD_SIZE;
      ant_common_page_82_encode(p_aero_message_payload->page_payload,&(p_profile->page_82));
      break;

    case ANT_COMMON_PAGE_70:
      // Should never occur..
      break;

    case ANT_COMMON_PAGE_89:
      *p_packet_size = ANT_STANDARD_DATA_PAYLOAD_SIZE;
      ant_common_page_89_encode(p_aero_message_payload->page_payload,&(p_profile->page_89));
      break;

    default:
      break;
  }
  p_profile->evt_handler(p_profile,(ant_aero_page_t) p_aero_message_payload->page_number);
}

static void ant_message_send(ant_aero_profile_t * p_profile)
{
    uint32_t err_code;

    // Get payload:
    uint8_t packet_size=0;
    // Declare payload as max size we are ever likely to see.
    uint8_t p_message_payload[NRF_SDH_ANT_BURST_QUEUE_SIZE];
    sens_message_encode(p_profile, p_message_payload,&packet_size);

    // It is automatically a burst message if the packet size is unusual:
    if (packet_size!=ANT_STANDARD_DATA_PAYLOAD_SIZE)
    {
      // Note that I never expect to send more than 128 bytes in one go, so just one burst segment:
      uint8_t  burst_segment = BURST_SEGMENT_START;
      NRF_LOG_INFO("Burst message, page %u : %u bytes",p_message_payload[0],packet_size);
      err_code = sd_ant_burst_handler_request(p_profile->p_chan_config->channel_number,packet_size,p_message_payload,burst_segment);
      APP_ERROR_CHECK(err_code);
    }
    else
    {
      if (p_profile->p_sens_cb->send_requested_page && p_profile->page_70.transmission_response.items.ack_resposne)
      {
        NRF_LOG_INFO("Acknowledged message, page %u : %u bytes",p_message_payload[0],packet_size);
        err_code = sd_ant_acknowledge_message_tx(p_profile->p_chan_config->channel_number, packet_size, p_message_payload);
        APP_ERROR_CHECK(err_code);
      }
      else
      {
        NRF_LOG_INFO("Broadcast message, page %u : %u bytes",p_message_payload[0],packet_size);
        err_code = sd_ant_broadcast_message_tx(p_profile->p_chan_config->channel_number, packet_size, p_message_payload);
        APP_ERROR_CHECK(err_code);
      }
    }
      // Check frequency is correct, and if not, change it.
  if (p_profile->p_sens_cb->send_requested_page && p_profile->page_70.radio_frequency<255)
  {
    if (p_profile->p_chan_config->rf_freq != p_profile->page_70.radio_frequency)
    {
      NRF_LOG_INFO("Setting radio frequency to %u",p_profile->page_70.radio_frequency);
      p_profile->p_chan_config->rf_freq = p_profile->page_70.radio_frequency;
      err_code = sd_ant_channel_radio_freq_set(p_profile->p_chan_config->channel_number, p_profile->p_chan_config->rf_freq);
      APP_ERROR_CHECK(err_code);
    }
  }
  else if (p_profile->p_chan_config->rf_freq != AERO_ANTPLUS_RF_FREQ )
  {
      NRF_LOG_INFO("Resetting radio frequency to %u",AERO_ANTPLUS_RF_FREQ);
      p_profile->p_chan_config->rf_freq = AERO_ANTPLUS_RF_FREQ;
      err_code = sd_ant_channel_radio_freq_set(p_profile->p_chan_config->channel_number, p_profile->p_chan_config->rf_freq);
      APP_ERROR_CHECK(err_code);
  }
   
}

// Open ANT channel
ret_code_t ant_aero_open(ant_aero_profile_t * p_profile)
{
    // Fill tx buffer for the first frame
    ant_message_send(p_profile);

    return sd_ant_channel_open(p_profile->p_chan_config->channel_number);
}

// Ant event handler
void ant_aero_stack_evt_handler(ant_evt_t * p_ant_event, void * p_context)
{
  ant_aero_profile_t * p_profile = ( ant_aero_profile_t *)p_context;

  if (p_ant_event->channel == p_profile->p_chan_config->channel_number)
  {
    //NRF_LOG_INFO("ANT Event = 0x%x",p_ant_event->event);
    switch (p_ant_event->event)
    {
      case EVENT_TX:
        //NRF_LOG_INFO("EVENT_TX");
        p_profile->p_sens_cb->event_is_tx=true;
        ant_message_send(p_profile);
        break;

      case EVENT_RX:
        //NRF_LOG_INFO("EVENT_RX");
        p_profile->p_sens_cb->event_is_tx=false;
        //if (p_ant_event->message.ANT_MESSAGE_ucMesgID == MESG_BROADCAST_DATA_ID)
        if (p_ant_event->message.ANT_MESSAGE_ucMesgID == MESG_ACKNOWLEDGED_DATA_ID)
        {	 
          sens_message_decode(p_profile, p_ant_event->message.ANT_MESSAGE_aucPayload);		
        }
        break;

      default:
        // No implementation needed
        break;
    }
  }
}