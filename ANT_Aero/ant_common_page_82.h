#ifndef ANT_COMMON_PAGE_82_H__
#define ANT_COMMON_PAGE_82_H__

#include <stdint.h>
#include "nrf_log.h"
#include "sdk_common.h"

#define ANT_COMMON_PAGE_82 (82)

// Packet structure
typedef struct 
{
  uint8_t reserved;
  struct
  {
    uint8_t battery_count: 4;
    uint8_t battery_id:    4;
  } battery_identifier;
  uint8_t cumulative_operating_time[3];
  uint8_t fractional_voltage;
  struct
  {
    uint8_t coarse_voltage:       4;
    uint8_t status:               3;
    uint8_t second_resolution:    1;
  }descriptive_bit_field;
} ant_common_page_82_data_layout_t;

// Page data structure
typedef struct
{
  uint16_t voltage;
  uint8_t soc;
} ant_common_page_82_data_t;

//Initialiser - most of this page doesn't change.
#define DEFAULT_ANT_COMMON_PAGE_82()          \
    (ant_common_page_82_data_t)       \
  {                                   \
    .soc=0,                           \
    .voltage=0,                       \
  }

    

// Functions
void ant_common_page_82_encode(uint8_t * p_page_buffer, ant_common_page_82_data_t * p_page_data);

void ant_common_page_82_data_log(ant_common_page_82_data_t const * p_page_data);

void ant_common_page_82_update(ant_common_page_82_data_t * p_page_data, uint16_t voltage, uint8_t soc);


#endif // ANT_COMMON_PAGE_82_H__